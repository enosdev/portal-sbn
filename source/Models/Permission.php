<?php

namespace Source\Models;

use Source\Core\Model;

/**
 * FSPHP | Class Permission Active Record Pattern
 *
 * @author Robson V. Leite <cursos@upinside.com.br>
 * @package Source\Models
 */
class Permission extends Model
{
    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct("permissions", ["id"], ["title", "uri"]);
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if (!$this->required()) {
            $this->message->warning("Título e slug são necessários");
            return false;
        }

        /** Top10 Update */
        if (!empty($this->id)) {
            $permissionId = $this->id;

            $this->update($this->safe(), "id = :id", "id={$permissionId}");
            if ($this->fail()) {
                $this->message->error("Erro ao atualizar, verifique os dados");
                return false;
            }
        }

        /** Top10 Create */
        if (empty($this->id)) {

            $permissionId = $this->create($this->safe());
            if ($this->fail()) {
                $this->message->error("Erro ao cadastrar, verifique os dados");
                return false;
            }
        }

        $this->data = ($this->findById($permissionId))->data();
        return true;
    }
}