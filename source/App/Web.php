<?php

namespace Source\App;

use Source\Core\Controller;
use Source\Models\Auth;
use Source\Models\Category;
use Source\Models\Faq\Channel;
use Source\Models\Faq\Question;
use Source\Models\Gallery;
use Source\Models\Stopwatch;
use Source\Models\Post;
use Source\Models\Highlight;
use Source\Models\Agenda;
use Source\Models\Report\Access;
use Source\Models\Report\Online;
use Source\Models\User;
use Source\Support\Pager;

/**
 * Web Controller
 * @package Source\App
 */
class Web extends Controller
{
    /**
     * Web constructor.
     */
    public function __construct()
    {
        parent::__construct(__DIR__ . "/../../themes/" . CONF_VIEW_THEME . "/");

        (new Access())->report();
        (new Online())->report();
    }

    /**
     * SITE HOME
     */
    public function home(): void
    {
        $head = $this->seo->render(
            CONF_SITE_NAME . " - " . CONF_SITE_TITLE,
            CONF_SITE_DESC,
            url(),
            theme("/assets/images/share.jpg")
        );

        //seleciona o parent da categoria
        $p_arr = (new Category())->find("id != :id || parent != :id && ( id != :p || parent != :p)","id=6&p=9")->fetch(true);
        $ids = [];
        foreach($p_arr as $ca){
            $ids[] = $ca->id;
        }
        $i_x = implode(',',$ids);

        //seleciona somernte os vídeos
        $c_arr = (new Category())->find("id = :p || parent = :p", "p=9")->fetch(true);
        $idsc = [];
        foreach($c_arr as $cac){
            $idsc[] = $cac->id;
        }
        $i_c = implode(',',$idsc);
        

        $_7dias =  date('Y-m-d H:i:s', strtotime('-7days'));
        $hoje = date('Y-m-d H:i:s');

        echo $this->view->render("home", [
            "head" => $head,
            "menuHome" => 'active',
            "chamadas_ultimas" => (new Post())
                ->findPost('type = :t', "t=post")
                ->order("post_at DESC")
                ->limit(5)
                ->offset(4)
                ->fetch(true),
            "slide" => (new Post())
                ->findPost("type = :t && category IN({$i_x})", "t=post")
                ->order("post_at DESC")
                ->limit(4)
                ->fetch(true),
            "maisDestaque" => (new Post())
                ->findPost("type = :t && category IN({$i_x})", "t=post")
                ->order("post_at DESC")
                ->limit(10)
                ->offset(4)
                ->fetch(true),
            "tvSbn" => (new Post())
                ->findPost("type = :t && category IN({$i_c})", "t=post")
                ->order("post_at DESC")
                ->limit(4)
                ->fetch(true),
            "maisNoticias" => (new Post())
                ->findPost("type = :t && category IN({$i_x})", "t=post")
                ->order("post_at DESC")
                ->limit(8)
                ->offset(14)
                ->fetch(true),
            "gallery" => (new Gallery())
                ->findGallery("entertainment = :e", "e=yes")
                ->order("date_at DESC")
                ->limit(4)
                ->fetch(true),
            "outrasNoticias" => (new Post())
                ->findPost("type = :t && category IN({$i_x})", "t=post")
                ->order("post_at DESC")
                ->limit(24)
                ->offset(22)
                ->fetch(true),
            "enqueteHome" => (new Channel())
                ->find("status = :status && created_at < NOW() && expire_at > NOW()", "status=post")
                ->order('RAND()')
                ->fetch(),
            // "colunista" => (new Post())
            //     ->findPost('type = :t', 't=column')
            //     ->order("post_at DESC")
            //     ->limit(3)
            //     ->fetch(true),
            "highliths" => (new Highlight())
                ->findPost()
                ->order("post_at DESC")
                ->limit(4)
                ->fetch(true),
            "colunista" => (new User())
                ->find('level = :l', 'l=7')
                ->order("id DESC")
                ->limit(3)
                ->fetch(true),
            // "colunista2" => (new Post())
            //     ->findPost('type = :t && author = 26', 't=column')
            //     ->order("post_at DESC")
            //     ->fetch(),
            "list_agenda" => (new Agenda())
                ->findAgenda('event_date >= NOW()')
                ->order("event_date ASC")
                ->limit(5)
                ->fetch(true),
            "timer" => (new Stopwatch())
                ->findStopwatch("event_at >= NOW()")
                ->order('RAND()')
                ->fetch(),
            "ultimasEstado" => (new Post())
                ->findPost("type = :t", "t=post")
                ->order("post_at DESC")
                ->limit(4)
                ->fetch(true),
            // "gallery" => (new Gallery())
            //     ->findGallery()
            //     ->order('date_at DESC')
            //     ->limit(4)
            //     ->fetch(true),
        ]);
    }

    /**
     * LOAD MORE
     */
    public function loadMore(): void
    {
        $getData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        $setData = array_map("strip_tags", $getData);
        $setData = array_map("trim", $setData);

        $offset = $setData['offset'];
        $jSon['accept'] = null;
        switch ($setData['link']):
            case 'index':
                $read = (new Post())
                    ->findPost('type = :t', "t=post")
                    ->order("post_at DESC")
                    ->limit(12)
                    ->offset($offset)
                    ->fetch(true);
                if ($read):
                    $jSon['offset'] = $offset;
                    foreach ($read as $news):
                        $icon = ( strpos($news->content, "youtube.com/embed/") ? "<div style=\"top:-17px; bottom:auto;\" class=\"main_midia_icon\">Vídeo <i class=\"fa fa-youtube-play\"></i></div>" : (strpos($news->content, "@#[") ? "<div style=\"top:-17px; height:18px; background: #0071BC;\" class=\"main_midia_icon\">Galeria <i class=\"fa fa-picture-o\"></i></div>" : "") );
                        $jSon['accept'] .= '<article class="main_box_news main_box_white item">
                                                <a href="'.url("/artigo/{$news->uri}").'" title="'.$news->title.'">
                                                    <picture title="'.$news->title.'">                 
                                                    <img src="'.image($news->cover, 520,320).'" alt="'.$news->title.'" title="'.$news->title.'"/>
                                                    </picture>
                                                </a>
                                            
                                                <div class="main_box_news_desc">
                                                    <ul class="social">
                                                        <li class="social_item"><a href="https://api.whatsapp.com/send?text=Veja esta matéria:'. url("/artigo/{$news->uri}").'" title="Compartilhe WhatsApp" onclick="window.open(this.href, "_blank", "scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200"); return false;" class="whatsapp" rel="nofollow"><i class="fa fa-whatsapp"></i></a></li>
                                                        <li class="social_item"><a href="http://www.facebook.com/sharer.php?u='. url("/artigo/{$news->uri}").'" title="Compartilhe no Facebook" onclick="window.open(this.href, "_blank", "scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200"); return false;" class="facebook" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
                                                        <li class="social_item"><a href="https://twitter.com/intent/tweet?url='. url("/artigo/{$news->uri}").'&text='.$news->title.'" title="Conte isto no Twitter" onclick="window.open(this.href, "_blank", "scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200"); return false;" class="twitter" rel="nofollow"><i class="fa fa-twitter"></i></a></li>
                                                    </ul>
                                                    
                                                    <a href="'.url("/artigo/{$news->uri}").'" title="'.$news->title.'">
                                                        '.$icon.'
                                                        <mark style="background-color:'. $news->category()->color.';" class="categoria">'.$news->category()->title.'</mark>
                                                        <p class="tagline">'.$news->tag.'</p>
                                                        <time datetime="'. $news->post_at.'">'. date('d/m/Y', strtotime($news->post_at)).'</time>
                                                        <div class="clear"></div>
                                                        <h1>'.str_limit_chars($news->title, 70).'</h1>
                                                    </a>
                                                </div>
                                            </article>';
                    endforeach;
                else:
                    $jSon['accept'] = null;
                endif;
            break;
            case 'artigo-post':
                $read = (new Post())
                    ->findPost('type = :t && category = :c', "t=post&c={$setData['category']}")
                    ->order("post_at DESC")
                    ->limit(12)
                    ->offset($offset)
                    ->fetch(true);
                if ($read):
                    $jSon['offset'] = $offset;
                    foreach ($read as $news):
                        $icon = ( strpos($news->content, "youtube.com/embed/") ? "<div style=\"top:-17px; bottom:auto;\" class=\"main_midia_icon\">Vídeo <i class=\"fa fa-youtube-play\"></i></div>" : (strpos($news->content, "@#[") ? "<div style=\"top:-17px; height:18px; background: #0071BC;\" class=\"main_midia_icon\">Galeria <i class=\"fa fa-picture-o\"></i></div>" : "") );
                        $jSon['accept'] .= '<article class="main_box_news main_box_white item">
                                                <a href="'.url("/artigo/{$news->uri}").'" title="'.$news->title.'">
                                                    <picture title="'.$news->title.'">                 
                                                    <img src="'.image($news->cover, 520,320).'" alt="'.$news->title.'" title="'.$news->title.'"/>
                                                    </picture>
                                                </a>
                                            
                                                <div class="main_box_news_desc">
                                                    <ul class="social">
                                                        <li class="social_item"><a href="https://api.whatsapp.com/send?text=Veja esta matéria:'. url("/artigo/{$news->uri}").'" title="Compartilhe WhatsApp" onclick="window.open(this.href, "_blank", "scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200"); return false;" class="whatsapp" rel="nofollow"><i class="fa fa-whatsapp"></i></a></li>
                                                        <li class="social_item"><a href="http://www.facebook.com/sharer.php?u='. url("/artigo/{$news->uri}").'" title="Compartilhe no Facebook" onclick="window.open(this.href, "_blank", "scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200"); return false;" class="facebook" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
                                                        <li class="social_item"><a href="https://twitter.com/intent/tweet?url='. url("/artigo/{$news->uri}").'&text='.$news->title.'" title="Conte isto no Twitter" onclick="window.open(this.href, "_blank", "scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200"); return false;" class="twitter" rel="nofollow"><i class="fa fa-twitter"></i></a></li>
                                                    </ul>
                                                    
                                                    <a href="'.url("/artigo/{$news->uri}").'" title="'.$news->title.'">
                                                        '.$icon.'
                                                        <mark style="background-color:'. $news->category()->color.';" class="categoria">'.$news->category()->title.'</mark>
                                                        <p class="tagline">'.$news->tag.'</p>
                                                        <time datetime="'. $news->post_at.'">'. date('d/m/Y', strtotime($news->post_at)).'</time>
                                                        <div class="clear"></div>
                                                        <h1>'.str_limit_chars($news->title, 70).'</h1>
                                                    </a>
                                                </div>
                                            </article>';
                    endforeach;
                else:
                    $jSon['accept'] = null;
                endif;
            break;
            case 'artigos':
                $read = (new Post())
                    ->findPost('type = :t && category = :c', "t=post&c={$setData['category']}")
                    ->order("post_at DESC")
                    ->limit(12)
                    ->offset($offset)
                    ->fetch(true);
                if ($read):
                    $jSon['offset'] = $offset;
                    foreach ($read as $news):
                        $icon = ( strpos($news->content, "youtube.com/embed/") ? "<div style=\"top:-17px; bottom:auto;\" class=\"main_midia_icon\">Vídeo <i class=\"fa fa-youtube-play\"></i></div>" : (strpos($news->content, "@#[") ? "<div style=\"top:-17px; height:18px; background: #0071BC;\" class=\"main_midia_icon\">Galeria <i class=\"fa fa-picture-o\"></i></div>" : "") );
                        $jSon['accept'] .= '<article class="main_box_news main_box_white item">
                                                <a href="'.url("/artigo/{$news->uri}").'" title="'.$news->title.'">
                                                    <picture title="'.$news->title.'">                 
                                                    <img src="'.image($news->cover, 520,320).'" alt="'.$news->title.'" title="'.$news->title.'"/>
                                                    </picture>
                                                </a>
                                            
                                                <div class="main_box_news_desc">
                                                    <ul class="social">
                                                        <li class="social_item"><a href="https://api.whatsapp.com/send?text=Veja esta matéria:'. url("/artigo/{$news->uri}").'" title="Compartilhe WhatsApp" onclick="window.open(this.href, "_blank", "scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200"); return false;" class="whatsapp" rel="nofollow"><i class="fa fa-whatsapp"></i></a></li>
                                                        <li class="social_item"><a href="http://www.facebook.com/sharer.php?u='. url("/artigo/{$news->uri}").'" title="Compartilhe no Facebook" onclick="window.open(this.href, "_blank", "scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200"); return false;" class="facebook" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
                                                        <li class="social_item"><a href="https://twitter.com/intent/tweet?url='. url("/artigo/{$news->uri}").'&text='.$news->title.'" title="Conte isto no Twitter" onclick="window.open(this.href, "_blank", "scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200"); return false;" class="twitter" rel="nofollow"><i class="fa fa-twitter"></i></a></li>
                                                    </ul>
                                                    
                                                    <a href="'.url("/artigo/{$news->uri}").'" title="'.$news->title.'">
                                                        '.$icon.'
                                                        <mark style="background-color:'. $news->category()->color.';" class="categoria">'.$news->category()->title.'</mark>
                                                        <p class="tagline">'.$news->tag.'</p>
                                                        <time datetime="'. $news->post_at.'">'. date('d/m/Y', strtotime($news->post_at)).'</time>
                                                        <div class="clear"></div>
                                                        <h1>'.str_limit_chars($news->title, 70).'</h1>
                                                    </a>
                                                </div>
                                            </article>';
                    endforeach;
                else:
                    $jSon['accept'] = null;
                endif;
            break;
            
        endswitch;

        echo json_encode($jSon);
    }

    /**
     * @param array|null $data
     */
    public function bahia(array $data): void
    {
        $state = (new Post())->findPost("type = :t", "t=post")->order("post_at DESC")->limit(4)->fetch(true);
        if($state){
            $jSon = [];
            foreach($state as $post):
                $jSon[] = [
                    'title' => $post->title,
                    'uri' => url("/artigo/{$post->uri}"),
                    'cover' => image($post->cover, $data['w'], $data['h'])
                ];
            endforeach;

            echo json_encode($jSon);
        }
    }

    /**
     * SITE ABOUT
     */
    public function about(): void
    {
        $head = $this->seo->render(
            "Descubra o " . CONF_SITE_NAME . " - " . CONF_SITE_DESC,
            CONF_SITE_DESC,
            url("/sobre"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("about", [
            "head" => $head,
            "about" => (new Post())
                ->findPost("type = :t", "t=radio")
                ->fetch()
        ]);
    }

    /**
     * SITE CONTACT
     */
    public function contact(): void
    {
        $head = $this->seo->render(
            "Descubra o " . CONF_SITE_NAME . " - " . CONF_SITE_DESC,
            CONF_SITE_DESC,
            url("/contato"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("contact", [
            "head" => $head,
            "video" => "lDZGl9Wdc7Y",
            "faq" => (new Question())
                ->find("channel_id = :id", "id=1", "question, response")
                ->order("order_by")
                ->fetch(true)
        ]);
    }

    /**
     * SITE BLOG
     * @param array|null $data
     */
    public function artigo(?array $data): void
    {
        $head = $this->seo->render(
            "Artigo - " . CONF_SITE_NAME,
            "Confira as notícias de nossa região atualizada a todo momento",
            url("/artigo"),
            theme("/assets/images/share.jpg")
        );

        $blog = (new Post())->findPost('type = :t', 't=post');
        $pager = new Pager(url("/artigo/p/"));
        $pager->pager($blog->count(), 9, ($data['page'] ?? 1));

        $_7dias =  date('Y-m-d H:i:s', strtotime('-7days'));
        $hoje = date('Y-m-d H:i:s');

        echo $this->view->render("artigo", [
            "head" => $head,
            "article" => $blog->order("post_at DESC")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render('justify-content-center'),
            "mais_lidas" => (new Post())
                ->findPost('type = :t && post_at >= :d && post_at <= :h', "t=post&d={$_7dias}&h={$hoje}")
                ->order( 'views DESC')
                ->limit(5)
                ->fetch(true),
        ]);
    }

    /**
     * SITE BLOG CATEGORY
     * @param array $data
     */
    public function artigoCategory(array $data): void
    {
        $categoryUri = filter_var($data["category"], FILTER_SANITIZE_STRIPPED);
        $category = (new Category())->findByUri($categoryUri);

        if (!$category) {
            redirect("/artigo");
        }

        $blogCategory = (new Post())->findPost("category = :c && type = :t", "c={$category->id}&t=post");
        $page = (!empty($data['page']) && filter_var($data['page'], FILTER_VALIDATE_INT) >= 1 ? $data['page'] : 1);
        $pager = new Pager(url("/artigo/em/{$category->uri}/"));
        $pager->pager($blogCategory->count(), 9, $page);

        $head = $this->seo->render(
            "Artigos em {$category->title} - " . CONF_SITE_NAME,
            $category->description,
            url("/artigo/em/{$category->uri}/{$page}"),
            ($category->cover ? image($category->cover, 1200, 628) : theme("/assets/images/share.jpg"))
        );

        $_7dias =  date('Y-m-d H:i:s', strtotime('-7days'));
        $hoje = date('Y-m-d H:i:s');

        echo $this->view->render("artigo", [
            "head" => $head,
            "title" => "Artigos em {$category->title}",
            "cor" => $category->color,
            "menu" => $category->id,
            "idd" => $category->id,
            "desc" => $category->description,
            "slide" => (new Post())
                ->findPost("type = :t && category = :c", "t=post&c={$category->id}")
                ->order("post_at DESC")
                ->limit(4)
                ->fetch(true),
            "maisDestaque" => (new Post())
                ->findPost("type = :t && category = :c", "t=post&c={$category->id}")
                ->order("post_at DESC")
                ->limit(12)
                ->offset(4)
                ->fetch(true),
            "maisNoticias" => (new Post())
                ->findPost("type = :t && category = :c", "t=post&c={$category->id}")
                ->order("post_at DESC")
                ->limit(12)
                ->offset(16)
                ->fetch(true),
            "outrasNoticias" => (new Post())
                ->findPost("type = :t && category = :c", "t=post&c={$category->id}")
                ->order("post_at DESC")
                ->limit(24)
                ->offset(28)
                ->fetch(true),
            "article" => $blogCategory
                ->limit($pager->limit())
                ->offset($pager->offset())
                ->order("post_at DESC")
                ->fetch(true),
            "enqueteHome" => (new Channel())
                ->find("status = :status && created_at < NOW() && expire_at > NOW()", "status=post")
                ->order('RAND()')
                ->fetch(),
            "colunista" => (new User())
                ->find('level = :l', 'l=7')
                ->order("id DESC")
                ->limit(3)
                ->fetch(true),
            // "colunista2" => (new Post())
            //     ->findPost('type = :t && author = 26', 't=column')
            //     ->order("post_at DESC")
            //     ->fetch(),
            "list_agenda" => (new Agenda())
                ->findAgenda('event_date >= NOW()')
                ->order("event_date ASC")
                ->limit(5)
                ->fetch(true),
            "timer" => (new Stopwatch())
                ->findStopwatch("event_at >= NOW()")
                ->fetch(),
            "paginator" => $pager->render(),
            "mais_lidas" => (new Post())
                ->findPost('type = :t && post_at >= :d && post_at <= :h', "t=post&d={$_7dias}&h={$hoje}")
                ->order("views DESC")
                ->limit(5)
                ->fetch(true)
        ]);
    }

    /**
     * SITE BLOG CATEGORY
     * @param array $data
     */
    public function artigoCategoryPrincipal(array $data): void
    {
        $categoryUri = filter_var($data["category"], FILTER_SANITIZE_STRIPPED);
        $category = (new Category())->findByUri($categoryUri);

        if (!$category) {
            redirect("/artigo");
        }

        $blogCategory = (new Post())->findPost("category = :c && type = :t", "c={$category->id}&t=post");
        $page = (!empty($data['page']) && filter_var($data['page'], FILTER_VALIDATE_INT) >= 1 ? $data['page'] : 1);
        $pager = new Pager(url("/artigo/em/{$category->uri}/"));
        $pager->pager($blogCategory->count(), 9, $page);

        $head = $this->seo->render(
            "Artigos em {$category->title} - " . CONF_SITE_NAME,
            $category->description,
            url("/artigo/em/{$category->uri}/{$page}"),
            ($category->cover ? image($category->cover, 1200, 628) : theme("/assets/images/share.jpg"))
        );

        $_7dias =  date('Y-m-d H:i:s', strtotime('-7days'));
        $hoje = date('Y-m-d H:i:s');

        $catArray = (new Category())->find("parent = :p || id = :id","p={$category->id}&id={$category->id}")->fetch(true);
        foreach($catArray as $cat){
            $catArray2[] = $cat->id; 
        }
        $cAr = implode(',',$catArray2);

        echo $this->view->render("artigo", [
            "head" => $head,
            "title" => "Artigos em {$category->title}",
            "cor" => $category->color,
            "menu" => $category->id,
            "idd" => $category->id,
            "desc" => $category->description,
            "slide" => (new Post())
                ->findPost("type = :t && category IN({$cAr})", "t=post")
                ->order("post_at DESC")
                ->limit(4)
                ->fetch(true),
            "maisDestaque" => (new Post())
                ->findPost("type = :t && category IN({$cAr})", "t=post")
                ->order("post_at DESC")
                ->limit(12)
                ->offset(4)
                ->fetch(true),
            "maisNoticias" => (new Post())
                ->findPost("type = :t && category IN({$cAr})", "t=post")
                ->order("post_at DESC")
                ->limit(12)
                ->offset(16)
                ->fetch(true),
            "outrasNoticias" => (new Post())
                ->findPost("type = :t && category IN({$cAr})", "t=post")
                ->order("post_at DESC")
                ->limit(24)
                ->offset(28)
                ->fetch(true),
            "article" => $blogCategory
                ->limit($pager->limit())
                ->offset($pager->offset())
                ->order("post_at DESC")
                ->fetch(true),
            "paginator" => $pager->render(),
            "enqueteHome" => (new Channel())
                ->find("status = :status && created_at < NOW() && expire_at > NOW()", "status=post")
                ->order('RAND()')
                ->fetch(),
            "colunista" => (new User())
                ->find('level = :l', 'l=7')
                ->order("id DESC")
                ->limit(3)
                ->fetch(true),
            // "colunista2" => (new Post())
            //     ->findPost('type = :t && author = 26', 't=column')
            //     ->order("post_at DESC")
            //     ->fetch(),
            "list_agenda" => (new Agenda())
                ->findAgenda('event_date >= NOW()')
                ->order("event_date ASC")
                ->limit(5)
                ->fetch(true),
            "timer" => (new Stopwatch())
                ->findStopwatch("event_at >= NOW()")
                ->fetch(),
            "ultimasEstado" => (new Post())
                ->findPost("type = :t && category IN({$cAr})", "t=post")
                ->order("post_at DESC")
                ->limit(4)
                ->fetch(true),
        ]);
    }

    /**
     * SITE BLOG SEARCH
     * @param array $data
     */
    public function artigoSearch(array $data): void
    {
        if (!empty($data['s'])) {
            $search = str_search($data['s']);
            //echo json_encode(["redirect" => url("/artigo/buscar/{$search}/1")]);
            redirect("/artigo/buscar/{$search}/1");
            return;
        }

        $search = str_search($data['search']);
        $page = (filter_var($data['page'], FILTER_VALIDATE_INT) >= 1 ? $data['page'] : 1);

        if ($search == "all") {
            redirect("/artigo/buscar");
        }

        $head = $this->seo->render(
            "Pesquisa por {$search} - " . CONF_SITE_NAME,
            "Confira os resultados de sua pesquisa para {$search}",
            url("/artigo/buscar/{$search}/{$page}"),
            theme("/assets/images/share.jpg")
        );

        $blogSearch = (new Post())->findPost("MATCH(title, subtitle) AGAINST(:s) && type = :t", "s={$search}&t=post");

        if (!$blogSearch->count()) {
            echo $this->view->render("busca", [
                "head" => $head,
                "title" => "Pesquisa por:",
                "search" => $search
            ]);
            return;
        }

        $pager = new Pager(url("/artigo/buscar/{$search}/"));
        $pager->pager($blogSearch->count(), 32, $page);

        $_7dias =  date('Y-m-d H:i:s', strtotime('-7days'));
        $hoje = date('Y-m-d H:i:s');

        echo $this->view->render("busca", [
            "head" => $head,
            "title" => "Pesquisa por:",
            "search" => $search,
            "conta" => $blogSearch->count(),
            "article" => $blogSearch->order("post_at DESC")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * SITE ARTIGO POST
     * @param array $data
     */
    public function artigoPost(array $data): void
    {
        $post = (new Post())->findByUri($data['uri']);
        if (!$post) {
            redirect("/404");
        }

        $user = Auth::user();
        if (!$user || $user->level < 5) {
            $post->views += 1;
            $post->save();
        }

        $head = $this->seo->render(
            html_entity_decode($post->title)." - " . CONF_SITE_NAME,
            ($post->subtitle ? html_entity_decode(strip_tags(str_limit_chars($post->subtitle,145))) : str_limit_chars(html_entity_decode(strip_tags($post->content)), 145)),
            url("/artigo/{$post->uri}"),
            ($post->cover ? image($post->cover, 1200, 628) : theme("/assets/images/share.jpg"))
        );

        $catArray = (new Category())->find("parent = :p || id = :id","p={$post->category()->id}&id={$post->category()->id}")->fetch(true);
        foreach($catArray as $cat){
            $catArray2[] = $cat->id; 
        }
        $cAr = implode(',',$catArray2);
        
        $type = ($post->type == 'post')? 'post' : 'column';
        $umano = date("Y-m-d", strtotime("-12 months"));
        $hoje = date("Y-m-d");
        echo $this->view->render("artigo-post", [
            "head" => $head,
            "post" => $post,
            "galleryId" => ($post->gallery != NULL)? (new Gallery())->findById($post->gallery) : false,
            "idd" => $post->category,
            "cor" => $post->category()->color,
            "latest" => (new Post())
                ->findPost('type = :t', 't=post')
                ->order('post_at DESC')
                ->limit(5)
                ->fetch(true),
            "related" => (new Post())
                ->findPost("category = :c AND id != :i && type = :t && post_at BETWEEN '{$umano}' AND '{$hoje}'", "c={$post->category}&i={$post->id}&t={$type}")
                ->order("RAND()")
                ->limit(18)
                ->fetch(true),
            "list_agenda" => (new Agenda())
                ->findAgenda('event_date >= NOW()')
                ->order("event_date ASC")
                ->limit(5)
                ->fetch(true),
            "ultimasEstado" => (new Post())
                ->findPost("type = :t && category IN({$cAr})", "t=post")
                ->order("post_at DESC")
                ->limit(4)
                ->fetch(true),
            "poll" => (new Channel())
                ->find("status = :status && created_at < NOW() && expire_at > NOW()", "status=post")
                ->order("RAND()")
                ->fetch(),
            "maislidas" => (new Post())
                ->findPost("type = :t && MONTH(post_at) = MONTH(NOW()) && YEAR(post_at) = YEAR(NOW())", "t=post")
                ->order("views DESC")
                ->limit(10)
                ->fetch(true)
        ]);
    }

    /**
     * SITE ENQUETE
     * @param array $data
     */
    public function enquete(): void
    {
        $getData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        $setData = array_map('strip_tags', $getData);
        $setData = array_map('trim', $getData);
        $json['result'] = null;
        $questionEdit = (new Question())->findById($setData["question_id"]);
        $questionRead = (new Question())->find("channel_id = :id","id={$setData["channel_id"]}");
        $channel = (new Channel())->findById($setData["channel_id"]);
        
        if ($channel->expire_at <= date("Y-m-d H:i:s")):
            $json['accept'] = "A enquete no momento está desativada para novos votos!";
            echo json_encode($json);
            exit;
        endif;

        if($setData["captcha"] == base64_decode($_SESSION["word"])):

            $questionEdit->votes += 1;

            if (!$questionEdit->save()):
                $json["message"] = $questionEdit->message()->render();
                echo json_encode($json);  
                return;          
            endif;
        else:
            $volta = '<a id="voltar" href="javascript:void(0)" class="view_results j_view_results" title="Voltar para votar"><i class="fa fa-chevron-left"></i> Voltar</a>';
        endif;

        if ($questionRead->fetch()):
            $totalVotes = 0;
            foreach ($questionRead->fetch(true) as $Sum):
                $totalVotes += $Sum->votes;
            endforeach;

            foreach ($questionRead->fetch(true) as $Ques):
                //Calcula o percentual dos votos
                //$percent = ( $Ques->votes == 0 ? 0 : round($Ques->votes / $totalVotes * 100) );
                $percent = ( $Ques->votes == 0 ? 0 : ($Ques->votes * 100) / $totalVotes);

                $json['result'] .= '
                <article>
                    <h1>' . $Ques->question . ' (' . number_format($percent,2,',','') . '% | ' . $Ques->votes . ' Votos)</h1>
                    <div class="percent_bar">
                        <div class="percent" style="width: ' . $percent . '%;"></div>
                    </div>
                </article>
                ';
            endforeach;
                $json['result'] .= '<p class="total_votes">Total de votos: '.$totalVotes.'</p>';
                $json['idPoll'] = $setData["channel_id"];
                $json['time'] = (time() + (7 * 24 * 60 * 60));
                $json['ip'] = $_SERVER['REMOTE_ADDR'];
        endif;

        if($setData["captcha"] == base64_decode($_SESSION["word"])):

            $json['accept'] = "Obrigado por votar na nossa enquete!";
            echo json_encode($json);

        else:

            $json['accept'] = "Voto não computado! Preencha o campo NUMÉRICO corretamente!";
            echo json_encode($json);

        endif;
    }

        /**
     * SITE ENQUETE POST
     * @param array $data
     */
    public function enquetePost(array $data): void
    {
        $post = (new Channel())->find("uri = :uri", "uri={$data['uri']}")->fetch();
        if (!$post) {
            redirect("/404");
        }

        // $user = Auth::user();
        // if (!$user || $user->level < 5) {
        //     $post->views += 1;
        //     $post->save();
        // }

        $head = $this->seo->render(
            "{$post->channel} - " . CONF_SITE_NAME,
            $post->channel,
            url("/enquete/{$post->uri}"),
            ($post->cover ? image($post->cover, 1200, 628) : theme("/assets/images/share.jpg"))
        );
        
        echo $this->view->render("enquete-post", [
            "head" => $head,
            "post" => $post,
            "list_agenda" => [],
            "ultimasEstado" => [],
            "maislidas" => []
            // "list_agenda" => (new Agenda())
            //     ->findAgenda('event_date >= NOW()')
            //     ->order("event_date ASC")
            //     ->limit(5)
            //     ->fetch(true),
            // "ultimasEstado" => (new Post())
            //     ->findPost("type = :t", "t=post")
            //     ->order("post_at DESC")
            //     ->limit(4)
            //     ->fetch(true),
            // "maislidas" => (new Post())
            //     ->findPost("type = :t && MONTH(post_at) = MONTH(NOW()) && YEAR(post_at) = YEAR(NOW())", "t=post")
            //     ->order("views DESC")
            //     ->limit(10)
            //     ->fetch(true)
        //     "poll" => (new Channel())
        //         ->find("status = :status && created_at < NOW() && expire_at > NOW()", "status=post")
        //         ->order("RAND()")
        //         ->fetch()
        ]);
    }

    /**
     * SITE PAGINAS
     * @param array $data
     */
    public function paginas(array $data): void
    {
        $post = (new Post())->findByUri($data['uri']);
        if (!$post) {
            redirect("/404");
        }

        $user = Auth::user();
        if (!$user || $user->level < 5) {
            $post->views += 1;
            $post->save();
        }

        $head = $this->seo->render(
            "{$post->title} - " . CONF_SITE_NAME,
            $post->subtitle,
            url("/pagina/{$post->uri}"),
            ($post->cover ? image($post->cover, 1200, 628) : theme("/assets/images/share.jpg"))
        );
        
        
        echo $this->view->render("pagina", [
            "head" => $head,
            "post" => $post,
            "idd" => $post->category
            // "cor" => $post->category()->color
        ]);
    }

    /**
     * SITE PROMOÇÃO
     * @param array|null $data
     */
    public function promotion(?array $data): void
    {
        $head = $this->seo->render(
            "Promoção - " . CONF_SITE_NAME,
            "Confira as promoções e participe",
            url("/promocao"),
            theme("/assets/images/share.jpg")
        );

        $blog = (new Post())->findPost('type = :t', 't=promotion');
        $pager = new Pager(url("/promotion/p/"));
        $pager->pager($blog->count(), 9, ($data['page'] ?? 1));

        echo $this->view->render("promotion", [
            "head" => $head,
            "article" => $blog->order("post_at DESC")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render('justify-content-center')
        ]);
    }

    /**
     * SITE PROMOTION POST
     * @param array $data
     */
    public function promotionPost(array $data): void
    {
        $post = (new Post())->findByUri($data['uri']);
        if (!$post) {
            redirect("/404");
        }

        $user = Auth::user();
        if (!$user || $user->level < 5) {
            $post->views += 1;
            $post->save();
        }

        $head = $this->seo->render(
            "{$post->title} - " . CONF_SITE_NAME,
            $post->subtitle,
            url("/promotion/{$post->uri}"),
            ($post->cover ? image($post->cover, 1200, 628) : theme("/assets/images/share.jpg"))
        );

        echo $this->view->render("artigo-post", [
            "head" => $head,
            "post" => $post
        ]);
    }

    /**
     * SITE LOGIN
     * @param null|array $data
     */
    public function login(?array $data): void
    {
        if (Auth::user()) {
            redirect("/app");
        }

        if (!empty($data['csrf'])) {
            if (!csrf_verify($data)) {
                $json['message'] = $this->message->error("Erro ao enviar, favor use o formulário")->render();
                echo json_encode($json);
                return;
            }

            if (request_limit("weblogin", 3, 60 * 5)) {
                $json['message'] = $this->message->error("Você já efetuou 3 tentativas, esse é o limite. Por favor, aguarde 5 minutos para tentar novamente!")->render();
                echo json_encode($json);
                return;
            }

            if (empty($data['email']) || empty($data['password'])) {
                $json['message'] = $this->message->warning("Informe seu email e senha para entrar")->render();
                echo json_encode($json);
                return;
            }

            $save = (!empty($data['save']) ? true : false);
            $auth = new Auth();
            $login = $auth->login($data['email'], $data['password'], $save);

            if ($login) {
                $this->message->success("Seja bem-vindo(a) de volta " . Auth::user()->first_name . "!")->flash();
                $json['redirect'] = url("/app");
            } else {
                $json['message'] = $auth->message()->before("Ooops! ")->render();
            }

            echo json_encode($json);
            return;
        }

        $head = $this->seo->render(
            "Entrar - " . CONF_SITE_NAME,
            CONF_SITE_DESC,
            url("/entrar"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("auth-login", [
            "head" => $head,
            "cookie" => filter_input(INPUT_COOKIE, "authEmail")
        ]);
    }

    /**
     * SITE PASSWORD FORGET
     * @param null|array $data
     */
    public function forget(?array $data)
    {
        if (Auth::user()) {
            redirect("/app");
        }

        if (!empty($data['csrf'])) {
            if (!csrf_verify($data)) {
                $json['message'] = $this->message->error("Erro ao enviar, favor use o formulário")->render();
                echo json_encode($json);
                return;
            }

            if (empty($data["email"])) {
                $json['message'] = $this->message->info("Informe seu e-mail para continuar")->render();
                echo json_encode($json);
                return;
            }

            if (request_repeat("webforget", $data["email"])) {
                $json['message'] = $this->message->error("Ooops! Você já tentou este e-mail antes")->render();
                echo json_encode($json);
                return;
            }

            $auth = new Auth();
            if ($auth->forget($data["email"])) {
                $json["message"] = $this->message->success("Acesse seu e-mail para recuperar a senha")->render();
            } else {
                $json["message"] = $auth->message()->before("Ooops! ")->render();
            }

            echo json_encode($json);
            return;
        }

        $head = $this->seo->render(
            "Recuperar Senha - " . CONF_SITE_NAME,
            CONF_SITE_DESC,
            url("/recuperar"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("auth-forget", [
            "head" => $head
        ]);
    }

    /**
     * SITE FORGET RESET
     * @param array $data
     */
    public function reset(array $data): void
    {
        if (Auth::user()) {
            redirect("/app");
        }

        if (!empty($data['csrf'])) {
            if (!csrf_verify($data)) {
                $json['message'] = $this->message->error("Erro ao enviar, favor use o formulário")->render();
                echo json_encode($json);
                return;
            }

            if (empty($data["password"]) || empty($data["password_re"])) {
                $json["message"] = $this->message->info("Informe e repita a senha para continuar")->render();
                echo json_encode($json);
                return;
            }

            list($email, $code) = explode("|", $data["code"]);
            $auth = new Auth();

            if ($auth->reset($email, $code, $data["password"], $data["password_re"])) {
                $this->message->success("Senha alterada com sucesso. Vamos controlar?")->flash();
                $json["redirect"] = url("/entrar");
            } else {
                $json["message"] = $auth->message()->before("Ooops! ")->render();
            }

            echo json_encode($json);
            return;
        }

        $head = $this->seo->render(
            "Crie sua nova senha no " . CONF_SITE_NAME,
            CONF_SITE_DESC,
            url("/recuperar"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("auth-reset", [
            "head" => $head,
            "code" => $data["code"]
        ]);
    }

    /**
     * SITE REGISTER
     * @param null|array $data
     */
    public function register(?array $data): void
    {
        if (Auth::user()) {
            redirect("/app");
        }

        if (!empty($data['csrf'])) {
            if (!csrf_verify($data)) {
                $json['message'] = $this->message->error("Erro ao enviar, favor use o formulário")->render();
                echo json_encode($json);
                return;
            }

            if (in_array("", $data)) {
                $json['message'] = $this->message->info("Informe seus dados para criar sua conta.")->render();
                echo json_encode($json);
                return;
            }

            $auth = new Auth();
            $user = new User();
            $user->bootstrap(
                $data["first_name"],
                $data["last_name"],
                $data["email"],
                $data["password"]
            );

            if ($auth->register($user)) {
                $json['redirect'] = url("/confirma");
            } else {
                $json['message'] = $auth->message()->before("Ooops! ")->render();
            }

            echo json_encode($json);
            return;
        }

        $head = $this->seo->render(
            "Criar Conta - " . CONF_SITE_NAME,
            CONF_SITE_DESC,
            url("/cadastrar"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("auth-register", [
            "head" => $head
        ]);
    }

    /**
     * SITE OPT-IN CONFIRM
     */
    public function confirm(): void
    {
        $head = $this->seo->render(
            "Confirme Seu Cadastro - " . CONF_SITE_NAME,
            CONF_SITE_DESC,
            url("/confirma"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("optin", [
            "head" => $head,
            "data" => (object)[
                "title" => "Falta pouco! Confirme seu cadastro.",
                "desc" => "Enviamos um link de confirmação para seu e-mail. Acesse e siga as instruções para concluir seu cadastro e comece a controlar com o CaféControl",
                "image" => theme("/assets/images/optin-confirm.jpg")
            ]
        ]);
    }

    /**
     * SITE OPT-IN SUCCESS
     * @param array $data
     */
    public function success(array $data): void
    {
        $email = base64_decode($data["email"]);
        $user = (new User())->findByEmail($email);

        if ($user && $user->status != "confirmed") {
            $user->status = "confirmed";
            $user->save();
        }

        $head = $this->seo->render(
            "Bem-vindo(a) ao " . CONF_SITE_NAME,
            CONF_SITE_DESC,
            url("/obrigado"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("optin", [
            "head" => $head,
            "data" => (object)[
                "title" => "Tudo pronto. Você já pode controlar :)",
                "desc" => "Bem-vindo(a) ao seu controle de contas, vamos tomar um café?",
                "image" => theme("/assets/images/optin-success.jpg"),
                "link" => url("/entrar"),
                "linkTitle" => "Fazer Login"
            ],
            "track" => (object)[
                "fb" => "Lead",
                "aw" => "AW-953362805/yAFTCKuakIwBEPXSzMYD"
            ]
        ]);
    }

    /**
     * SITE TERMS
     */
    public function terms(): void
    {
        $head = $this->seo->render(
            CONF_SITE_NAME . " - Termos de uso",
            CONF_SITE_DESC,
            url("/termos"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("terms", [
            "head" => $head
        ]);
    }

    /**
     * SITE NAV ERROR
     * @param array $data
     */
    public function error(array $data): void
    {
        $error = new \stdClass();

        switch ($data['errcode']) {
            case "problemas":
                $error->code = "OPS";
                $error->title = "Estamos enfrentando problemas!";
                $error->message = "Parece que nosso serviço não está diponível no momento. Já estamos vendo isso mas caso precise, envie um e-mail :)";
                $error->linkTitle = "ENVIAR E-MAIL";
                $error->link = "mailto:" . CONF_MAIL_SUPPORT;
                break;

            case "manutencao":
                $error->code = "OPS";
                $error->title = "Desculpe. Estamos em manutenção!";
                $error->message = "Voltamos logo! Por hora estamos trabalhando para melhorar nosso conteúdo para você controlar melhor as suas contas :P";
                $error->linkTitle = null;
                $error->link = null;
                break;

            default:
                $error->code = $data['errcode'];
                $error->title = "Ooops. Conteúdo indisponível :/";
                $error->message = "Sentimos muito, mas o conteúdo que você tentou acessar não existe, está indisponível no momento ou foi removido :/";
                $error->linkTitle = "Continue navegando!";
                $error->link = url_back();
                break;
        }

        $head = $this->seo->render(
            "{$error->code} | {$error->title}",
            $error->message,
            url("/ops/{$error->code}"),
            theme("/assets/images/share.jpg"),
            false
        );

        echo $this->view->render("error", [
            "head" => $head,
            "error" => $error,
            "maisNoticias" => (new Post())
                ->findPost('type = :t', 't=post')
                ->order("post_at DESC")
                ->limit(4)
                ->fetch(true)
        ]);
    }
}