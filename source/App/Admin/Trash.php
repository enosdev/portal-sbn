<?php

namespace Source\App\Admin;

use Source\Models\Gallery;
use Source\Models\Publicity;
use Source\Models\Post;
use Source\Models\Highlight;
use Source\Support\Thumb;

/**
 * Class Trash
 * @package Source\App\Admin
 */
class Trash extends Admin
{
    /**
     * Dash constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function home(?array $data): void
    {

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Lixeira",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN."/trash"),
            theme("/assets/images/image.jpg", CONF_VIEW_ADMIN),
            false
        );

        echo $this->view->render("widgets/trash/home", [
            "app" => "trash",
            "head" => $head,
            "posts" => (new Post())->find("type = :type && status = :st",'type=post&st=trash')->order('post_at DESC')->fetch(true),
            "column" => (new Post())->find("type = :type && status = :st",'type=column&st=trash')->order('post_at DESC')->fetch(true),
            "gallery" => (new Gallery())->find("status = :st",'st=trash')->order('date_at DESC')->fetch(true),
            "ads" => (new Publicity())->find("status = :st",'st=trash')->order('publicity_at DESC')->fetch(true),
            "pages" => (new Post())->find("type = :type && status = :st",'type=page&st=trash')->order('post_at DESC')->fetch(true),
            "highlight" => (new Highlight())->find("status = :st",'st=trash')->order('post_at DESC')->fetch(true)
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function post(?array $data): void
    {

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $postDelete = (new Post())->findById($data["post_id"]);

            if (!$postDelete) {
                $this->message->error("Você tentou excluir um post que não existe ou já foi removido")->flash();
                echo json_encode(["reload" => true]);
                return;
            }

            if ($postDelete->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover}");
                (new Thumb())->flush($postDelete->cover);
            }

            $postDelete->destroy();
            $this->message->success("O post foi excluído com sucesso...")->flash();

            echo json_encode(["reload" => true]);
            return;
        }

    }

}