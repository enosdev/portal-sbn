<?php

namespace Source\App\Admin;

use Source\Models\Permission;
use Source\Models\User;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;

/**
 * Class Conf
 * @package Source\App\Admin
 */
class Conf extends Admin
{
    /**
     * Conf constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        $permitir = (new Permission())->find();       

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Configurações",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/setting/home", [
            "app" => "setting/home",
            "head" => $head,
            // "search" => $search,
            // "top" => $top->order("position")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "permissions" => $permitir->fetch(true)
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function permit(?array $data): void
    {
        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $permissionCreate = new Permission();
            $permissionCreate->title = $data["title"];
            $permissionCreate->uri = $data["uri"];


            if (!$permissionCreate->save()) {
                $json["message"] = $permissionCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Permissão cadastrada com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/setting/permission/{$permissionCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $permissionUpdate = (new Permission())->findById($data["permission_id"]);

            if (!$permissionUpdate) {
                $this->message->error("Você tentou gerenciar uma permissão que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/setting/home")]);
                return;
            }

            $permissionUpdate->title = $data["title"];
            $permissionUpdate->uri = $data["uri"];

            if (!$permissionUpdate->save()) {
                $json["message"] = $permissionUpdate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Permissão atualizada com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $permissionDelete = (new Permission())->findById($data["permission_id"]);

            if (!$permissionDelete) {
                $this->message->error("Você tentnou deletar uma Permissão que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/setting/home")]);
                return;
            }

            $permissionDelete->destroy();

            $this->message->success("A Permissão foi excluída com sucesso...")->flash();
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/setting/home")]);

            return;
        }

        $permissionEdit = null;
        if (!empty($data["permission_id"])) {
            $permissionId = filter_var($data["permission_id"], FILTER_VALIDATE_INT);
            $permissionEdit = (new Permission())->findById($permissionId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Permissões para Usuários e Menu",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/setting/permission", [
            "app" => "setting/permission",
            "head" => $head,
            "permission" => $permissionEdit
        ]);
    }
}