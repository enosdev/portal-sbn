<?php

namespace Source\App\Admin;

use Source\Models\Category;
use Source\Models\Post;
use Source\Models\User;
use Source\Models\Gallery;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;
use Source\Models\Faq\Channel;

/**
 * Class Blog
 * @package Source\App\Admin
 */
class Blog extends Admin
{
    /**
     * Blog constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/blog/home/{$s}/1")]);
            return;
        }

        //category redirect
        if(!isset($_SESSION['cate'])){
            $_SESSION['cate'] = null;
        }
        if (!empty($data["c"])) {
            if($data['c'] == "null"){
                $_SESSION['cate'] = null;
            } else {
                $c = str_search($data["c"]);
                $_SESSION['cate'] = $c;
            }
            echo json_encode(["reload" => true]);
            return;
        }

        $names_cat = str_replace(",", "','", user()->categories);
        $permit_cat = (new Category())->find("uri IN ('{$names_cat}')")->fetch(true);
        $cat_juntas = '0';
        foreach($permit_cat as $pc){
            $cat_juntas .= ",{$pc->id}";
        }

        $cat_complet = (new Category())->find("id IN ($cat_juntas) || parent IN ($cat_juntas)")->fetch(true);
        $cat_full = '0';
        foreach($cat_complet as $complet){
            $cat_full .= ",{$complet->id}";
        }

        $where_cat = ($_SESSION['cate'] != null)? " && category = {$_SESSION['cate']}" : " && category IN ({$cat_full})";

        $search = null;
        $posts = (new Post())->find("type = :type && status != :st{$where_cat}",'type=post&st=trash');

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $posts = (new Post())->find("MATCH(title, subtitle) AGAINST(:s){$where_cat} && status != :st", "s={$search}&st=trash");
            if (!$posts->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/blog/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/blog/home/{$all}/"));
        $pager->pager($posts->count(), 36, (!empty($data["page"]) ? $data["page"] : 1));

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Portal",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/blog/home", [
            "app" => "blog/home",
            "head" => $head,
            "posts" => $posts->limit($pager->limit())->offset($pager->offset())->order("post_at DESC")->fetch(true),
            "paginator" => $pager->render(),
            "search" => $search,
            "select_cate" => $_SESSION['cate'],
            "categories" => (new Category())->find("type = :type && id IN ({$cat_juntas})", "type=post")->order("title")->fetch(true),
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function post(?array $data): void
    {
        //MCE Upload
        if (!empty($data["upload"]) && !empty($_FILES["image"])) {
            $files = $_FILES["image"];
            $upload = new Upload();
            $image = $upload->image($files, "post-" . time());

            if (!$image) {
                $json["message"] = $upload->message()->render();
                echo json_encode($json);
                return;
            }

            $json["mce_image"] = '<img src="' . url("/storage/{$image}") . '" alt="" title="{title}">';
            echo json_encode($json);
            return;
        }

        //MCE Upload Arch
        if (!empty($data["upload_file"]) && !empty($_FILES["arch"])) {
            $files = $_FILES["arch"];
            $upload = new Upload();
            $file = $upload->file($files, "post-" . time());

            if (!$file) {
                $json["message"] = $upload->message()->render();
                echo json_encode($json);
                return;
            }

            $json["mce_file"] = '<a style="color:#fff;background-color:#db2242;border-radius:10px;padding:.3em .8em;text-decoration:none;margin:0 5px" href="' . url("/storage/{$file}") . '" alt="{title}" title="{title}" target="_blank">BAIXAR ARQUIVO</a>';
            echo json_encode($json);
            return;
        }

        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $content = $data["content"];
            $seemore = ($data['seemore'] == '')? NULL : $data['seemore'];
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $postCreate = new Post();
            $postCreate->author = $data["author"];
            $postCreate->category = $data["category"];
            $postCreate->title = $data["title"];
            $postCreate->uri = str_slug($postCreate->title);
            $postCreate->subtitle = $data["subtitle"];
            $postCreate->content = str_replace(["{title}"], [$postCreate->title], $content);
            $postCreate->video = $data["video"];
            $postCreate->poll = ($data["poll"] == '0')? NULL : $data["poll"];
            $postCreate->gallery = ($data["gallery"] == '0')? NULL : $data["gallery"];
            $postCreate->seemore = $seemore;
            $postCreate->status = $data["status"];
            $postCreate->tag = $data["tag"];
            $postCreate->type = 'post';
            $postCreate->post_at = date_fmt_back($data["post_at"]);

            //upload cover
            if (!empty($_FILES["cover"])) {
                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $postCreate->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->cover = $image;
            }

            if (!$postCreate->save()) {
                $json["message"] = $postCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Post publicado com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/blog/post/{$postCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $content = $data["content"];
            $seemore = ($data['seemore'] == '')? NULL : $data['seemore'];
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $postEdit = (new Post())->findById($data["post_id"]);

            if (!$postEdit) {
                $this->message->error("Você tentou atualizar um post que não existe ou foi removido")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/blog/home")]);
                return;
            }

            $postEdit->author = $data["author"];
            $postEdit->category = $data["category"];
            $postEdit->title = $data["title"];
            $postEdit->uri = str_slug($postEdit->title);
            $postEdit->subtitle = $data["subtitle"];
            $postEdit->content = str_replace(["{title}"], [$postEdit->title], $content);
            $postEdit->video = $data["video"];
            $postEdit->poll = ($data["poll"] == '0')? NULL : $data["poll"];
            $postEdit->gallery = ($data["gallery"] == '0')? NULL : $data["gallery"];
            $postEdit->seemore = $seemore;
            $postEdit->status = $data["status"];
            $postEdit->tag = $data["tag"];
            $postEdit->post_at = date_fmt_back($data["post_at"]);

            //upload cover
            if (!empty($_FILES["cover"])) {
                if ($postEdit->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover}");
                    (new Thumb())->flush($postEdit->cover);
                }

                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $postEdit->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->cover = $image;
            }

            if (!$postEdit->save()) {
                $json["message"] = $postEdit->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Post atualizado com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $postDelete = (new Post())->findById($data["post_id"]);

            if (!$postDelete) {
                $this->message->error("Você tentou excluir um post que não existe ou já foi removido")->flash();
                echo json_encode(["reload" => true]);
                return;
            }

            if ($postDelete->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover}");
                (new Thumb())->flush($postDelete->cover);
            }

            $postDelete->destroy();
            $this->message->success("O post foi excluído com sucesso...")->flash();

            echo json_encode(["reload" => true]);
            return;
        }

        //Leixeira
        if (!empty($data["action"]) && $data["action"] == "lixeira") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $postLixeira = (new Post())->findById($data["post_id"]);

            if (!$postLixeira) {
                $this->message->error("Você tentou apagar um post que não existe ou foi removido")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/blog/home")]);
                return;
            }

            $postLixeira->status = "trash";

            if (!$postLixeira->save()) {
                $json["message"] = $postLixeira->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Post enviado para Lixeira")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        $postEdit = null;
        if (!empty($data["post_id"])) {
            $postId = filter_var($data["post_id"], FILTER_VALIDATE_INT);
            $postEdit = (new Post())->findById($postId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($postEdit->title ?? "Novo Artigo"),
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        $permit_categories = str_replace(",", "','", user()->categories);

        echo $this->view->render("widgets/blog/post", [
            "app" => "blog/post",
            "head" => $head,
            "post" => $postEdit,
            "categories" => (new Category())->find("type = :type && uri IN ('{$permit_categories}')", "type=post")->order("title")->fetch(true),
            "authors" => (new User())->find("level >= :level", "level=5")->fetch(true),
            "gallery" => (new Gallery())->findGallery("entertainment = :e", "e=no")->order("date_at DESC")->limit(10)->fetch(true),
            "enquete" => (new Channel())->find("status = :status && created_at < NOW() && expire_at > NOW()", "status=post")->order("created_at")->fetch(true)
        ]);
    }

    /**
     * @param array|null $data
     */
    public function categories(?array $data): void
    {
        $categories = (new Category())->find("parent IS NULL");
        $pager = new Pager(url("/".PATH_ADMIN."/blog/categories/"));
        $pager->pager($categories->count(), 11, (!empty($data["page"]) ? $data["page"] : 1));

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Categorias",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/blog/categories", [
            "app" => "blog/categories",
            "head" => $head,
            "categories" => $categories->order("title")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function category(?array $data): void
    {
        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $categoryCreate = new Category();
            $categoryCreate->title = $data["title"];
            $categoryCreate->parent = ($data["parent"] == 'null')? NULL : $data["parent"];
            $categoryCreate->uri = str_slug($categoryCreate->title);
            $categoryCreate->description = $data["description"];
            $categoryCreate->menu = $data["menu"];
            $categoryCreate->color = $data["color"];

            //upload cover
            if (!empty($_FILES["cover"])) {
                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $categoryCreate->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $categoryCreate->cover = $image;
            }

            if (!$categoryCreate->save()) {
                $json["message"] = $categoryCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Categoria criada com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/blog/category/{$categoryCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $categoryEdit = (new Category())->findById($data["category_id"]);

            if (!$categoryEdit) {
                $this->message->error("Você tentou editar uma categoria que não existe ou foi removida")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/blog/categories")]);
                return;
            }

            $categoryEdit->title = $data["title"];
            $categoryEdit->parent = ($data["parent"] == 'null')? NULL : $data["parent"];
            $categoryEdit->uri = str_slug($categoryEdit->title);
            $categoryEdit->description = $data["description"];
            $categoryEdit->menu = $data["menu"];
            $categoryEdit->color = $data["color"];

            //upload cover
            if (!empty($_FILES["cover"])) {
                if ($categoryEdit->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$categoryEdit->cover}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$categoryEdit->cover}");
                    (new Thumb())->flush($categoryEdit->cover);
                }

                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $categoryEdit->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $categoryEdit->cover = $image;
            }

            if (!$categoryEdit->save()) {
                $json["message"] = $categoryEdit->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Categoria atualizada com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $categoryDelete = (new Category())->findById($data["category_id"]);

            if (!$categoryDelete) {
                $json["message"] = $this->message->error("A categoria não existe ou já foi excluída antes")->render();
                echo json_encode($json);
                return;
            }

            if ($categoryDelete) {
                $json["message"] = $this->message->warning("Não é possível remover pois existem posts cadastrados")->render();
                echo json_encode($json);
                return;
            }

            if ($categoryDelete->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$categoryDelete->cover}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$categoryDelete->cover}");
                (new Thumb())->flush($categoryDelete->cover);
            }

            $categoryDelete->destroy();

            $this->message->success("A categoria foi excluída com sucesso...")->flash();
            echo json_encode(["reload" => true]);

            return;
        }

        $categoryEdit = null;
        if (!empty($data["category_id"])) {
            $categoryId = filter_var($data["category_id"], FILTER_VALIDATE_INT);
            $cat = (new Category());
            $categoryEdit = $cat->findById($categoryId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Categoria",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/blog/category", [
            "app" => "blog/categories",
            "head" => $head,
            "category" => $categoryEdit,
            "categories" => (new Category())->find("parent IS NULL")->order("title")->fetch(true)
        ]);
    }

    /**
     * LOAD MORE
     */
    public function loadMoreNews(): void
    {
        $getData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        $setData = array_map("strip_tags", $getData);
        $setData = array_map("trim", $setData);
        
        $read = (new Post())
                    ->find("MATCH(title, subtitle) AGAINST(:s) && status = :status", "s={$setData['search']}&status=post")
                    ->order("post_at DESC")
                    ->limit(20)
                    ->fetch(true);
                if ($read):
                    foreach ($read as $news):
                        $linkEnt = ($news->category()->id == 10 || $news->category()->parent == 10 || $news->category()->id == 36)? 'artigos-entretenimento' : 'artigo';
                        
                        $value = "<a href=\"".url("/{$linkEnt}/{$news->uri}")."\" title=\"{$news->title}\" target=\"_blank\"><h3>{$news->title}</h3></a>";
                        $jSon[] = ["value"=>$value,"label"=>$news->title];
                    endforeach;
                else:
                    $jSon[] = 'Nada encontrado';
                endif;

        echo json_encode($jSon);
    }

    /**
     * LOAD MORE
     */
    public function loadMoreNewsCategory(): void
    {
        $getData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        $setData = array_map("strip_tags", $getData);
        $setData = array_map("trim", $setData);
        
        $read = (new Post())
                    ->find("status = :s && category = :c","s=post&c={$setData['category']}")
                    ->order("post_at DESC")
                    ->limit(10)
                    ->fetch(true);
                if ($read):
                    foreach ($read as $news):
                        $linkEnt = ($news->category()->id == 10 || $news->category()->parent == 10 || $news->category()->id == 36)? 'artigos-entretenimento' : 'artigo';
                        
                        $value = "<a href=\"".url("/{$linkEnt}/{$news->uri}")."\" title=\"{$news->title}\" target=\"_blank\"><h3>{$news->title}</h3></a>";
                        $jSon[] = ["value"=>$value,"label"=>str_replace('&#39;',"'",$news->title)];
                    endforeach;
                else:
                    $jSon[] = 'Nada encontrado';
                endif;

        echo json_encode($jSon);
    }
}