<?php

namespace Source\App\Admin;

use Source\Models\Top10;
use Source\Models\User;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;

/**
 * Class Users
 * @package Source\App\Admin
 */
class Top extends Admin
{
    /**
     * Users constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/top/home/{$s}/1")]);
            return;
        }

        $search = null;
        $top = (new Top10())->find();

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $top = (new Top10())->find("MATCH(title) AGAINST(:s)", "s={$search}");
            if (!$top->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/top/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/top/home/{$all}/"));
        $pager->pager($top->count(), 12, (!empty($data["page"]) ? $data["page"] : 1));

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Top 10",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/top/home", [
            "app" => "top/home",
            "head" => $head,
            "search" => $search,
            "top" => $top->order("position")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function top10(?array $data): void
    {
        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $publicityCreate = new Publicity();
            $publicityCreate->name = $data["name"];
            $publicityCreate->link = (empty($data["link"]))? '#' : $data["link"];
            $publicityCreate->page = implode(',',$data['page']);
            $publicityCreate->local = $data["local"];
            $publicityCreate->publicity_at = date_fmt_back($data["publicity_at"]);
            $publicityCreate->status = $data["status"];

            //upload photo
            if (!empty($_FILES["photo"])) {
                $files = $_FILES["photo"];
                $upload = new Upload();
                $image = $upload->image($files, $publicityCreate->name, 1024);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $publicityCreate->photo = $image;
            }



            if (!$publicityCreate->save()) {
                $json["message"] = $publicityCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Publicidade cadastrada com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/ads/publicity/{$publicityCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $top10Update = (new Top10())->findById($data["top10_id"]);

            if (!$top10Update) {
                $this->message->error("Você tentou gerenciar um Top10 que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/top/home")]);
                return;
            }

            $top10Update->title = $data["title"];
            $top10Update->link = $data["link"];
            $top10Update->status = $data["status"];

            if (!$top10Update->save()) {
                $json["message"] = $top10Update->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Top 10 atualizada com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $publicityDelete = (new Publicity())->findById($data["top10_id"]);

            if (!$publicityDelete) {
                $this->message->error("Você tentnou deletar uma publicidade que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/ads/home")]);
                return;
            }

            if ($publicityDelete->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$publicityDelete->photo}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$publicityDelete->photo}");
                (new Thumb())->flush($publicityDelete->photo);
            }

            $publicityDelete->destroy();

            $this->message->success("A publicidade foi excluída com sucesso...")->flash();
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/ads/home")]);

            return;
        }

        $top10Edit = null;
        if (!empty($data["top10_id"])) {
            $top10Id = filter_var($data["top10_id"], FILTER_VALIDATE_INT);
            $top10Edit = (new Top10())->findById($top10Id);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($top10Edit ? "Top 10 na posição {$top10Edit->position}" : "Novo T0 10"),
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/top/top10", [
            "app" => "top/top10",
            "head" => $head,
            "top10" => $top10Edit
        ]);
    }
}