<?php

namespace Source\App\Admin;

use Source\Models\Category;
use Source\Models\Post;
use Source\Models\User;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;

/**
 * Class Pages
 * @package Source\App\Admin
 */
class Pages extends Admin
{
    /**
     * Pages constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/pages/home/{$s}/1")]);
            return;
        }

        //category redirect
        if(!isset($_SESSION['author'])){
            $_SESSION['author'] = null;
        }
        if (!empty($data["auth"])) {
            if($data['auth'] == "null"){
                $_SESSION['author'] = null;
            } else {
                $auth = str_search($data["auth"]);
                $_SESSION['author'] = $auth;
            }
            echo json_encode(["reload" => true]);
            return;
        }

        $where_author = ($_SESSION['author'] != null)? " && author = {$_SESSION['author']}" : '';

        $search = null;
        $posts = (new Post())->find("type = :type && status != :s{$where_author}",'type=page&s=trash');

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $posts = (new Post())->find("MATCH(title, subtitle) AGAINST(:s) && type = :t", "s={$search}&t=page");
            if (!$posts->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/pages/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/pages/home/{$all}/"));
        $pager->pager($posts->count(), 12, (!empty($data["page"]) ? $data["page"] : 1));

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Páginas",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/pages/home", [
            "app" => "pages/home",
            "head" => $head,
            "posts" => $posts->limit($pager->limit())->offset($pager->offset())->order("post_at DESC")->fetch(true),
            "paginator" => $pager->render(),
            "search" => $search,
            "select_author" => $_SESSION['author'],
            "list_author" => (new User())->find("status = :status && level = :level","status=confirmed&level=7")
                ->order('first_name')
                ->fetch(true)
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function post(?array $data): void
    {
        //MCE Upload
        if (!empty($data["upload"]) && !empty($_FILES["image"])) {
            $files = $_FILES["image"];
            $upload = new Upload();
            $image = $upload->image($files, "post-" . time());

            if (!$image) {
                $json["message"] = $upload->message()->render();
                echo json_encode($json);
                return;
            }

            $json["mce_image"] = '<img style="width: 100%;" src="' . url("/storage/{$image}") . '" alt="{title}" title="{title}">';
            echo json_encode($json);
            return;
        }

        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $content = $data["content"];
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $postCreate = new Post();
            $postCreate->title = $data["title"];
            $postCreate->uri = str_slug($postCreate->title);
            $postCreate->subtitle = $data["title"];
            $postCreate->content = str_replace(["{title}"], [$postCreate->title], $content);
            $postCreate->status = $data["status"];
            $postCreate->type = 'page';
            $postCreate->tag = ':';
            $postCreate->post_at = date_fmt_back($data["post_at"]);

            //upload cover
            if (!empty($_FILES["cover"])) {
                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $postCreate->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->cover = $image;
            }

            if (!$postCreate->save()) {
                $json["message"] = $postCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Página publicada com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/pages/post/{$postCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $content = $data["content"];
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $postEdit = (new Post())->findById($data["post_id"]);

            if (!$postEdit) {
                $this->message->error("Você tentou atualizar um post que não existe ou foi removido")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/column/home")]);
                return;
            }

            $postEdit->title = $data["title"];
            $postEdit->subtitle = $data["title"];
            $postEdit->uri = str_slug($postEdit->title);
            $postEdit->content = str_replace(["{title}"], [$postEdit->title], $content);
            $postEdit->status = $data["status"];
            $postEdit->post_at = date_fmt_back($data["post_at"]);

            //upload cover
            if (!empty($_FILES["cover"])) {
                if ($postEdit->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover}");
                    (new Thumb())->flush($postEdit->cover);
                }

                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $postEdit->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->cover = $image;
            }

            if (!$postEdit->save()) {
                $json["message"] = $postEdit->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Página atualizada com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $postDelete = (new Post())->findById($data["post_id"]);

            if (!$postDelete) {
                $this->message->error("Você tentou excluir uma página que não existe ou já foi removida")->flash();
                echo json_encode(["reload" => true]);
                return;
            }

            if ($postDelete->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover}");
                (new Thumb())->flush($postDelete->cover);
            }

            $postDelete->destroy();
            $this->message->success("a Pagina foi excluída com sucesso...")->flash();

            echo json_encode(["reload" => true]);
            return;
        }

        //lixeira
        if (!empty($data["action"]) && $data["action"] == "lixeira") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $postLixeira = (new Post())->findById($data["post_id"]);

            if (!$postLixeira) {
                $this->message->error("Você tentou apagar uma página que não existe ou foi removido")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/pages/home")]);
                return;
            }

            $postLixeira->status = "trash";

            if (!$postLixeira->save()) {
                $json["message"] = $postLixeira->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Página enviada para Lixeira")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        $postEdit = null;
        if (!empty($data["post_id"])) {
            $postId = filter_var($data["post_id"], FILTER_VALIDATE_INT);
            $postEdit = (new Post())->findById($postId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($postEdit->title ?? "Nova Página - Páginas"),
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/pages/post", [
            "app" => "pages/post",
            "head" => $head,
            "post" => $postEdit
        ]);
    }
}