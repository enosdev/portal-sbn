<?php

namespace Source\App\Admin;

use Source\Models\Stopwatch;
use Source\Models\User;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;

/**
 * Class Users
 * @package Source\App\Admin
 */
class Timer extends Admin
{
    /**
     * Users constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/timer/home/{$s}/1")]);
            return;
        }

        $search = null;
        $timer = (new Stopwatch())->find();

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $timer = (new Stopwatch())->find("MATCH(title) AGAINST(:s)", "s={$search}");
            if (!$timer->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/timer/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/timer/home/{$all}/"));
        $pager->pager($timer->count(), 12, (!empty($data["page"]) ? $data["page"] : 1));

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Cronômetro",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/timer/home", [
            "app" => "timer/home",
            "head" => $head,
            "search" => $search,
            "timer" => $timer->order("title, event_at")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function stopwatch(?array $data): void
    {
        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $stopwatchCreate = new Stopwatch();
            $stopwatchCreate->title = $data["title"];
            $stopwatchCreate->description = $data["description"];
            $stopwatchCreate->event_at = date_fmt_back($data["event_at"]);
            $stopwatchCreate->status = $data["status"];

            //upload photo
            if (!empty($_FILES["photo"])) {
                $files = $_FILES["photo"];
                $upload = new Upload();
                $image = $upload->image($files, $stopwatchCreate->title, 300);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $stopwatchCreate->cover = $image;
            }



            if (!$stopwatchCreate->save()) {
                $json["message"] = $stopwatchCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Cronômetro cadastrado com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/timer/stopwatch/{$stopwatchCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $stopwatchUpdate = (new Stopwatch())->findById($data["stopwatch_id"]);

            if (!$stopwatchUpdate) {
                $this->message->error("Você tentou gerenciar um Cronômetro que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/timer/home")]);
                return;
            }

            $stopwatchUpdate->title = $data["title"];
            $stopwatchUpdate->description = $data["description"];
            $stopwatchUpdate->event_at = date_fmt_back($data["event_at"]);
            $stopwatchUpdate->status = $data["status"];

            //upload photo
            if (!empty($_FILES["photo"])) {
                if ($stopwatchUpdate->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$stopwatchUpdate->cover}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$stopwatchUpdate->cover}");
                    (new Thumb())->flush($stopwatchUpdate->cover);
                }

                $files = $_FILES["photo"];
                $upload = new Upload();
                $image = $upload->image($files, $stopwatchUpdate->title, 300);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $stopwatchUpdate->cover = $image;
            }

            if (!$stopwatchUpdate->save()) {
                $json["message"] = $stopwatchUpdate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Cronômetro atualizado com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $stopwatchDelete = (new Stopwatch())->findById($data["stopwatch_id"]);

            if (!$stopwatchDelete) {
                $this->message->error("Você tentou deletar um Cronômetro que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/timer/home")]);
                return;
            }

            if ($stopwatchDelete->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$stopwatchDelete->photo}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$stopwatchDelete->photo}");
                (new Thumb())->flush($stopwatchDelete->photo);
            }

            $stopwatchDelete->destroy();

            $this->message->success("O Cronônometro foi excluído com sucesso...")->flash();
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/timer/home")]);

            return;
        }

        $stopwatchEdit = null;
        if (!empty($data["stopwatch_id"])) {
            $stopwatchId = filter_var($data["stopwatch_id"], FILTER_VALIDATE_INT);
            $stopwatchEdit = (new Stopwatch())->findById($stopwatchId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($stopwatchEdit ? "Cronômetro de {$stopwatchEdit->title}" : "Novo Cronômetro"),
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/timer/stopwatch", [
            "app" => "timer/stopwatch",
            "head" => $head,
            "stopwatch" => $stopwatchEdit
        ]);
    }
}