<div class="main_sidebar">

    <h3><i class="fas fa-laptop"></i> dash/destaque</h3>
    <p class="dash_content_sidebar_desc">Aqui você gerencia os destaques da parte de entretenimento do portal...</p>
    

    <?php if (!empty($post->cover)):?>
        <div>
            <h2 style="font-size:var(--font-small);margin:26px 0 13px 0"><i class="fa fa-pencil"></i> Editar artigo#<?= $post->id; ?></h2>
            <a class="btn btn-green" href="<?= url("/destaques/{$post->uri}"); ?>" target="_blank" title=""><i class="fa fa-link"></i> Ver no
                site</a>
        </div>
        <div id="image-holder">
            <img class="radius" style="width: 100%; margin-top: 10px" src="<?= image($post->cover, 300); ?>"/>
        </div>
    <?php else: ?>
        <div id="image-holder"></div>
    <?php endif; ?>

    <?php if (!empty($post->cover2)):?>
        <div id="image-holder2">
            <img class="radius" style="width: 100%; margin-top: 10px" src="<?= image($post->cover2, 300); ?>"/>
        </div>
    <?php else: ?>
        <div id="image-holder2"></div>
    <?php endif; ?>

    <?php if (!empty($post->cover3)):?>
        <div id="image-holder3">
            <img class="radius" style="width: 100%; margin-top: 10px" src="<?= image($post->cover3, 300); ?>"/>
        </div>
    <?php else: ?>
        <div id="image-holder3"></div>
    <?php endif; ?>

    <?php if (!empty($post->cover4)):?>
        <div id="image-holder4">
            <img class="radius" style="width: 100%; margin-top: 10px" src="<?= image($post->cover4, 300); ?>"/>
        </div>
    <?php else: ?>
        <div id="image-holder4"></div>
    <?php endif; ?>

    <?php if (!empty($post->cover5)):?>
        <div id="image-holder5">
            <img class="radius" style="width: 100%; margin-top: 10px" src="<?= image($post->cover5, 300); ?>"/>
        </div>
    <?php else: ?>
        <div id="image-holder5"></div>
    <?php endif; ?>

    <?php if (!empty($post->businessperson_photo)):?>
        <div id="image-holder_businessperson">
            <img class="radius" style="width: 100%; margin-top: 10px" src="<?= image($post->businessperson_photo, 300); ?>"/>
        </div>
    <?php else: ?>
        <div id="image-holder-businessperson"></div>
    <?php endif; ?>

    <?php if (!empty($post->businessperson_photo2)):?>
        <div id="image-holder_businessperson2">
            <img class="radius" style="width: 100%; margin-top: 10px" src="<?= image($post->businessperson_photo2, 300); ?>"/>
        </div>
    <?php else: ?>
        <div id="image-holder-businessperson2"></div>
    <?php endif; ?>
</div>
