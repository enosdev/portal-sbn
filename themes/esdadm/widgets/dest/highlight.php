<?php $v->layout("_admin"); ?>
<div class="desc"><i class="fa fa-edit"></i> Destaques</div>

<main>
    <?php $v->insert("widgets/dest/sidebar.php"); ?>
    <?php if (!$post): ?>
        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/dest/highlight"); ?>" method="post">
                <!-- ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Destaque:</span>
                        <input type="text" name="title_highlight" placeholder="Título do destaque" required/>
                    </label>
                </div>
                
                <label class="label">
                    <span class="legend">*Título:</span>
                    <input type="text" name="title" placeholder="Chamada principal" required/>
                </label>

                <label class="label">
                    <span class="legend">*Nome:</span>
                    <input type="text" name="name" placeholder="Nome da pessoa" required/>
                    <input style="height:30px" type="color" name="nameColor" value="#ffffff">
                </label>

                <label class="label">
                    <span class="legend">*Ficha Técnica: <small style="font-weight:100;color:red">separar com " : " Ex => Fotografia: Fulado da Silva</small></span>
                    <div class="remov"><span>x</span><input type="text" name="datasheet[]" placeholder="Fotografia: Fulado da Silva" required/></div>
                    <div class="add"></div>
                    <input style="height:30px" type="color" name="datasheetColor" value="#ffffff">
                    <div id="add" style="width:90px; font-size:.8em;background-color: purple; text-align:center;padding:5px;margin-top:5px;color:#fff;border-radius:6px;cursor:pointer">adicionar</div>
                </label>

                <label class="label">
                    <span class="legend">Foto Principal: (<small style="font-weight:100;color:red">687px por 454px</small>)</span>
                    <input id="file" type="file" name="cover" placeholder="Uma imagem de capa"/>
                </label>

                <label class="label">
                    <span class="legend">*Subtítulo 1:</span>
                    <input type="text" name="title2" placeholder="Título do primeiro tópico" required/>
                </label>

                <label class="label">
                    <span class="legend">*Conteúdo subtítulo 1:</span>
                    <textarea class="mce" name="content"></textarea>
                    <div class="content" style="color: red">0</div>
                </label>

                <label class="label">
                    <span class="legend">Foto subtítulo 1: (<small style="font-weight:100;color:red">515px por 863px</small>)</span>
                    <input id="file2" type="file" name="cover2"/>
                </label>

                <label class="label">
                    <span class="legend">Foto 1: (<small style="font-weight:100;color:red">515px por 533px</small>)</span>
                    <input id="file3" type="file" name="cover3"/>
                </label>

                <label class="label">
                    <span class="legend">Foto 2: (<small style="font-weight:100;color:red">515px por 470px</small>)</span>
                    <input id="file4" type="file" name="cover4"/>
                </label>

                <label class="label">
                    <span class="legend">*Subtítulo 2:</span>
                    <input type="text" name="title3" placeholder="Título do segundo tópico" required/>
                </label>

                <label class="label">
                    <span class="legend">*Conteúdo subtítulo 2:</span>
                    <textarea class="mce" name="content2"></textarea>
                    <div class="content2" style="color: red">0</div>
                </label>

                <span class="legend">PALAVRA POR PALAVRA:</span>
                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Deus?</span>
                        <input type="text" name="deus" placeholder="reposta" required/>
                    </label>
                    <label class="label">
                        <span class="legend">*Família?</span>
                        <input type="text" name="familia" placeholder="resposta" required/>
                    </label>                   
                </div>
                
                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Estudos?</span>
                        <input type="text" name="estudos" placeholder="reposta" required/>
                    </label>
                    <label class="label">
                        <span class="legend">*Trabalho?</span>
                        <input type="text" name="trabalho" placeholder="resposta" required/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Moda?</span>
                        <input type="text" name="moda" placeholder="reposta" required/>
                    </label>
                    <label class="label">
                        <span class="legend">*Vida?</span>
                        <input type="text" name="vida" placeholder="resposta" required/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Casamento?</span>
                        <input type="text" name="casamento" placeholder="reposta" required/>
                    </label>
                    <label class="label">
                        <span class="legend">*Portal SBN?</span>
                        <input type="text" name="portal" placeholder="resposta" required/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Foto 3: (<small style="font-weight:100;color:red">515px por 854px</small>)</span>
                    <input id="file5" type="file" name="cover5"/>
                </label>

                <label class="label">
                    <span class="legend">*Projeto SBN: <small style="font-weight:100;color:red">separar com " : " Ex => Ensaio: Portal SBN || Caso depois do " : " tiver 2 nomes separar com barra " / " Ex => Ensaio: Portal SBN / portalsbn.com.br</small></span>
                    <input style="margin-bottom:5px" type="text" name="project[]" placeholder="Nome do Projeto / Ex: Projeto Musa SBN" required/>
                    <div class="remov"><span>x</span><input type="text" name="project[]" placeholder="Ensaio: Portal SBN" required/></div>
                    <div class="add2"></div>
                    <input style="height:30px" type="color" name="projectColor" value="#ffffff">
                    <div id="add2" style="width:90px; font-size:.8em;background-color: purple; text-align:center;padding:5px;margin-top:5px;color:#fff;border-radius:6px;cursor:pointer">adicionar</div>
                </label>

                <label class="label">
                    <div id="image-holder-publi" style="width: 100px;"></div>
                    <span class="legend">Banner Publicidade: (<small style="font-weight:100;color:red">400px por 247px</small>)</span>
                    <input id="publicity" type="file" name="publicity"/>
                    <input type="text" name="publicityLink" placeholder="Link: https://www.nomedosite.com"/>
                </label>

                <label class="label">
                    <div id="image-holder-publi2" style="width: 100px;"></div>
                    <span class="legend">Banner Publicidade 2: (<small style="font-weight:100;color:red">400px por 247px</small>)</span>
                    <input id="publicity2" type="file" name="publicity2"/>
                    <input type="text" name="publicity2Link" placeholder="Link: https://www.nomedosite.com"/>
                </label>

                <label class="label">
                    <div id="image-holder-publi3" style="width: 100px;"></div>
                    <span class="legend">Banner Publicidade 3: (<small style="font-weight:100;color:red">400px por 247px</small>)</span>
                    <input id="publicity3" type="file" name="publicity3"/>
                    <input type="text" name="publicity3Link" placeholder="Link: https://www.nomedosite.com"/>
                </label>

                <div class="label_g2">
                    <label class="label" style="position:relative">
                        <span class="legend">Vincular Galeria:</span>
                        <select id="gallery" name="gallery">
                            <option value="0">Selecione uma Galeria</option>
                            <?php
                                foreach($gallery as $g):?>
                                    <option value="<?=$g->id;?>"><?=$g->title;?></option>
                            <?php endforeach;?>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">Vídeo:</span>
                        <input type="text" name="video" placeholder="O LINK de um vídeo do YouTube"/>
                    </label>
                </div>

                <span class="legend" style="color:red">EMPRESÁRIO(a):</span>
                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Destaque Empresário(a):</span>
                        <input type="text" name="businessperson_title" placeholder="Título do empersário(a)"/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Título:</span>
                    <input type="text" name="businessperson_title1" placeholder="Chamada principal"/>
                </label>

                <label class="label">
                    <span class="legend">Conteúdo:</span>
                    <textarea class="mce" name="businessperson_content"></textarea>
                    <div class="businessperson_content" style="color: red">0</div>
                </label>

                <label class="label">
                    <span class="legend">Foto1: (<small style="font-weight:100;color:red">900px por 540px</small>)</span>
                    <input id="businessperson_file" type="file" name="businessperson_photo"/>
                </label>

                <label class="label">
                    <span class="legend">Legenda1:</span>
                    <input type="text" name="businessperson_photo_subtitle" placeholder="legenda foto 1"/>
                    <input style="height:30px" type="color" name="businessperson_photo_subtitleColor" value="#000000">
                </label>

                <label class="label">
                    <span class="legend">Foto2: (<small style="font-weight:100;color:red">900px por 1200px</small>)</span>
                    <input id="businessperson_file2" type="file" name="businessperson_photo2"/>
                </label>

                <label class="label">
                    <span class="legend">Legenda2:</span>
                    <input type="text" name="businessperson_photo2_subtitle" placeholder="legenda foto 2"/>
                </label>

                <label class="label">
                    <div id="image-holder-publi4" style="width: 100px;"></div>
                    <span class="legend">Banner Empresário(a): (<small style="font-weight:100;color:red">450px por 150px</small>)</span>
                    <input id="publicity4" type="file" name="publicity4"/>
                    <input type="text" name="publicity4Link" placeholder="Link: https://www.nomedosite.com"/>
                </label>

                <label class="label">
                    <span class="legend">Título 2:</span>
                    <input type="text" name="businessperson_subtitle" placeholder="Titulo 2"/>
                </label>

                <label class="label">
                    <span class="legend">Conteúdo 2:</span>
                    <textarea class="mce" name="businessperson_content2"></textarea>
                    <div class="businessperson_content2" style="color: red">0</div>
                </label>

                <div class="label_g2">
                     <label class="label">
                        <span class="legend">Autor do Destaque:</span>
                        <input type="text" name="businessperson_name" placeholder="Nome do autor"/>
                    </label>
                    <label class="label">
                        <span class="legend">E-mail do autor do destaque:</span>
                        <input type="email" name="businessperson_email" placeholder="E-mail do autor"/>
                    </label>                    
                </div>

                <div class="label_g2">
                    <label class="label"></label>
                    <label class="label">
                        <span class="legend">*Data de publicação:</span>
                        <input class="mask-datetime datetimepicker" type="text" name="post_at" value="<?= date("d/m/Y H:i"); ?>"
                               required/>
                    </label>
                </div>

                <div class="label_g2">
                     <label class="label">
                        <span class="legend">*Autor:</span>
                        <select name="author" required>

                            <?php foreach ($authors as $author):
                                $authorId = user()->id;
                                $select = function ($value) use ($authorId) {
                                    return ($authorId == $value ? "selected" : "");
                                };
                                ?>
                                <option <?= $select($author->id); ?> value="<?= $author->id; ?>"><?= $author->fullName(); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <option value="post">Publicar</option>
                            <option value="draft">Rascunho</option>
                            <option value="trash">Lixo</option>
                        </select>
                    </label>                    
                </div>

                <div class="al-right">
                    <button class="btn btn-green"><i class="fa fa-save"></i>Publicar</button>
                </div>
            </form>
        </div>
    <?php else: ?>
        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/dest/highlight/{$post->id}"); ?>" method="post">
                <!-- ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Destaque:</span>
                        <input type="text" name="title_highlight" value="<?= $post->title_highlight; ?>" placeholder="Título do destaque" required/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">*Título:</span>
                    <input type="text" name="title" value="<?= $post->title; ?>"  placeholder="Chamada principal" required/>
                </label>

                <label class="label">
                    <span class="legend">*Nome:</span>
                    <input type="text" name="name" value="<?= explode('~',$post->name)[0]; ?>" placeholder="Nome da pessoa" required/>
                    <input style="height:30px" type="color" name="nameColor" value="<?= explode('~',$post->name)[1]; ?>">
                </label>

                <label class="label">
                    <span class="legend">*Ficha Técnica: <small style="font-weight:100;color:red">separar com " : " Ex => Fotografia: Fulado da Silva</small></span>
                    <?php
                        $corDatasheet = explode('~',$post->datasheet);
                        $datasheet = explode('!!',$corDatasheet[0]);
                        foreach($datasheet as $datasheet_dados):?>
                            <div class="remov"><span>x</span><input style="margin:5px 0" type="text" name="datasheet[]" value="<?=$datasheet_dados;?>" placeholder="Fotografia: Fulado da Silva" required/></div>
                    <?php        
                        endforeach;?>
                    <div class="add"></div>
                    <input style="height:30px" type="color" name="datasheetColor" value="<?= $corDatasheet[1];?>">
                    <div id="add" style="width:90px; font-size:.8em;background-color: purple; text-align:center;padding:5px;margin-top:5px;color:#fff;border-radius:6px;cursor:pointer">adicionar</div>
                </label>

                <label class="label">
                    <span class="legend">Foto Principal: (<small style="font-weight:100;color:red">687px por 454px</small>)</span>
                    <input id="file" type="file" name="cover" placeholder="Uma imagem de capa"/>
                </label>

                <label class="label">
                    <span class="legend">*Subtítulo 1:</span>
                    <input type="text" name="title2" value="<?=$post->title2;?>" placeholder="Título do primeiro tópico" required/>
                </label>

                <label class="label">
                    <span class="legend">*Conteúdo subtítulo 1:</span>
                    <textarea class="mce" name="content"><?= $post->content; ?></textarea>
                    <div class="content" style="color: red">0</div>
                </label>

                <label class="label">
                    <span class="legend">Foto subtítulo 1: (<small style="font-weight:100;color:red">515px por 863px</small>)</span>
                    <input id="file2" type="file" name="cover2"/>
                </label>

                <label class="label">
                    <span class="legend">Foto 1: (<small style="font-weight:100;color:red">515px por 533px</small>)</span>
                    <input id="file3" type="file" name="cover3"/>
                </label>

                <label class="label">
                    <span class="legend">Foto 2: (<small style="font-weight:100;color:red">515px por 470px</small>)</span>
                    <input id="file4" type="file" name="cover4"/>
                </label>

                <label class="label">
                    <span class="legend">*Subtítulo 2:</span>
                    <input type="text" name="title3" value="<?=$post->title3;?>" placeholder="Título do segundo tópico" required/>
                </label>

                <label class="label">
                    <span class="legend">*Conteúdo subtítulo 2:</span>
                    <textarea class="mce" name="content2" ><?=$post->content2;?></textarea>
                    <div class="content2" style="color: red">0</div>
                </label>

                <span class="legend">PALAVRA POR PALAVRA:</span>
                <?php
                    $valores = explode("!!",$post->word_by_word);
                ?>
                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Deus?</span>
                        <input type="text" name="deus" value="<?=$valores[0];?>" placeholder="reposta" required/>
                    </label>
                    <label class="label">
                        <span class="legend">*Família?</span>
                        <input type="text" name="familia" value="<?=$valores[1];?>" placeholder="resposta" required/>
                    </label>                   
                </div>
                
                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Estudos?</span>
                        <input type="text" name="estudos" value="<?=$valores[2];?>" placeholder="reposta" required/>
                    </label>
                    <label class="label">
                        <span class="legend">*Trabalho?</span>
                        <input type="text" name="trabalho" value="<?=$valores[3];?>" placeholder="resposta" required/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Moda?</span>
                        <input type="text" name="moda" value="<?=$valores[4];?>" placeholder="reposta" required/>
                    </label>
                    <label class="label">
                        <span class="legend">*Vida?</span>
                        <input type="text" name="vida" value="<?=$valores[5];?>" placeholder="resposta" required/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Casamento?</span>
                        <input type="text" name="casamento" value="<?=$valores[6];?>" placeholder="reposta" required/>
                    </label>
                    <label class="label">
                        <span class="legend">*Portal SBN?</span>
                        <input type="text" name="portal" value="<?=$valores[7];?>" placeholder="resposta" required/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Foto 3: (<small style="font-weight:100;color:red">515px por 854px</small>)</span>
                    <input id="file5" type="file" name="cover5"/>
                </label>

                <label class="label">
                    <span class="legend">*Projeto SBN: <small style="font-weight:100;color:red">separar com " : " Ex => Ensaio: Portal SBN || Caso depois do " : " tiver 2 nomes separar com barra " / " Ex => Ensaio: Portal SBN / portalsbn.com.br</small></span>
                    <?php
                    $corProject = explode('~', $post->project);
                    $arr = explode('!!', $corProject[0]);
                    echo "<input style=\"margin-bottom:5px\" type=\"text\" name=\"project[]\" value=\"{$arr[0]}\" placeholder=\"Nome do Projeto / Ex: Projeto Musa SBN\" required/>";
                    array_shift($arr);
                        foreach( $arr as $project):?>
                            <div class="remov"><span>x</span><input style="margin:5px 0" type="text" name="project[]" value="<?=$project;?>" placeholder="Ensaio: Portal SBN" required/></div>
                    <?php
                        endforeach;
                    ?>
                    
                    <div class="add2"></div>
                    <input style="height:30px" type="color" name="projectColor" value="<?= $corProject[1];?>">
                    <div id="add2" style="width:90px; font-size:.8em;background-color: purple; text-align:center;padding:5px;margin-top:5px;color:#fff;border-radius:6px;cursor:pointer">adicionar</div>
                </label>

                <label class="label">
                    <div id="image-holder-publi" style="width: 100px;">
                        <?php
                            $publicity = explode('~',$post->publicity);
                        ?>
                        <img class="radius" style="width: 100%; margin-top: 5px" src="<?= image($publicity[0], 100); ?>"/>
                    </div>
                    <span class="legend">Banner Publicidade: (<small style="font-weight:100;color:red">400px por 247px</small>)</span>
                    <input id="publicity" type="file" name="publicity"/>
                    <input type="hidden" name="publicityImage" value="<?= $publicity[0]; ?>"/>
                    <input type="text" name="publicityLink" placeholder="Link: https://www.nomedosite.com" value="<?=$publicity[1];?>"/>
                </label>

                <label class="label">
                    <div id="image-holder-publi2" style="width: 100px;">
                        <?php
                            $publicity2 = explode('~',$post->publicity2);
                        ?>
                        <img class="radius" style="width: 100%; margin-top: 5px" src="<?= image($publicity2[0], 100); ?>"/>
                    </div>
                    <span class="legend">Banner Publicidade 2: (<small style="font-weight:100;color:red">400px por 247px</small>)</span>
                    <input id="publicity2" type="file" name="publicity2"/>
                    <input type="hidden" name="publicity2Image" value="<?= $publicity2[0]; ?>"/>
                    <input type="text" name="publicity2Link" placeholder="Link: https://www.nomedosite.com" value="<?=$publicity2[1];?>"/>
                </label>

                <label class="label">
                    <div id="image-holder-publi3" style="width: 100px;">
                        <?php
                            $publicity3 = explode('~',$post->publicity3);
                        ?>
                        <img class="radius" style="width: 100%; margin-top: 5px" src="<?= image($publicity3[0], 100); ?>"/>
                    </div>
                    <span class="legend">Banner Publicidade 3: (<small style="font-weight:100;color:red">400px por 247px</small>)</span>
                    <input id="publicity3" type="file" name="publicity3"/>
                    <input type="hidden" name="publicity3Image" value="<?= $publicity3[0]; ?>"/>
                    <input type="text" name="publicity3Link" placeholder="Link: https://www.nomedosite.com" value="<?=$publicity3[1];?>"/>
                </label>

                <div class="label_g2">
                    <label class="label" style="position:relative">
                        <span class="legend">Vincular Galeria: </span>
                        <select id="gallery" name="gallery">
                            <option value="0">Selecione uma Galeria</option>
                            <?php
                                $selectGallerryId = $post->gallery;
                                $select = function ($value) use ($selectGallerryId) {
                                    return ($selectGallerryId == $value ? "selected" : "");
                                };
                                foreach($gallery as $g):?>
                                    <option <?= $select($g->id); ?> value="<?=$g->id;?>"><?=$g->title;?></option>
                            <?php endforeach;?>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">Vídeo:</span>
                        <input type="text" name="video" value="<?= $post->video; ?>" placeholder="O LINK de um vídeo do YouTube"/>
                    </label>
                </div>

                <span class="legend" style="color:red">EMPRESÁRIO(a):</span>
                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Destaque Empresário(a):</span>
                        <input type="text" name="businessperson_title" value="<?= $post->businessperson_title; ?>" placeholder="Título do empersário(a)"/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Título:</span>
                    <input type="text" name="businessperson_title1" value="<?= $post->businessperson_title1; ?>"  placeholder="Chamada principal"/>
                </label>

                <label class="label">
                    <span class="legend">Conteúdo:</span>
                    <textarea class="mce" name="businessperson_content"><?= $post->businessperson_content; ?></textarea>
                    <div class="businessperson_content" style="color: red">0</div>
                </label>

                <label class="label">
                    <span class="legend">Foto1: (<small style="font-weight:100;color:red">900px por 540px</small>)</span>
                    <input id="businessperson_file" type="file" name="businessperson_photo"/>
                </label>

                <label class="label">
                    <span class="legend">Legenda1:</span>
                    <input type="text" name="businessperson_photo_subtitle" value="<?= explode('~',$post->businessperson_photo_subtitle)[0]; ?>"  placeholder="legenda foto 1"/>
                    <input style="height:30px" type="color" name="businessperson_photo_subtitleColor" value="<?= explode('~',$post->businessperson_photo_subtitle)[1]; ?>">
                </label>

                <label class="label">
                    <span class="legend">Foto2: (<small style="font-weight:100;color:red">900px por 1200px</small>)</span>
                    <input id="businessperson_file2" type="file" name="businessperson_photo2"/>
                </label>

                <label class="label">
                    <span class="legend">Legenda1:</span>
                    <input type="text" name="businessperson_photo2_subtitle" value="<?= $post->businessperson_photo2_subtitle; ?>"  placeholder="legenda foto 2"/>
                </label>

                <label class="label">
                    <div id="image-holder-publi4" style="width: 100px;">
                        <?php
                            $publicity4 = explode('~',$post->publicity4);
                        ?>
                        <img class="radius" style="width: 100%; margin-top: 5px" src="<?= image($publicity4[0], 100); ?>"/>
                    </div>
                    <span class="legend">Banner Empresário(a): (<small style="font-weight:100;color:red">450px por 150px</small>)</span>
                    <input id="publicity4" type="file" name="publicity4"/>
                    <input type="hidden" name="publicity4Image" value="<?= $publicity4[0]; ?>"/>
                    <input type="text" name="publicity4Link" placeholder="Link: https://www.nomedosite.com" value="<?=$publicity4[1];?>"/>
                </label>

                <label class="label">
                    <span class="legend">Título 2:</span>
                    <input type="text" name="businessperson_subtitle" value="<?= $post->businessperson_subtitle; ?>"  placeholder="Titulo 2"/>
                </label>

                <label class="label">
                    <span class="legend">Conteúdo 2:</span>
                    <textarea class="mce" name="businessperson_content2"><?= $post->businessperson_content2; ?></textarea>
                    <div class="businessperson_content2" style="color: red">0</div>
                </label>

                <div class="label_g2">
                    <?php
                        $aut = explode("!!", $post->businessperson_author);
                    ?>
                     <label class="label">
                        <span class="legend">Autor do Destaque:</span>
                        <input type="text" name="businessperson_name" value="<?=$aut[0];?>" placeholder="Nome do autor"/>
                    </label>
                    <label class="label">
                        <span class="legend">E-mail do autor do destaque:</span>
                        <input type="email" name="businessperson_email" value="<?=$aut[1] ?? '';?>" placeholder="E-mail do autor"/>
                    </label>                    
                </div>

                <div class="label_g2">
                    <label class="label"></label>
                    <label class="label">
                        <span class="legend">*Data de publicação:</span>
                        <input class="mask-datetime datetimepicker" type="text" name="post_at"
                               value="<?= date_fmt($post->post_at, "d/m/Y H:i"); ?>" required/>
                    </label>
                </div>

                <div class="label_g2">
                <label class="label">
                        <span class="legend">*Autor:</span>
                        <select name="author" required>
                            <?php foreach ($authors as $author):
                                $authorId = $post->author;
                                $select = function ($value) use ($authorId) {
                                    return ($authorId == $value ? "selected" : "");
                                };
                                ?>
                                <option <?= $select($author->id); ?>
                                        value="<?= $author->id; ?>"><?= $author->fullName(); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <?php
                            $status = $post->status;
                            $select = function ($value) use ($status) {
                                return ($status == $value ? "selected" : "");
                            };
                            ?>
                            <option <?= $select("post"); ?> value="post">Publicar</option>
                            <option <?= $select("draft"); ?> value="draft">Rascunho</option>
                            <option <?= $select("trash"); ?> value="trash">Lixo</option>
                        </select>
                    </label>
                </div>

                <div class="al-right">
                    <button class="btn btn-blue"><i class="fa fa-sync"></i>Atualizar</button>
                </div>
            </form>
        </div>
    <?php endif; ?>
</main>

<?php $v->start('scripts');?>
    <script>
        const textoBody = () => {
        let count = document.querySelectorAll('textarea')
            count.forEach((item, index) => {
                let id = item.getAttribute("name")
                let conten = document.querySelector("." + id)
                let str = item.textContent

                String.prototype.stripHTML = function() {return this.replace(/<.*?>/g, '');}

                conten.innerHTML = `Caracteres: ${str.stripHTML().length}`

            })
        }
        textoBody()
        $(function () {
            $('#add').on('click',function(){
                $('.add').append('<div class="remov"><span>x</span><input style="margin:5px 0" type="text" name="datasheet[]" placeholder="Fotografia: Fulado da Silvass" required/></div>');
                
                $('.remov span').on('click',function(){
                    $(this).parent().remove();
                });
            });
            $('#add2').on('click',function(){
                $('.add2').append('<div class="remov"><span>x</span><input style="margin:5px 0" type="text" name="project[]" placeholder="Ensaio: Portal SBN" required/></div>');

                $('.remov span').on('click',function(){
                    $(this).parent().remove();
                });
            });

            $('.remov span').on('click',function(){
                $(this).parent().remove();
            });
        });
    </script>
<?php $v->end(); ?>