<?php $v->layout("_admin"); ?>
<div class="desc"><i class="far fa-images"></i> Publicidades</div>
<div class="searsh_form">
    <form action="<?= url("/".PATH_ADMIN."/ads/home"); ?>" class="app_search_form">
        <input type="text" name="s" value="<?= $search; ?>" placeholder="Pesquisar Publicidade:">
        <button><i class="fas fa-search"></i></button>
    </form>
</div>

<main>
    <?php if (!$ads): ?>
        <div class="message info"><i class="fas fa-info fa-2x"></i>Ainda não existem Publicidades cadastrada.</div>
    <?php else: ?>
        <?php foreach ($ads as $publicity):
        $publicityPhoto = ($publicity->photo() ? image($publicity->photo, 300) :
        theme("/assets/images/avatar.jpg", CONF_VIEW_ADMIN));
        ?>
        <div class="widgets user-list">
            <div class="cover" style="background-image:url('<?= $publicityPhoto; ?>')"></div>
            <hr class="hr">
            <p class="title" style="font-size:var(--font-medium)"><?=$publicity->name;?></p>
            <div class="info_list">
                <p><i class="far fa-clock"></i><?= ($publicity->publicity_at > date("Y-m-d H:i:s"))? '<span style=color:blue>Agendado para: '.date_fmt($publicity->publicity_at, "d.m.y \à\s H\hi").'</span>': date_fmt($publicity->publicity_at, "d.m.y \à\s H\hi"); ?></p>
                <p class="publi-out"><i class="far fa-eye-slash"></i><?= date_fmt($publicity->publicity_out, "d.m.y \à\s H\hi"); ?></p>
                
                <?php
                    if(date('Y-m-d H:i:s') > $publicity->publicity_out){?>
                       <p class="conta-hora"><i class="fas fa-times-circle"></i> Expirado, <strong>ENVIAR PARA LIXEIRA!!!</strong></p>
                    <?php } else {?>
                        <p class="conta-hora-green"><i class="fas fa-check"></i><strong>Expira:</strong> <?=dataPublicidade($publicity->publicity_out);?></p>
                   <?php }
                    $pagina = str_replace(['300x600','300x250','728x90','220x90','popup'],[' 300x600',' 300x250',' 728x90',' 220x90',' popup'],$publicity->page);
                    $pagina = explode(' ',current(explode(',', $pagina)));
                ?>
                <p class="local-list"><i class="far fa-images"></i> <strong>Mídia:</strong> <?=end($pagina);?></p>
                <p><i class="fas fa-share-square"></i><?= ($publicity->status == "post" ? "<span style='color:var(--color-green)'>Público</span>" : ($publicity->status == "draft" ? "<span style='color:var(--color-yellow)'>Rascunho</span>" : "<span style='color:var(--color-red)'>Lixo</span>")); ?></p>
            </div>
            <div class="actions">
                <a class="btn btn-blue" href="<?= url("/".PATH_ADMIN."/ads/publicity/{$publicity->id}"); ?>" title=""><i class="fa fa-cogs"></i>Gerenciar</a>
            </div> 
        </div>
        <?php endforeach; ?>
        <div class="clear"></div>
        
        <div class="paginacao">
        <hr class="hr">
            <?= $paginator; ?>
        </div>
    <?php endif; ?>
</main>
