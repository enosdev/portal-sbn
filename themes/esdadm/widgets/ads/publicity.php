<?php $v->layout("_admin"); ?>
<div class="desc"><i class="far fa-images"></i> Publicidades</div>
<?php

//padrão de mídias
$tamanhos_midia = ['728 x 90' => '728x90', '300 x 600' => '300x600', '300 x 250' => '300x250', '220 x 90' => '220x90', 'PopUp' => 'popup'];

$locais_midia = ['Inicial' => 'inicial', 'Política' => 'politica', 'Educação' => 'educacao',
'Saúde' => 'saude', 'Polícia' => 'policia', 'Brasil' => 'brasil', 'Esporte' => 'esporte',
'Tv SBN' => 'tv-sbn', 'Entretenimento' => 'entretenimento', 'Estilo de vida' => 'estilo-de-vida',
'Mundo' => 'mundo', 'Colunista' => 'colunista'];
?>

<main>
<?php $v->insert("widgets/ads/sidebar.php"); ?>
    <?php if (!$publicity): ?>

        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/ads/publicity"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Nome:</span>
                        <input type="text" name="name" placeholder="Nome" required/>
                    </label>

                    <label class="label">
                        <span class="legend">Link:</span>
                        <input type="text" name="link" placeholder="url link"/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Data da publicação:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="publicity_at" value="<?= date('d/m/Y H:i');?>" required/>
                    </label>
                    <label class="label">
                        <span class="legend">Data para expirar:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="publicity_out" value="<?= date('d/m/Y H:i',strtotime("+23 Days"));?>" required/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Mídias:</span>
                    <div style="color:#f00">
                        ATENÇÃO: com exceção do PopUp todas a mídias devem conter os tamanhos específicos listados abaixo!
                    </div>
                </label>

                <div class="box-publi">
                    <?php
                        $midias = $tamanhos_midia;
                        foreach($midias as $midia => $valueMidia):
                    ?>
                    <div class="box-publi-midia">
                        <div class="title-midia"><?=$midia;?></div>
                        <?php
                            $locais = $locais_midia;
                            $i = 1;
                        foreach($locais as $local => $valueLocal):
                        ?>
                            
                            <div class="space-content">       
                                <div class="checks">
                                    <?php if($valueMidia == 'popup'):?>
                                        <input type="checkbox" name="local[]" value="<?=$valueLocal.$valueMidia."_".$i++;?>" data-value="<?=$valueLocal.$valueMidia;?>">
                                    <?php else: ?>
                                        <input type="checkbox" name="local[]" value="<?=$valueLocal.$valueMidia."_".$i++;?>" data-value="<?=$valueLocal.$valueMidia;?>">
                                        <input type="checkbox" name="local[]" value="<?=$valueLocal.$valueMidia."_".$i++;?>" data-value="<?=$valueLocal.$valueMidia;?>">
                                        <input type="checkbox" name="local[]" value="<?=$valueLocal.$valueMidia."_".$i++;?>" data-value="<?=$valueLocal.$valueMidia;?>">
                                    <?php endif; ?>
                                    <input id="<?=$valueLocal.$valueMidia;?>" type="hidden" name="page[]" value="<?=$valueLocal.$valueMidia;?>" disabled>
                                    <div><?=$local;?></div>
                                </div>
                            </div>
                        
                        <?php
                        $i = 1;
                        endforeach;
                        ?>
                    </div>
                    <?php
                    endforeach;
                    ?>
                </div>
                <script>
                    (() => {
                        
                        const checks = document.querySelectorAll('[data-value]')

                        checks.forEach(check => check.addEventListener('click', () => {
                            const valor = check.dataset.value
                            const id = check.dataset.id
                            let countChecked = document.querySelectorAll('[data-value="'+valor+'"]')
                            let cont = 0;
                            for (let x=0; x < countChecked.length; x++) {
                                if (countChecked[x].checked) {
                                    cont++;
                                }
                            }

                            let enables = document.querySelector('#'+valor)
                            if(cont >= 1){
                                enables.disabled = false
                            } else {
                                enables.disabled = true
                            }
                        }))

                    })()
                </script>

                <label class="label">
                    <span class="legend">Imagem:</span>
                    <input id="file" type="file" name="photo"/>
                </label>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <option value="post">Publicar</option>
                            <option value="draft">Rascunho</option>
                            <option value="trash ">Lixo</option>
                        </select>
                    </label>
                </div>

                <div class="al-right">
                    <button class="btn btn-green"><i class="far fa-save"></i>Criar Publicidade</button>
                </div>
            </form>
        </div>
    <?php else: ?>
        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/ads/publicity/{$publicity->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Nome:</span>
                        <input type="text" name="name" placeholder="Nome" value="<?= $publicity->name; ?>" required/>
                    </label>

                    <label class="label">
                        <span class="legend">Link:</span>
                        <input type="text" name="link" placeholder="url link" value="<?= $publicity->link; ?>"/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Data da publicação:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="publicity_at" value="<?= date_fmt_br($publicity->publicity_at) ;?>" required/>
                    </label>
                    <label class="label">
                        <span class="legend">Data para expirar:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="publicity_out" value="<?= date_fmt_br($publicity->publicity_out) ;?>" required/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Mídias:</span>
                    <div style="color:#f00">
                        ATENÇÃO: com exceção do PopUp todas a mídias devem conter os tamanhos específicos listados abaixo!
                    </div>
                </label>

                <div class="box-publi">
                    <?php
                        $midias = $tamanhos_midia;
                        foreach($midias as $midia => $valueMidia):
                    ?>
                    <div class="box-publi-midia">
                        <div class="title-midia"><?=$midia;?></div>
                        <?php
                            $locais = $locais_midia;
                            $i = 1;
                            $d = 1;
                        foreach($locais as $local => $valueLocal):
                            $pos1 = strpos($publicity->local, $valueLocal.$valueMidia.'_1');
                            $pos2 = strpos($publicity->local, $valueLocal.$valueMidia.'_2');
                            $pos3 = strpos($publicity->local, $valueLocal.$valueMidia.'_3');
                            
                            $c1 = ($pos1 !== false)? 'checked' : '';
                            $c2 = ($pos2 !== false)? 'checked' : '';
                            $c3 = ($pos3 !== false)? 'checked' : '';
                            
                            if($c1 == 'checked'){
                                $disabled = '';
                            } elseif ($c2 == 'checked'){
                                $disabled = '';
                            } elseif ($c3 == 'checked'){
                                $disabled = '';
                            } else {
                                $disabled = 'disabled';
                            }
                            // echo $valor;

                        ?>
                            
                            <div class="space-content">       
                                <div class="checks">
                                    <?php if($valueMidia == 'popup'):?>
                                        <input <?=$c1;?> type="checkbox" name="local[]" value="<?=$valueLocal.$valueMidia."_1";?>" data-value="<?=$valueLocal.$valueMidia;?>">
                                    <?php else: ?>
                                        <input <?=$c1;?> type="checkbox" name="local[]" value="<?=$valueLocal.$valueMidia."_1";?>" data-value="<?=$valueLocal.$valueMidia;?>">
                                        <input <?=$c2;?> type="checkbox" name="local[]" value="<?=$valueLocal.$valueMidia."_2";?>" data-value="<?=$valueLocal.$valueMidia;?>">
                                        <input <?=$c3;?> type="checkbox" name="local[]" value="<?=$valueLocal.$valueMidia."_3";?>" data-value="<?=$valueLocal.$valueMidia;?>">
                                    <?php endif;?>
                                    <input id="<?=$valueLocal.$valueMidia;?>" type="hidden" name="page[]" value="<?=$valueLocal.$valueMidia;?>" <?=$disabled;?>>
                                    <div><?=$local;?></div>
                                </div>
                            </div>
                        
                        <?php
                        $i = 1;
                        $d++;
                        endforeach;
                        ?>
                    </div>
                    <?php
                    endforeach;
                    ?>
                </div>
                <script>
                    (() => {
                        
                        const checks = document.querySelectorAll('[data-value]')

                        checks.forEach(check => check.addEventListener('click', () => {
                            const valor = check.dataset.value
                            const id = check.dataset.id
                            let countChecked = document.querySelectorAll('[data-value="'+valor+'"]')
                            let cont = 0;
                            for (let x=0; x < countChecked.length; x++) {
                                if (countChecked[x].checked) {
                                    cont++;
                                }
                            }

                            let enables = document.querySelector('#'+valor)
                            if(cont >= 1){
                                enables.disabled = false
                            } else {
                                enables.disabled = true
                            }
                        }))

                    })()
                </script>

                <label class="label">
                    <span class="legend">Imagem:</span>
                    <input id="file" type="file" name="photo"/>
                </label>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <?php
                        $sta = $publicity->status;
                        $select = function ($value) use ($sta) {
                            return ($sta == $value ? "selected" : "");
                        };
                        ?>
                        <select name="status" required>
                            <option value="post" <?=$select('post');?>>Publicar</option>
                            <option value="draft" <?=$select('draft');?>>Rascunho</option>
                            <option value="trash" <?=$select('trash');?>>Lixo</option>
                        </select>
                    </label>
                </div>

                <div class="app_form_footer">
                    <button class="btn btn-blue"><i class="fa fa-sync"></i>Atualizar</button>
                    <a href="#" class="remove_link"
                       data-post="<?= url("/".PATH_ADMIN."/ads/publicity/{$publicity->id}"); ?>"
                       data-action="delete"
                       data-confirm="ATENÇÃO: Tem certeza que deseja excluir a publicidade? Essa ação não pode ser feita!"
                       data-publicity_id="<?= $publicity->id; ?>"><i class="far fa-trash-alt"></i>Excluir Publicidade</a>
                </div>
            </form>
        </div>
    <?php endif; ?>
</main>