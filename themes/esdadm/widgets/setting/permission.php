<?php $v->layout("_admin"); ?>
<div class="desc"><i class="fas fa-cogs"></i> Permissões</div>

<main>
    <?php if (!$permission): ?>
        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/setting/permission"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Titulo:</span>
                        <input type="text" name="title" placeholder="título" required/>
                    </label>

                    <label class="label">
                        <span class="legend">*Nome Slug:</span>
                        <input type="text" name="uri" placeholder="slug"/>
                    </label>
                </div>

                <div class="al-right">
                    <button class="btn btn-green"><i class="far fa-save"></i> Criar Permissões</button>
                </div>
            </form>
        </div>
    <?php else: ?>

        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/setting/permission/{$permission->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Titulo:</span>
                        <input type="text" name="title" placeholder="titulo" value="<?= $permission->title; ?>" required/>
                    </label>

                    <label class="label">
                        <span class="legend">*Nome slug:</span>
                        <input type="text" name="uri" placeholder="slug" value="<?= $permission->uri; ?>" required/>
                    </label>
                </div>

                <div class="app_form_footer">
                    <button class="btn btn-blue"><i class="fas fa-sync"></i> Atualizar</button
                </div>
            </form>
        </div>
    <?php endif; ?>
</main>