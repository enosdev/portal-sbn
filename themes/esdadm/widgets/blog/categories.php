<?php $v->layout("_admin"); ?>
<div class="desc"><i class="fa fa-tag"></i> Categorias</div>

<div class="searsh_form">
    <a class="btn btn-green" href="<?= url("/".PATH_ADMIN."/blog/category"); ?>"><i class="fa fa-plus-circle fa-2x"></i>Nova Categoria</a>
</div>

    <main>
        <?php if (!$categories): ?>
            <div class="message info icon-info">Ainda não existem categorias cadastradas em seu site</div>
        <?php else:
                        
            foreach ($categories as $category):
                $categoryCover = ($category->cover ? image($category->cover, 300) : "");
                ?>
                <article class="widgets_categories">
                    <div class="thumb">
                        <div style="background-image: url(<?= $categoryCover; ?>); width: 134px; height: 75px;" class="cover embed"></div>
                    </div>
                    <div class="info">
                        <h3 class="title" style="color:<?=$category->color;?>">
                            <?= $category->title; ?>
                            [ <strong><?= $category->posts()->count(); ?> posts</strong> ] <span style="margin-left:10px" class="btn btn-<?= ($category->menu == 'off')? 'red' : 'green' ;?>">#Menu <?= $category->menu;?></span>
                        </h3>
                        <!-- <p>< ?= $category->description; ?></p> -->

                        <div class="actions">
                            <a class="btn btn-blue" title=""
                                href="<?= url("/".PATH_ADMIN."/blog/category/{$category->id}"); ?>"><i class="fa fa-pencil-alt"></i>Editar</a>

                            <a class="btn btn-red" href="#" title=""
                                data-post="<?= url("/".PATH_ADMIN."/blog/category"); ?>"
                                data-action="delete"
                                data-confirm="Tem certeza que deseja deletar a categoria?"
                                data-category_id="<?= $category->id; ?>"><i class="fa fa-trash-alt"></i>Deletar</a>
                        </div>

                        <?php
                        $parent = (new Source\Models\Category())->find("parent = :p", "p={$category->id}")->order("title")->fetch(true);
                        if($parent){
                            foreach($parent as $p):?>
                                <div style="margin:10px;border-bottom:solid #ccc 1px;padding-bottom:5px">
                                    <div class="actions">
                                    <?=$p->title;?>
                                    [ <strong><?= $p->posts()->count(); ?> posts</strong> ]
                                        <a class="btn btn-blue" title=""
                                        href="<?= url("/".PATH_ADMIN."/blog/category/{$p->id}"); ?>"><i class="fa fa-pencil-alt"></i></a>

                                        <a class="btn btn-red" href="#" title=""
                                        data-post="<?= url("/".PATH_ADMIN."/blog/category"); ?>"
                                        data-action="delete"
                                        data-confirm="Tem certeza que deseja deletar a categoria?"
                                        data-category_id="<?= $p->id; ?>"><i class="fa fa-trash-alt"></i></a>
                                    </div>
                                </div>
                        <?php    endforeach; 
                        }
                        ?>
                        
                    </div>
                </article>
            <?php endforeach; ?>
            <div class="clear"></div>
        
            <div class="paginacao">
            <hr class="hr">
                <?= $paginator; ?>
            </div>

        <?php endif; ?>
    </main>