<?php $v->layout("_admin"); ?>
<div class="desc"><i class="fa fa-tag"></i> Categorias</div>

<main>
<?php $v->insert("widgets/blog/sidebar.php"); ?>

    <?php if (!$category): ?>
        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/blog/category"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <label class="label">
                    <span class="legend">*Título:</span>
                    <input type="text" name="title" placeholder="O nome da categoria" required/>
                </label>

                <label class="label">
                    <span class="legend">*Descrição:</span>
                    <textarea name="description" placeholder="Sobre esta categoria" required></textarea>
                </label>

                <label class="label">
                    <span class="legend">Capa:</span>
                    <input id="file" type="file" name="cover" placeholder="Uma imagem de capa"/>
                </label>

                <label class="label">
                    <span class="legend">Colocar no Menu?:</span>
                </label>
                <div class="label_g2">
                    <label class="label">
                        <label class="legend" for="on">Sim</label>
                        <input id="on" type="radio" name="menu" value="on">
                    </label>
                    <label class="label">
                        <label class="legend" for="off">Não</label>
                        <input id="off" type="radio" name="menu" value="off" checked>
                    </label>
                    <label class="label"></label>
                    <label class="label"></label>
                    <label class="label"></label>
                    <label class="label"></label>
                    <label class="label"></label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Categoria:</span>
                        <select name="parent">
                            <option value="null">Categoria principal</option>
                            <?php foreach ($categories as $category): ?>
                                <option data-color="<?= $category->color; ?>" value="<?= $category->id; ?>"><?= $category->title; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">Cor:</span>
                    <input type="color" name="color" value="#707070"/>
                </div>

                <div class="al-right">
                    <button class="btn btn-green"><i class="fa fa-save"></i>Criar Categoria</button>
                </div>
            </form>
        </div>
    <?php else: ?>

        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/blog/category/{$category->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <label class="label">
                    <span class="legend">*Título:</span>
                    <input type="text" name="title" value="<?= $category->title; ?>"
                           placeholder="O nome da categoria" required/>
                </label>

                <label class="label">
                    <span class="legend">*Descrição:</span>
                    <textarea name="description" placeholder="Sobre esta categoria"
                              required><?= $category->description; ?></textarea>
                </label>

                <label class="label">
                    <span class="legend">Capa:</span>
                    <input id="file" type="file" name="cover" placeholder="Uma imagem de capa"/>
                </label>

                <label class="label">
                    <span class="legend">Colocar no Menu?:</span>
                </label>
                <div class="label_g2">
                    <?php
                        $checkmenu = $category->menu;
                        $checked = function ($value) use ($checkmenu) {
                            return ( $checkmenu == $value ? "checked" : "");
                        };
                    ?>
                    <label class="label">
                        <label class="legend" for="on">Sim</label>
                        <input id="on" type="radio" name="menu" value="on" <?=$checked("on");?>>
                    </label>
                    <label class="label">
                        <label class="legend" for="off">Não</label>
                        <input id="off" type="radio" name="menu" value="off" <?=$checked("off");?>>
                    </label>
                    <label class="label"></label>
                    <label class="label"></label>
                    <label class="label"></label>
                    <label class="label"></label>
                    <label class="label"></label>
                </div>
                
                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Categoria:</span>
                        <select name="parent">
                        <option value="null">Categoria Principal</option>
                            <?php
                            $cat = (new Source\Models\Category())->find("parent IS NULL")->order("title")->fetch(true);
                            if(!$category->parent){
                                
                                foreach ($cat as $cate): ?>
                                <option data-color="<?= $cate->color; ?>" value="<?= $cate->id; ?>"><?= $cate->title; ?></option>
                            <?php endforeach;

                            } else {
                             foreach ($cat as $c):
                                $categoryId = $category->parent;
                                $select = function ($value) use ($categoryId) {
                                    return ($categoryId == $value ? "selected" : "");
                                };
                                ?>
                                <option data-color="<?= $c->color; ?>" <?= $select($c->id); ?>
                                        value="<?= $c->id; ?>"><?= $c->title; ?></option>
                            <?php endforeach;
                            }?>
                        </select>
                    </label>
                    <label class="label">
                    <span class="legend">Cor:</span>
                    <input type="color" name="color" value="<?=$category->color;?>"/>
                </label>
                </div>

                <div class="al-right">
                    <button class="btn btn-blue"><i class="fa fa-sync"></i>Atualizar</button>
                </div>
            </form>
        </div>
    <?php endif; ?>
</main>