<?php $v->layout("_admin"); ?>
<div class="desc"><i class="fas fa-user-friends"></i> Usuários</div>
<main>
<?php $v->insert("widgets/users/sidebar.php"); ?>
    <?php if (!$user): ?>
        <di class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/users/user"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Nome:</span>
                        <input type="text" name="first_name" placeholder="Primeiro nome" required/>
                    </label>

                    <label class="label">
                        <span class="legend">*Sobrenome:</span>
                        <input type="text" name="last_name" placeholder="Último nome" required/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Genero:</span>
                    <select name="genre">
                        <option value="male">Masculino</option>
                        <option value="female">Feminino</option>
                        <option value="other">Outros</option>
                    </select>
                </label>

                <label class="label">
                    <span class="legend">Foto: (600x600px)</span>
                    <input id="file" type="file" name="photo"/>
                </label>

                <label class="label">
                    <span class="legend">Banner: (1000x162px)</span>
                    <input id="file" type="file" name="banner"/>
                </label>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Nascimento:</span>
                        <input type="text" class="mask-date" name="datebirth" placeholder="dd/mm/yyyy"/>
                    </label>

                    <label class="label">
                        <span class="legend">Documento:</span>
                        <input class="mask-doc" type="text" name="document" placeholder="CPF do usuário"/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*E-mail:</span>
                        <input type="email" name="email" placeholder="Melhor e-mail" required/>
                    </label>

                    <label class="label">
                        <span class="legend">*Senha:</span>
                        <input type="password" name="password" placeholder="Senha de acesso" required/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Level:</span>
                        <select name="level" required>
                            <?php
                            if ( user()->level == 5) {
                                ?>
                                <option value="5">Super Admin</option>
                                <?php
                            }
                            ?>
                            <!-- <option value="1">Usuário</option> -->
                            <option value="6">Admin</option>
                            <option value="7">Colunista</option>
                            <option value="8">Editor</option>
                        </select>
                    </label>

                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <option value="registered">Inativo</option>
                            <option value="confirmed">Ativo</option>
                        </select>
                    </label>
                </div>

                <div class="label">
                    <label class="label">
                        <span class="legend">Frase do Colunista (OPCIONAL):</span>
                        <input type="text" name="description" placeholder="Frase"/>
                    </label>
                </div>

                <?php if(user()->level <= 6):?>
                <div class="label">
                    <label class="legend">Permissão</label>
                    <div class="box-permission">
                        <?php foreach($permission as $permit):?>
                            <div class="permission-item">
                                <input id="<?=$permit->uri;?>" type="checkbox" name="permission[]" value="<?=$permit->uri;?>">
                                <label for="<?=$permit->uri;?>"><?=$permit->title;?></label>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>

                <div class="label">
                    <label class="legend">Categorias / Postagens</label>
                    <div class="box-permission">
                        <?php foreach($categories as $category):?>
                            <div class="permission-item">
                                <input id="<?=$category->uri;?>" type="checkbox" name="categories[]" value="<?=$category->uri;?>">
                                <label for="<?=$category->uri;?>"><?=$category->title;?></label>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
                <?php endif;?>

                <div class="al-right">
                    <button class="btn btn-green"><i class="far fa-save"></i> Criar Usuário</button>
                </div>
            </form>
        </div>
    <?php else: ?>
        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/users/user/{$user->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Nome:</span>
                        <input type="text" name="first_name" value="<?= $user->first_name; ?>"
                               placeholder="Primeiro nome" required/>
                    </label>

                    <label class="label">
                        <span class="legend">*Sobrenome:</span>
                        <input type="text" name="last_name" value="<?= $user->last_name; ?>" placeholder="Último nome"
                               required/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Genero:</span>
                    <select name="genre">
                        <?php
                        $genre = $user->genre;
                        $select = function ($value) use ($genre) {
                            return ($genre == $value ? "selected" : "");
                        };
                        ?>
                        <option <?= $select("male"); ?> value="male">Masculino</option>
                        <option <?= $select("female"); ?> value="female">Feminino</option>
                        <option <?= $select("other"); ?> value="other">Outros</option>
                    </select>
                </label>

                <label class="label">
                    <span class="legend">Foto: (600x600px)</span>
                    <input id="file" type="file" name="photo"/>
                </label>

                <label class="label">
                    <span class="legend">Banner: (1000x162px)</span>
                    <input id="file" type="file" name="banner"/>
                </label>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Nascimento:</span>
                        <input type="text" class="mask-date" value="<?= date_fmt($user->datebirth, "d/m/Y"); ?>"
                               name="datebirth" placeholder="dd/mm/yyyy"/>
                    </label>

                    <label class="label">
                        <span class="legend">Documento:</span>
                        <input class="mask-doc" type="text" value="<?= $user->document; ?>" name="document"
                               placeholder="CPF do usuário"/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*E-mail:</span>
                        <input type="email" name="email" value="<?= $user->email; ?>" placeholder="Melhor e-mail"
                               required/>
                    </label>

                    <label class="label">
                        <span class="legend">Alterar Senha:</span>
                        <input type="password" name="password" placeholder="Senha de acesso"/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Level:</span>
                        <select name="level" required>
                            <?php
                            $level = $user->level;
                            $select = function ($value) use ($level) {
                                return ($level == $value ? "selected" : "");
                            };
                            if ( user()->level == 5){
                                ?>
                                <option <?= $select(5); ?> value="5">Super Admin</option>
                            <?php
                            }
                            ?>
                            <!-- <option < ?= $select(1); ?> value="1">Usuário</option> -->
                            <option <?= $select(6); ?> value="6">Admin</option>
                            <option <?= $select(7); ?> value="7">Colunista</option>
                            <option <?= $select(8); ?> value="8">Editor</option>
                        </select>
                    </label>

                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <?php
                            $status = $user->status;
                            $select = function ($value) use ($status) {
                                return ($status == $value ? "selected" : "");
                            };
                            ?>
                            <option <?= $select("registered"); ?> value="registered">Inativo</option>
                            <option <?= $select("confirmed"); ?> value="confirmed">Ativo</option>
                        </select>
                    </label>
                </div>

                <div class="label">
                    <label class="label">
                        <span class="legend">Frase do Colunista (OPCIONAL):</span>
                        <input type="text" name="description" placeholder="Frase" value="<?=$user->description;?>"/>
                    </label>
                </div>

                <?php if(user()->level <= 6): ?>
                <div class="label">
                    <label class="legend" for="artigo">Permissão</label>
                    <div class="box-permission">
                        <?php 
                        $pag = explode(',',$user->permission);
                        $checked = function ($value) use ($pag) {
                            return ((in_array($value, $pag)) ? "checked" : "");
                        };
                        foreach($permission as $permit):?>
                            <div class="permission-item">
                                <input id="<?=$permit->uri;?>" type="checkbox" name="permission[]" value="<?=$permit->uri;?>" <?= $checked($permit->uri); ?>>
                                <label for="<?=$permit->uri;?>"><?=$permit->title;?></label>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
                
                <div class="label">
                    <label class="legend" for="artigo">Categorias / Postagens</label>
                    <div class="box-permission">
                        <?php 
                        $pag = explode(',',$user->categories);
                        $checked = function ($value) use ($pag) {
                            return ((in_array($value, $pag)) ? "checked" : "");
                        };
                        foreach($categories as $category):?>
                            <div class="permission-item">
                                <input id="<?=$category->uri;?>" type="checkbox" name="categories[]" value="<?=$category->uri;?>" <?= $checked($category->uri); ?>>
                                <label for="<?=$category->uri;?>"><?=$category->title;?></label>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
                <?php endif;?>
                <div class="app_form_footer">
                    <button class="btn btn-blue"><i class="fas fa-sync"></i> Atualizar</button>
                    <a href="#" class="remove_link"
                       data-post="<?= url("/".PATH_ADMIN."/users/user/{$user->id}"); ?>"
                       data-action="delete"
                       data-confirm="ATENÇÃO: Tem certeza que deseja excluir o usuário e todos os dados relacionados a ele? Essa ação não pode ser desfeita!"
                       data-user_id="<?= $user->id; ?>"><i class="far fa-trash-alt"></i> Excluir Usuário</a>
                </div>
            </form>
        </div>
    <?php endif; ?>
</main>