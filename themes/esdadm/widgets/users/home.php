<?php $v->layout("_admin"); ?>
<div class="desc"><i class="fas fa-user-friends"></i> Usuários</div>
<div class="searsh_form">
    <form action="<?= url("/".PATH_ADMIN."/users/home"); ?>" class="app_search_form">
        <input type="text" name="s" value="<?= $search; ?>" placeholder="Pesquisar Usuário:">
        <button><i class="fas fa-search"></i></button>
    </form>
</div>
<main>
    <?php foreach ($users as $user):
    $userPhoto = ($user->photo() ? image($user->photo, 240, 200) :
        theme("/assets/images/avatar.jpg", CONF_VIEW_ADMIN));
    ?>
    <div class="widgets user-list">
        <div class="cover" style="background-image:url('<?=$userPhoto;?>')"></div>
        <hr class="hr">
        <p><?= $user->fullName(); ?></p>
        <p><?php if ($user->level == 5): ?>
                <p class="level"><i class="fa fa-laptop-code"></i> Web Master</p>
            <?php elseif ($user->level == 6): ?>
                <p class="level"><i class="fa fa-users-cog"></i> Administrador</p>
            <?php elseif ($user->level == 7): ?>
                <p class="level icon-life-ring"><i class="fa fa-spell-check"></i> Colunista</p>
            <?php elseif ($user->level == 8): ?>
                <p class="level icon-life-ring"><i class="fa fa-user-edit"></i> Editor</p>
            <?php else: ?>
                <p class="level icon-user"><i class="fa fa-user"></i> Usuário</p>
            <?php endif; ?></p>
            <?php if($user->status == 'confirmed'): ?>
                <p style="color:green"> <i class="fa fa-check"></i>Ativo</p>
            <?php else: ?>
                <p style="color:red"> <i class="fa fa-times"></i>Inativo</p>
            <?php endif; ?>
            <a class="btn btn-blue" href="<?= url("/".PATH_ADMIN."/users/user/{$user->id}"); ?>" title=""><i class="fa fa-cogs"></i>Gerenciar</a>
            
    </div>
    <?php endforeach; ?>
    <div class="clear"></div>
    
    <div class="paginacao">
    <hr class="hr">
        <?= $paginator; ?>
    </div>
</main>