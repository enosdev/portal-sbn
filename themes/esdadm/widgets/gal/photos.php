<?php $v->layout("_admin"); ?>
<style>
    .input-check{
        all: unset;
        border: 1px solid #ccc;
        width: 33px;
        height: 28px;
        display: inline-block;
        padding-top:5px
    }
    .input-check:checked::after{
        content: '✓';
        font-size:2em;
        color: #36ba9b!important;
    }
    label{margin-top:5px}
</style>

<div class="desc"><i class="fas fa-images"></i> <?=$count;?> Imagens para: "<strong><?=$gall->title;?></strong>"</div>
<div class="searsh_form">
<a id="btnDel" href="#" class="btn btn-red" style="margin-right:10px;padding-top:10px;padding-bottom:10px"
                    data-post="<?= url("/".PATH_ADMIN."/gal/photos"); ?>"
                    data-action="deleteAll"
                    data-confirm="ATENÇÃO: Tem certeza que deseja excluir estas fotos? Essa ação não pode ser desfeita!"
                    data-back="<?=$gall->id;?>"><i class="far fa-trash-alt"></i>Apagar selecionados</a>

<a href="<?= url("/".PATH_ADMIN."/gal/gallery/{$gall->id}"); ?>" class="btn btn-green"><i class="far fa-upload"></i> Enviar mais fotos para essa galeria</a>
</div>
<main>
    <?php if (!$image): ?>
        <div class="message info"><i class="fas fa-info fa-2x"></i>Ainda não existem fotos nesta galeria.</div>
    <?php else: ?>
        <?php $i = 0; foreach ($image as $img):
            $photo = explode('storage',$img);
            ?>
        <div class="widgets user-list">
            <div class="cover" style="background-image:url('<?=url(CONF_UPLOAD_DIR.end($photo));?>')"></div>
            <hr class="hr">
            <div class="actions">
            <label for="i<?=$i;?>">selecionar&nbsp;</label>
            <input id="i<?=$i;?>" class="input-check" type="checkbox" name="photo" value="<?=end($photo);?>">
                <a href="#" class="btn btn-red"
                        data-post="<?= url("/".PATH_ADMIN."/gal/photos"); ?>"
                        data-action="delete"
                        data-confirm="ATENÇÃO: Tem certeza que deseja excluir foto? Essa ação não pode ser desfeita!"
                        data-back="<?=$gall->id;?>"
                        data-gallery_id="<?=end($photo);?>"><i class="far fa-trash-alt"></i>Apagar</a>
            </div>   
        </div>
        <?php $i++; endforeach; ?>
    <?php endif; ?>
</main>

<script>
    //pega o botão que vai ter a ação
    var btnDel = document.querySelector('#btnDel');
    //var b = document.querySelector("button"); 

    function getValues() {
        var photo = document.querySelectorAll('[name=photo]:checked');
        var values = [];
        for (var i = 0; i < photo.length; i++) {
            // utilize o valor aqui, adicionei ao array
            values.push(photo[i].value);
        }
        // alert(values);
        btnDel.setAttribute("data-check", values);
    }
    // adicionar ação ao clique no checkbox
    var checkboxes = document.querySelectorAll('[name=photo]');
    for (var i = 0; i < checkboxes.length; i++) {
        // somente nome da função, sem executar com ()
        checkboxes[i].addEventListener('click', getValues, false);
    }

    // var btnDel = document.querySelector('#btnDel');
    // btnDel.addEventListener('click', getValues, false);
</script>