<?php $v->layout("_admin"); ?>
<div class="desc"><i class="far fa-clock"></i> Cronômetro</div>
<div class="searsh_form">
    <form action="<?= url("/".PATH_ADMIN."/timer/home"); ?>" class="app_search_form">
        <input type="text" name="s" value="<?= $search; ?>" placeholder="Pesquisar Cronômetro:">
        <button><i class="fas fa-search"></i></button>
    </form>
</div>

<main>
    <?php if (!$timer): ?>
        <div class="message info"><i class="fas fa-info fa-2x"></i>Ainda não existem Cronômetro.</div>
    <?php else: ?>
        <?php foreach ($timer as $st):
        $stPhoto = ($st->photo() ? image($st->cover, 300) :
        theme("/assets/images/avatar.jpg", CONF_VIEW_ADMIN));
        ?>
        <div class="widgets user-list">
            <div class="cover" style="background-image:url('<?= $stPhoto; ?>')"></div>
            <hr class="hr">
            <p class="title" style="font-size:var(--font-medium)"><?=$st->title;?></p>
            <p style="font-size:var(--font-small)"><?=$st->description;?></p>
            <div class="info_list">
                <p><i class="far fa-clock"></i><?= date_fmt($st->event_at, "d.m.y \à\s H\hi"); ?></p>
            </div>
            <div class="actions">
                <a class="btn btn-blue" href="<?= url("/".PATH_ADMIN."/timer/stopwatch/{$st->id}"); ?>" title=""><i class="fa fa-cogs"></i>Gerenciar</a>
            </div> 
        </div>
        <?php endforeach; ?>
        <div class="clear"></div>
        
        <div class="paginacao">
        <hr class="hr">
            <?= $paginator; ?>
        </div>
    <?php endif; ?>
</main>