<?php $v->layout("_admin"); ?>
<div class="desc"><i class="far fa-clock"></i> Cronômetro</div>

<main>
    <?php $v->insert("widgets/timer/sidebar.php"); ?>
    <?php if (!$stopwatch): ?>
        
        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/timer/stopwatch"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Titulo:</span>
                        <input type="text" name="title" placeholder="Título" required/>
                    </label>

                    <label class="label">
                        <span class="legend">*Descrição:</span>
                        <input type="text" name="description" placeholder="Descrição" required/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Data do evento:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="event_at" value="<?= date('d/m/Y H:i');?>" required/>
                    </label>
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <option value="post">Publicar</option>
                            <option value="draft">Rascunho</option>
                            <option value="trash ">Lixo</option>
                        </select>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Imagem:</span>
                    <input id="file" type="file" name="photo"/>
                </label>

                <div class="al-right">
                    <button class="btn btn-green"><i class="far fa-save"></i>Criar Cronômetro</button>
                </div>
            </form>
        </div>
    <?php else: ?>
        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/timer/stopwatch/{$stopwatch->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Título:</span>
                        <input type="text" name="title" placeholder="Título" value="<?= $stopwatch->title; ?>" required/>
                    </label>

                    <label class="label">
                        <span class="legend">Descrição:</span>
                        <input type="text" name="description" placeholder="Descrição" value="<?= $stopwatch->description; ?>" required/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Data do vento:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="event_at" value="<?= date_fmt($stopwatch->event_at, "d/m/Y H:i");?>" required/>
                    </label>
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <?php
                        $sta = $stopwatch->status;
                        $select = function ($value) use ($sta) {
                            return ($sta == $value ? "selected" : "");
                        };
                        ?>
                        <select name="status" required>
                            <option value="post" <?=$select('post');?>>Publicar</option>
                            <option value="draft" <?=$select('draft');?>>Rascunho</option>
                            <option value="trash" <?=$select('trash');?>>Lixo</option>
                        </select>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Imagem:</span>
                    <input id="file" type="file" name="photo"/>
                </label>

                <div class="app_form_footer">
                    <button class="btn btn-blue"><i class="fa fa-sync"></i> Atualizar</button>
                    <a href="#" class="remove_link"
                       data-post="<?= url("/".PATH_ADMIN."/timer/stopwatch/{$stopwatch->id}"); ?>"
                       data-action="delete"
                       data-confirm="ATENÇÃO: Tem certeza que deseja excluir o Cronômetro? Essa ação não pode ser desfeita!"
                       data-stopwatch_id="<?= $stopwatch->id; ?>"><i class="far fa-trash-alt"></i> Excluir Cronômetro</a>
                </div>
            </form>
        </div>
    <?php endif; ?>
</main>