<?php $v->layout("_admin");?>
<div class="desc"><i class="far fa-user-edit"></i> Colunas</div>


<div class="mce_upload" style="z-index: 997">
    <div class="mce_upload_box">
        <form class="app_form" action="<?=url("/".PATH_ADMIN."/column/post");?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="upload" value="true"/>
            <label>
                <label class="legend">Selecione uma imagem JPG ou PNG:</label>
                <input accept="image/*" type="file" name="image" required/>
            </label>
            <button class="btn btn-blue"><i class="fa fa-cloud-upload-alt"></i>Enviar Imagem</button>
        </form>
    </div>
</div>

<main>
<?php $v->insert("widgets/column/sidebar.php");?>
    <?php if (!$post): ?>
        <div class="main_box">
            <form class="app_form" action="<?=url("/".PATH_ADMIN."/column/post");?>" method="post">
                <!-- ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <label class="label">
                    <span class="legend">*Título:</span>
                    <input type="text" name="title" placeholder="A manchete do seu artigo" required/>
                </label>

                <label class="label">
                    <span class="legend">Capa: (1920x1080px)</span>
                    <input id="file" type="file" name="cover" placeholder="Uma imagem de capa"/>
                </label>

                <label class="label">
                    <span class="legend">Vídeo:</span>
                    <input type="text" name="video" placeholder="O ID de um vídeo do YouTube"/>
                </label>

                <label class="label">
                    <span class="legend">*Conteúdo:</span>
                    <textarea class="mce" name="content"></textarea>
                </label>

                <div class="label_g2">
                    <label class="label">

                    </label>

                    <label class="label">
                        <span class="legend">*Autor:</span>
                        <select name="author" required>
                        <option value="">selecione um autor</option>
                            <?php foreach ($authors as $author):
                                $authorId = user()->id;
                                $select = function ($value) use ($authorId) {
                                    return ($authorId == $value ? "selected" : "");
                                };
                                ?>
                                <option <?=$select($author->id);?> value="<?=$author->id;?>"><?=$author->fullName();?></option>
                            <?php endforeach;?>
                        </select>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <option value="post">Publicar</option>
                            <option value="draft">Rascunho</option>
                            <option value="trash">Lixo</option>
                        </select>
                    </label>

                    <label class="label">
                        <span class="legend">Data de publicação:</span>
                        <input class="mask-datetime" type="text" name="post_at" value="<?=date("d/m/Y H:i");?>"
                               required/>
                    </label>
                </div>

                <div class="al-right">
                    <button class="btn btn-green"><i class="far fa-save"></i> Publicar</button>
                </div>
            </form>
        </div>
    <?php else: ?>

        <div class="main_box">
            <form class="app_form" action="<?=url("/".PATH_ADMIN."/column/post/{$post->id}");?>" method="post">
                <!-- ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <label class="label">
                    <span class="legend">*Título:</span>
                    <input type="text" name="title" value="<?=$post->title;?>" placeholder="A manchete do seu artigo"
                           required/>
                </label>

                <label class="label">
                    <span class="legend">Capa: (1920x1080px)</span>
                    <input id="file" type="file" name="cover" placeholder="Uma imagem de capa"/>
                </label>

                <label class="label">
                    <span class="legend">Vídeo:</span>
                    <input type="text" name="video" value="<?=$post->video;?>"
                           placeholder="O ID de um vídeo do YouTube"/>
                </label>

                <label class="label">
                    <span class="legend">*Conteúdo:</span>
                    <textarea class="mce" name="content"><?=$post->content;?></textarea>
                </label>

                <div class="label_g2">
                    <label class="label">

                    </label>

                    <label class="label">
                        <span class="legend">*Autor:</span>
                        <select name="author" required>
                            <?php foreach ($authors as $author):
                                $authorId = $post->author;
                                $select = function ($value) use ($authorId) {
                                    return ($authorId == $value ? "selected" : "");
                                };
                            ?>
                            <option <?=$select($author->id);?> value="<?=$author->id;?>"><?=$author->fullName();?></option>
                            <?php endforeach;?>
                        </select>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <?php
                                $status = $post->status;
                                $select = function ($value) use ($status) {
                                    return ($status == $value ? "selected" : "");
                                };
                            ?>
                            <option <?=$select("post");?> value="post">Publicar</option>
                            <option <?=$select("draft");?> value="draft">Rascunho</option>
                            <option <?=$select("trash");?> value="trash">Lixo</option>
                        </select>
                    </label>

                    <label class="label">
                        <span class="legend">Data de publicação:</span>
                        <input class="mask-datetime" type="text" name="post_at"
                               value="<?=date_fmt($post->post_at, "d/m/Y H:i");?>" required/>
                    </label>
                </div>

                <div class="al-right">
                    <button class="btn btn-blue"><i class="fa fa-sync"></i> Atualizar</button>
                </div>
            </form>
        </div>
    <?php endif;?>
</main>