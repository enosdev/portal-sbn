<?php $v->layout("_admin"); ?>
<div class="desc"><i class="far fa-calendar-alt"></i> Agenda de Eventos</div>
<div class="searsh_form">
    <form action="<?= url("/".PATH_ADMIN."/agend/home"); ?>" class="app_search_form">
        <input type="text" name="s" value="<?= $search; ?>" placeholder="Pesquisar Usuário:">
        <button><i class="fas fa-search"></i></button>
    </form>
</div>

<main>
    <?php if (!$posts): ?>
        <div class="message info"><i class="fas fa-info fa-2x"></i>Ainda não existem Agenda cadastrados nos eventos.</div>
    <?php else: ?>
        <?php foreach ($posts as $post):
        $postCover = ($post->cover ? image($post->cover, 300) : "");
        ?>
        <div class="widgets user-list">
            <div class="cover" style="background-image:url('<?=$postCover;?>')"></div>
            <hr class="hr">
            <p class="title"><a target="_blank" href=" <?= url("/evento/{$post->uri}"); ?>" title="Ver no site">
                    <?php if ($post->post_at > date("Y-m-d H:i:s")): ?>
                        <span><i style="color:var(--color-yellow)" class="far fa-clock"></i> <?= $post->title; ?></span>
                    <?php else: ?>
                        <span><i style="color:var(--color-green)" class="fa fa-check"></i><?= $post->title; ?></span>
                    <?php endif; ?>
                </a>
            </p>
            <div class="info_list">
                <p><i class="far fa-clock"></i><?= date_fmt($post->event_date, "d.m.y \à\s H\hi"); ?></p>
                <p><i class="fa fa-map-marker-alt"></i><?= $post->local; ?>, <?= $post->city; ?>/<?= strtoupper($post->estate); ?></p>
                <p><i class="fas fa-eye"></i><?= $post->views; ?></p>
                <p><i class="fas fa-share-square"></i><?= ($post->status == "post" ? "<span style='color:var(--color-green)'>Público</span>" : ($post->status == "draft" ? "<span style='color:var(--color-yellow)'>Rascunho</span>" : "<span style='color:var(--color-red)'>Lixo</span>")); ?></p>
            </div>
            <div class="actions">
                <a class="btn btn-blue" title=""
                    href="<?= url("/".PATH_ADMIN."/agend/agenda/{$post->id}"); ?>"><i class="fas fa-edit"></i>Editar</a>

                <a class="btn btn-red" title="" href="#"
                    data-post="<?= url("/".PATH_ADMIN."/agend/agenda"); ?>"
                    data-action="delete"
                    data-confirm="Tem certeza que deseja deletar essa agenda??"
                    data-agenda_id="<?= $post->id; ?>"><i class="far fa-trash-alt"></i>Deletar</a>
            </div> 
        </div>
        <?php endforeach; ?>
        <div class="clear"></div>
        
        <div class="paginacao">
        <hr class="hr">
            <?= $paginator; ?>
        </div>
    <?php endif; ?>
</main>