<div class="main_sidebar">
    <h3><i class="fas fa-laptop"></i> dash/enquete</h3>
    <p class="dash_content_sidebar_desc">Gerencie, monitore e acompanhe as enquetes do seu site aqui...</p>

    <?php if (!empty($channel->cover)): ?>
        <div>
            <h2 style="font-size:var(--font-small);margin:26px 0 13px 0"><i class="fa fa-pencil"></i> Editar enquete#<?= $channel->id; ?></h2>
            <a class="btn btn-green" href="<?= url("/enquete/{$channel->uri}"); ?>" target="_blank" title=""><i class="fa fa-link"></i> Ver no
                site</a>
        </div>
        <div id="image-holder">
            <img class="radius" style="width: 100%; margin-top: 30px" src="<?= image($channel->cover, 300); ?>"/>
        </div>
    <?php else: ?>
        <div id="image-holder"></div>
    <?php endif; ?>

</div>
