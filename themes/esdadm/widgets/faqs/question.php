<?php $v->layout("_admin"); ?>
<div class="desc"><i class="fas fa-poll"></i> Enquetes</div>

<main>
<?php $v->insert("widgets/faqs/sidebar.php"); ?>
    <?php if (!$question): ?>
        <div class="main_box">
            <div class="channel"># <?=$channel->channel;?></div>
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/faq/question/{$channel->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <label class="label">
                    <span class="legend">*Resposta:</span>
                    <input type="text" name="question" placeholder="Pergunta frequente" required/>
                </label>

                <label class="label">
                    <span class="legend">*Ordem:</span>
                    <input type="number" name="order_by" value="1" required/>
                </label>

                <div class="al-right">
                    <button class="btn btn-green"><i class="far fa-save"></i> Cadastrar</button>
                </div>
            </form>
        </div>
    <?php else: ?>
        <div class="main_box">
            <div class="channel"># <?=$channel->channel;?></div>
            <div class="searsh_form">
                <a href="<?= url("/".PATH_ADMIN."/faq/question/{$channel->id}"); ?>" class="btn btn-green"><i class="fas fa-plus"></i>Nova Resposta</a>
            </div>
            
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/faq/question/{$channel->id}/{$question->id}"); ?>"
                  method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <label class="label">
                    <span class="legend">*Resposta:</span>
                    <input type="text" name="question" value="<?= $question->question; ?>"
                           placeholder="Pergunta frequente" required/>
                </label>

                <label class="label">
                    <span class="legend">*Ordem:</span>
                    <input type="number" name="order_by" value="<?= $question->order_by; ?>" required/>
                </label>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Votos:</span>
                        <input type="number" name="votes" value="<?= $question->votes; ?>"/>
                    </label>
                </div>

                <div class="app_form_footer">
                    <button class="btn btn-blue"><i class="fa fa-sync"></i> Atualizar</button>
                    <a href="#" class="remove_link"
                       data-post="<?= url("/".PATH_ADMIN."/faq/question/{$channel->id}/{$question->id}"); ?>"
                       data-action="delete"
                       data-confirm="Tem certeza que deseja excluir a perguntas e a respostas?"
                       data-question_id="<?= $question->id; ?>"><i class="far fa-trash-alt"></i>Excluir Resposta</a>
                </div>
            </form>
        </div>
    <?php endif; ?>
</main>