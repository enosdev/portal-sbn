<?php $v->layout("_admin"); ?>
<div class="desc"><i class="fas fa-poll"></i> Enquetes</div>

<main>
    <?php $v->insert("widgets/faqs/sidebar.php"); ?>
    <?php if (!$channel): ?>
        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/faq/channel"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <label class="label">
                    <span class="legend">*Pergunta:</span>
                    <input type="text" name="channel" placeholder="Nome do canal" required/>
                </label>

                <label class="label">
                    <span class="legend">Capa: (1920x1080px)</span>
                    <input id="file" type="file" name="cover" placeholder="Inserir imagem"/>
                </label>
                
                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Data da publicação:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="created_at" value="<?= date('d/m/Y H:i');?>" required/>
                    </label>
                    <label class="label">
                        <span class="legend">*Data para expirar:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="expire_at" value="<?= date('d/m/Y H:i',strtotime("+23 Days"));?>" required/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">*Status:</span>
                    <select name="status" required>
                        <option value="post">Publicar</option>
                        <option value="draft">Rascunho</option>
                        <option value="trash ">Lixo</option>
                    </select>
                </label>

                <div class="al-right">
                    <button class="btn btn-green"><i class="far fa-save"></i>Criar Pergunta</button>
                </div>
            </form>
        </div>
    <?php else: ?>
        <div class="main_box">
            <div class="searsh_form">
                <a href="<?= url("/".PATH_ADMIN."/faq/question/{$channel->id}"); ?>" class="btn btn-green"><i class="fas fa-plus"></i>Criar Resposta</a>
            </div>
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/faq/channel/{$channel->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <label class="label">
                    <span class="legend">*Pergunta:</span>
                    <input type="text" name="channel" value="<?= $channel->channel; ?>" placeholder="Nome do canal"
                           required/>
                </label>

                <label class="label">
                    <span class="legend">Capa: (1920x1080px)</span>
                    <input id="file" type="file" name="cover" placeholder="Inserir imagem"/>
                </label>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Data da publicação:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="created_at" value="<?= date_fmt_br($channel->created_at) ;?>" required/>
                    </label>
                    <label class="label">
                        <span class="legend">Data para expirar:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="expire_at" value="<?= date_fmt_br($channel->expire_at) ;?>" required/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">*Status:</span>
                    <?php
                    $sta = $channel->status;
                    $select = function ($value) use ($sta) {
                        return ($sta == $value ? "selected" : "");
                    };
                    ?>
                    <select name="status" required>
                        <option value="post" <?=$select('post');?>>Publicar</option>
                        <option value="draft" <?=$select('draft');?>>Rascunho</option>
                        <option value="trash" <?=$select('trash');?>>Lixo</option>
                    </select>
                </label>

                <div class="app_form_footer">
                    <button class="btn btn-blue"><i class="fas fa-sync"></i> Atualizar</button>
                    <a href="#" class="remove_link"
                       data-post="<?= url("/".PATH_ADMIN."/faq/channel/{$channel->id}"); ?>"
                       data-action="delete"
                       data-confirm="Tem certeza que deseja excluir este canal e todas as suas perguntas e respostas?"
                       data-plan_id="<?= $channel->id; ?>"><i class="far fa-trash-alt"></i>Excluir Canal</a>
                </div>
            </form>
        </div>
    <?php endif; ?>
</main>