<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?= $head; ?>
    
    <link rel="stylesheet" href="<?= theme("/assets/styles/boot.css", CONF_VIEW_ADMIN); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/styles/login.css", CONF_VIEW_ADMIN); ?>">

    <link rel="icon" type="image/png" href="<?= theme("/assets/images/favicon.png", CONF_VIEW_ADMIN); ?>"/>
    
    
    <script src="https://kit.fontawesome.com/bc9f245d96.js" crossorigin="anonymous"></script>
</head>
<body>
    <div class="ajax_load">
        <div class="ajax_load_box">
            <div class="ajax_load_box_circle"></div>
            <p class="ajax_load_box_title">Aguarde, carregando...</p>
        </div>
    </div>

    <main>
        <?= $v->section("content"); ?>
    </main>

    <script src="<?= url("/shared/scripts/jquery.min.js"); ?>"></script>
    <script src="<?= url("/shared/scripts/jquery-ui.js"); ?>"></script>
    <script src="<?= theme("/assets/scripts/login.js", CONF_VIEW_ADMIN); ?>"></script>
</body>
</html>