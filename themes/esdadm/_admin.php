<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?= $head; ?>

    <link rel="stylesheet" href="<?= theme("/assets/styles/style.css?v=".CONF_SITE_VERSION, CONF_VIEW_ADMIN); ?>"/>
    <link rel="stylesheet" href="<?= url("/shared/styles/datetimepicker.css"); ?>"/>
    <link rel="stylesheet" href="<?= url("/shared/styles/jquery-ui.css"); ?>"/>
    <link rel="stylesheet" href="<?= url("/shared/fontawesome/css/all.min.css"); ?>"/>

    <link rel="icon" type="image/png" href="<?= theme("/assets/images/favicon.png", CONF_VIEW_ADMIN); ?>"/>

    <!-- <script src="https://kit.fontawesome.com/bc9f245d96.js" crossorigin="anonymous"></script> -->
</head>
<body>
    <div class="ajax_load" style="z-index: 999;">
        <div class="ajax_load_box">
            <div class="ajax_load_box_circle"></div>
            <p class="ajax_load_box_title">Aguarde, carregando...</p>
        </div>
    </div>

    <div class="ajax_response"><?= flash(); ?></div>

    <div class="overlay"></div>

    <header>
        <div class="logo mobile">
            <img src="<?=theme("/assets/images/es.png", CONF_VIEW_ADMIN);?>" alt="">
        </div>
        <div class="header-action">
            <span class="icon maior"><i class="far fa-bell"></i> <span class="notification_center_open" data-count="<?= url("/".PATH_ADMIN."/notifications/count"); ?>" data-notify="<?= url("/".PATH_ADMIN."/notifications/list"); ?>">0</span> </span>
            <span class="icon hide maior"><i class="far fa-clock"></i> <?= date("d/m H:i"); ?></span>
            <span class="hide"><a href="<?= url("/".PATH_ADMIN."/logoff"); ?>" class="sair icon">sair <i class="fas fa-power-off fa-2x"></i></a></span>
            <span class="menu"><i class="fas fa-bars"></i></span>
        </div>
    </header>

    <aside class="menu-mobile">
        <div class="logo">
            <img src="<?=theme("/assets/images/es.png", CONF_VIEW_ADMIN);?>" alt=""> 
            <img src="<?=theme("/assets/images/system.png", CONF_VIEW_ADMIN);?>" alt="">
        </div>
        <hr>
        <div class="perfil">
            <?php
                $photo = user()->photo();
                $userPhoto = ($photo ? image($photo, 100, 100) : theme("/assets/images/avatar.jpg", CONF_VIEW_ADMIN));
            ?>
            <div class="img"><a href="<?= url("/".PATH_ADMIN."/users/user/" . user()->id); ?>"><div class="bc" style="background-image:url('<?=$userPhoto;?>')"></div></a></div>
            <p><?= user()->fullName(); ?></p>
        </div>
        <hr>
        <nav>
            <ul>
            <?php
                $nav = function ($icon, $href, $title, array $sub = null) use ($app) {
                    $active = (explode("/", $app)[0] == explode("/", $href)[0] ? "active" : null);
                    $url = url("/".PATH_ADMIN."/{$href}");
                    //verifica a permissão do usuário
                    // $permissoes = ['dash','blog','column','timer','agend','gal','ads','users','trash'];
                    $permissoes = explode(',',user()->permission);
                    if(!in_array(explode("/", $href)[0], $permissoes)){
                        if(strpos($_SERVER['REQUEST_URI'], explode("/", $href)[0]) !== false){
                            redirect("/".PATH_ADMIN."/dash/home");
                            exit;
                        }
                        return false;
                    }

                    $return = "<li class=\"{$active}\"><a class=\"link\" href=\"{$url}\"><i class=\"fa fa-{$icon}\"></i> {$title}</a>";
                        if($sub != null):
                            $return .= "<ul class=\"sub\">";
                                $navSub = function ($titleSub, $hrefSub) use ($app) {
                                    $activeSub = ($app == $hrefSub ? "active" : null);
                                    $urlSub = url("/".PATH_ADMIN."/{$hrefSub}");

                                    $arrayUser = explode(',',user()->permission);
                                    $arrayPermission = (new Source\Models\Permission())->find()->fetch(true);
                                    $arrayPer = [];
                                    foreach($arrayPermission as $ap){
                                        $arrayPer[] = $ap->uri; 
                                    }

                                    //compara os 2 array do banco permission com user permission
                                    $arrayResult = array_diff($arrayPer,$arrayUser);
                                    if(in_array(explode("/", $hrefSub)[1],$arrayResult)){
                                        if(strpos($_SERVER['REQUEST_URI'], explode("/", $hrefSub)[1]) !== false){
                                            redirect("/".PATH_ADMIN."/dash/home");
                                            exit;
                                        }
                                        return false;
                                    }

                                    return "<li class=\"{$activeSub}\"><a class=\"link\" href=\"{$urlSub}\">• {$titleSub}</a></li>";
                                    
                                };
                                foreach($sub as $key => $value):
                                    $return .= $navSub($key,$value);
                                endforeach;
                            $return .="</ul>";
                        endif;
                    $return .= "</li>";
                    return $return;
                };

                $navFixed = function ($icon, $href, $title) use ($app) {
                    $active = (explode("/", $app)[0] == explode("/", $href)[0] ? "active" : null);
                    $url = url("/".PATH_ADMIN."/{$href}");
                    return "<li class=\"{$active}\"><a class=\"link\" href=\"{$url}\"><i class=\"fa fa-{$icon}\"></i> {$title}</a>";
                };
                
                /**
                 * nível de usuários
                 * 8 editor
                 * 7 coluna
                 * 6 admin comum
                 * 5 super admin
                 */

                echo $navFixed("home", "dash", "Dashboard");

                $art = ["Listar"=>"blog/home","Categorias"=>"blog/categories","Novo Artigo"=>"blog/post"];
                echo $nav("edit", "blog/home", "Artigos",$art);

                echo $nav("share", "share/home", "Compartilhamento");

                $col = ["Listar"=>"column/home","Nova Coluna"=>"column/post"];
                echo $nav("user-edit", "column/home", "Colunas", $col);
                
                $col = ["Listar"=>"pages/home","Adicionar"=>"pages/post"];
                echo $nav("file-alt", "pages/home", "Apresentação", $col);

                $cron = ["Listar"=>"timer/home","Novo Cron"=>"timer/stopwatch"];
                echo $nav("clock", "timer/home", "Cronômetro", $cron);

                $event = ["Listar"=>"agend/home","Novo Evento"=>"agend/agenda"];
                echo $nav("calendar-alt", "agend/home", "Agenda de Eventos", $event);

                $gale = ["Listar"=>"gal/home","Nova Galeria"=>"gal/gallery"];
                echo $nav("camera-retro", "gal/home", "Galerias", $gale);

                $publ = ["Listar"=>"ads/home","Nova Publicidade"=>"ads/publicity"];
                echo $nav("images", "ads/home", "Publicidades", $publ);

                echo $nav("poll", "faq/home", "Enquetes");

                $dest = ["Listar"=>"dest/home","Novo Destaque"=>"dest/highlight"];
                echo $nav("file-alt", "dest/home", "Destaques", $dest);

                $menu_usuario = ["Listar"=>"users/home","Novo Usuário"=>"users/user"];
                echo $nav("user-friends", "users/home", "Usuários", $menu_usuario);

                echo $nav("trash-alt", "trash/home", "Lixeira");

                $configuracoes = ["Listar"=>"setting/home","Nova Permissão"=>"setting/permission"];
                echo $nav("cogs", "setting/home", "Configurações", $configuracoes);

                echo "<li><a class=\"link\" href=\"".url()."\" target=\"_blank\"><i class=\"fas fa-link\"></i> Ver site</a></li>";
                echo "<li class=\"on_mobile\"><a class=\"link\" href=\"".url("/".PATH_ADMIN."/logoff")."\"><i class=\"fas fa-power-off\"></i> Sair</a></li>";
            ?>
            </ul>
        </nav>
        <footer>
            2019 © 
            ESD -> 
            <a target="_blank" href="https://www.enosdev.com">{es}</a>
        </footer>
    </aside>

    <section>
        <?= $v->section("content"); ?>        
    </section>


    <script src="<?= url("/shared/scripts/jquery.min.js"); ?>"></script>
    <script src="<?= url("/shared/scripts/jquery.form.js"); ?>"></script>
    <script src="<?= url("/shared/scripts/jquery-ui.js"); ?>"></script>
    <script src="<?= url("/shared/scripts/jquery.mask.js"); ?>"></script>
    <script src="<?= url("/shared/scripts/tinymce/tinymce.min.js"); ?>"></script>
    <script src="<?= url("/shared/scripts/datetimepicker.js"); ?>"></script>
    <script src="<?= theme("/assets/scripts/scripts.js?v=".CONF_SITE_VERSION, CONF_VIEW_ADMIN); ?>"></script>
    <?= $v->section("scripts"); ?>
</body>
</html>