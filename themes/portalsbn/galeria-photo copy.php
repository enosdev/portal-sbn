<li class="main_entretenimento_foto">
    <a data-fancybox="fotos" data-caption="<?= $post->title; ?>" href="<?= url("/storage{$img}"); ?>" title="<?= $post->title; ?>">
        <img src="<?= image($img, 215, 140); ?>" alt="<?= $post->title; ?>" title="<?= $post->title; ?>"/>
    </a>
    <div class="foto_links">
        <div class="foto_download"><a href="<?=url("/download.php?imagem=storage{$img}");?>&evento=<?= $post->title; ?>" title="Baixar Foto"><i class="fa fa-download"></i> Baixar</a></div>
        <!-- <div class="fb-like" data-href="< ?= HOME; ?>/like/< ?= $post_name; ?>/< ?= $FotoName[2]; ?>" data-width="160" data-layout="button_count" data-show-faces="false" data-send="true"></div> -->
        <div class="clear"></div>
    </div>
</li>