<?php $v->layout('_theme');?>
<main class="main_content container">
    <section class="content main_entretenimento_form" style="margin-bottom: 50px;">
        <div class="main_entretenimento_page_title">
            <h1>Solicitar cobertura</h1>
            <p>Que tal ter a cobertura do Portal SBN na sua festa? Legal né? Solicite nossa presença através do formulário abaixo e entraremos em contato.</p>
        </div>
        
        <form name="solicitar-cobertura" method="post" class="j_solicite_form" action="">
            <input type="hidden" name="action" value="solicitar_cobertura"/>
            <input type="hidden" name="solicite_estate" value="<?= $estate;?>"/>
            <label class="label form_30">
                <span class="legend">Seu Nome <b>*</b></span>
                <input type="text" name="solicite_user_name" required/>
            </label>
            <label class="label form_30">
                <span class="legend">E-mail <b>*</b></span>
                <input type="email" name="solicite_user_email" required/>
            </label>
            <label class="label form_30" style="margin-right: 0;">
                <span class="legend">Telefone <b>*</b></span>
                <input type="tel" name="solicite_user_phone" placeholder="(DDD)0000-0000" required/>
            </label>

            <label class="label form_100">
                <span class="legend">Nome do Evento <b>*</b></span>
                <input type="text" name="solicite_event_name" required/>
            </label>

            <label class="label" style="width: 100%; max-width: fit-content">
                <span style="margin-bottom: 20px; float: left; width: 100%;">Tipo de evento <b>*</b></span>
                
                <div class="clear"></div>
                <label class="label-check check-cat" style="float: left;">
                    <input type="radio" name="solicite_event_type" style="float: left; position: absolute;" value="1"/>
                    <span style="margin-left: 20px; color: #666;">Evento Aberto</span>
                </label>
                <label class="label-check check-cat" style="float: left; margin-left: 10px;">
                    <input type="radio" name="solicite_event_type" style="float: left; position: absolute;" value="2"/>
                    <span style="margin-left: 20px; color: #666;">Evento Fechado</span>
                </label>
                <div class="clear"></div>
            </label>

            <label class="label form_30">
                <span class="legend">Data do evento <b>*</b></span>
                <input type="text" name="solicite_event_date" class="datetimepicker" required/>
            </label>
            <label class="label form_30">
                <span class="legend">Cidade do evento <b>*</b></span>
                <input type="text" name="solicite_event_city" required/>
            </label>
            <label class="label form_30" style="margin-right: 0;">
                <span class="legend">Local do evento <b>*</b></span>
                <input type="text" name="solicite_event_local" placeholder="Clube, área, etc." required/>
            </label>

            <label class="label form_100">
                <span class="legend">Detalhes <b>*</b></span>
                <textarea name="solicite_event_details" rows="10" placeholder="Horários, venda de ingressos, informações adicionais." required></textarea>
            </label>

            <button class="btn btn-blue">Enviar <img class="form-load margin-left-10 j_more_load" style="display:none; float: right; max-height: 15px;" alt="Enviando Requisição!" title="Enviando Requisição!" src="<?= INCLUDE_PATH; ?>/img/load.gif"/></button>
        
        </form>
        
        <div class="clear"></div>
    </section>
</main>