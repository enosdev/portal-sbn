<?php
session_start();
require_once '../../_app/Config.inc.php';
$getData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$setData = array_map('strip_tags', $getData);
$setData = array_map('trim', $getData);
$jSon['result'] = null;

if (isset($setData['question_id'])):
    //Adiciona voto na enquete
    $readQuestion = new Read;
    $readQuestion->FullRead("SELECT question_votes FROM pc_enquete_question WHERE question_id = :id", "id={$setData['question_id']}");
    $Dados = ["question_votes" => $readQuestion->getResult()[0]['question_votes'] + 1, "question_last_vote_date" => date('Y-m-d H:i:s')];
    $updateQuestion = new Update;
    $updateQuestion->ExeUpdate("pc_enquete_question", $Dados, "WHERE question_id = :id", "id={$setData['question_id']}");
    if ($updateQuestion->getResult()):

        $ip = $_SERVER['REMOTE_ADDR'];
        $ag = $_SERVER['HTTP_USER_AGENT'];
        $d = time() + (15 * 24 * 60 * 60);
        // Data atual mais 7 dias, vezes 24 horas, vezes 60 mins, vezes 60segs.

        //$times = date("d-m-Y", $d);
        // Mostra 00-00-0000

        $Da = [ 'ip' => $ip, 'times' => $d, 'enquete_id' => $setData['enquete_id'], 'agent' => $ag ];
        /* o ip com as informações */
        $create = new Create;
        $create->ExeCreate("enquete_ip", $Da);
        
        //Seta o cookie e não deixa o usuário votar por 3 dias
        //setcookie('enquete-'.$setData['enquete_id'], $setData['enquete_id'], (time() + (15 * 24 * 3600)), '/');
        
        //Consulta questões da enquete
        $readQuestions = new Read;
        $readQuestions->ExeRead("pc_enquete_question", "WHERE question_enquete_id = :id ORDER BY question_votes DESC, question_title ASC", "id={$setData['enquete_id']}");
        if ($readQuestions->getResult()):
            $totalVotes = 0;
            foreach ($readQuestions->getResult() as $Sum):
                $totalVotes += $Sum['question_votes'];
            endforeach;

            foreach ($readQuestions->getResult() as $Ques):
                //Calcula o percentual dos votos
                $percent = ( $Ques['question_votes'] == 0 ? 0 : round($Ques['question_votes'] / $totalVotes * 100) );

                $jSon['result'] .= '
                <article>
                    <h1>' . $Ques['question_title'] . ' (' . $percent . '%, ' . $Ques['question_votes'] . ' Votos)</h1>
                    <div class="percent_bar">
                        <div class="percent" style="width: ' . $percent . '%;"></div>
                    </div>
                </article>
                ';
            endforeach;
        endif;
        $jSon['accept'] = "Obrigado por votar na nossa enquete!";
    endif;
endif;
echo json_encode($jSon);