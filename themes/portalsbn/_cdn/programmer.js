$(function () {
	HOME = $('link[rel=canonical]').attr('href');
	SITE = $('link[rel=site]').attr('href');

	var figure = $('figure');
	if (figure.hasClass('align-left')) {
		figure.addClass('left');
	}

	// ###############################################################
	// ######################## MENU MOBILE ##########################
	// ###############################################################
	$('.main_mobile_action').click(function () {
		if (!$(this).hasClass('active')) {
			$(this).addClass('active');
			$('.main_header_nav').animate({ left: '0px' }, 300);
		} else {
			$(this).removeClass('active');
			$('.main_header_nav').animate({ left: '-100%' }, 300);
		}
	});

	// ###############################################################
	// ########################## MENU FIXO ##########################
	// ###############################################################
	var nav = $('#menuHeader');
	$(window).scroll(function () {
		if ($(this).scrollTop() > 168) {
			nav.addClass('fixed_menu');
			$('body').css('padding-top', 43);
		} else {
			nav.removeClass('fixed_menu');
			$('body').css('padding-top', 0);
		}
	});

	// ###############################################################
	// ########################### ENQUETE ###########################
	// ###############################################################
	var enquete = $('.j_enquete_form');
	var enqueteResults = $('.j_enquete_results');
	var count = 0;

	/* Adiciona o Voto */
	$('.j_enquete').submit(function () {
		var form = $(this);
		var data = form.serialize();
		var btn = $('.j_enquete_action');

		if (!data) {
			jErro('Selecione um item da lista', 'PC_ALERT', count);
			return;
		}

		$.ajax({
			// url: HOME + '/_cdn/ajax/Enquete.ajax.php',
			url: SITE + '/enquete/votar',
			data: data,
			type: 'POST',
			dataType: 'JSON',
			beforeSend: function () {
				btn.html('Enviando...');
			},
			success: function (resposta) {
				btn.html('Votar');

				if (resposta.accept) {
					// jErro(resposta.accept, 'PC_ACCEPT', count);
					$('.javotou').html(resposta.accept);

					enquete.fadeOut(300, function () {
						enquete.addClass('display-none');
					});

					enqueteResults.html(resposta.result).fadeIn(300, function () {
						enqueteResults.removeClass('display-none');
					});

					let get = { id: resposta.idPoll, time: resposta.time, ip: resposta.ip };
					localStorage.setItem('WkdjWgd8df#jdsnHFn4' + resposta.idPoll, JSON.stringify(get));

				} else if (resposta.info) {
					jErro(resposta.info, 'PC_INFO', count);
				} else if (resposta.alert) {
					jErro(resposta.alert, 'PC_ALERT', count);
				} else if (resposta.error) {
					jErro(resposta.error, 'PC_ERROR', count);
				}
			}
		});
		count++;
		return false;
	});

	/* Exibe/oculta resultados */
	$('.j_view_results').click(function () {
		if (enquete.hasClass('display-none')) {
			enquete.fadeIn(300, function () {
				enquete.removeClass('display-none');
			});

			enqueteResults.fadeOut(300, function () {
				enqueteResults.addClass('display-none');
			});
		} else {
			enquete.fadeOut(300, function () {
				enquete.addClass('display-none');
			});

			enqueteResults.fadeIn(300, function () {
				enqueteResults.removeClass('display-none');
			});
		}
	});

	// ###############################################################
	// ############### FORM - SOLICITAÇÃO DE COBERTURA ###############
	// ###############################################################
	var countForm = 0;
	$('.j_solicite_form').submit(function () {
		countForm++;
		var form = $(this);
		var data = form.serialize();

		$.ajax({
			url: HOME + '/themes/portalsbn/ajax/Forms.ajax.php',
			data: data,
			type: 'POST',
			dataType: 'JSON',
			beforeSend: function () {
				form.find('.form-load').fadeIn(400);
			},
			success: function (resposta) {
				form.find('.form-load').fadeOut(400);

				if (resposta.accept) {
					jErro(resposta.accept, 'PC_ACCEPT', countForm);
					setInterval(function () {
						window.location.href = HOME + '/entretenimento';
					}, 5000);
				} else if (resposta.info) {
					jErro(resposta.info, 'PC_INFO', countForm);
				} else if (resposta.alert) {
					jErro(resposta.alert, 'PC_ALERT', countForm);
				} else if (resposta.error) {
					jErro(resposta.error, 'PC_ERROR', countForm);
				}
			}
		});

		return false;
	});

	// ###############################################################
	// ######### FORM - SOLICITAÇÃO DE DIVULGAÇÃO DE EVENTO ##########
	// ###############################################################
	var countForm = 0;
	$('.j_solicite_form_cover').submit(function () {
		countForm++;
		var form = $(this);
		var data = form.serialize();

		$.ajax({
			url: HOME + '/themes/portalsbn/ajax/Forms.ajax.php',
			data: data,
			type: 'POST',
			dataType: 'JSON',
			beforeSend: function () {
				form.find('.form-load').fadeIn(400);
			},
			success: function (resposta) {
				form.find('.form-load').fadeOut(400);

				if (resposta.accept) {
					jErro(resposta.accept, 'PC_ACCEPT', countForm);
					setInterval(function () {
						window.location.href = HOME + '/entretenimento';
					}, 5000);
				} else if (resposta.info) {
					jErro(resposta.info, 'PC_INFO', countForm);
				} else if (resposta.alert) {
					jErro(resposta.alert, 'PC_ALERT', countForm);
				} else if (resposta.error) {
					jErro(resposta.error, 'PC_ERROR', countForm);
				}
			}
		});

		return false;
	});

	/*UPLOAD IMAGEM DE CAPA DO POST*/
	$('.j_form_cover').change(function () {
		var file = $(this);
		var form = $('.j_solicite_form_cover');

		countForm++;

		form.ajaxSubmit({
			url: HOME + '/themes/portalsbn/ajax/Forms.ajax.php',
			data: { action: 'upload_form_cover' },
			type: 'POST',
			dataType: 'json',
			beforeSubmit: function () { },
			uploadProgress: function (evento, posicao, total, completo) {
				$('.j_post_cover_bar').fadeIn(300);
				$('.j_post_cover_bar .bar').text(completo + '%').width(completo + '%');
			},
			success: function (resposta) {
				$('.j_post_cover_bar').fadeOut(300);

				if (resposta.accept) {
					jErro(resposta.accept, 'PC_ACCEPT', countForm);
				} else if (resposta.info) {
					jErro(resposta.info, 'PC_INFO', countForm);
				} else if (resposta.alert) {
					jErro(resposta.alert, 'PC_ALERT', countForm);
				} else if (resposta.error) {
					jErro(resposta.error, 'PC_ERROR', countForm);
				} else {
					file.val('');
				}
			}
		});

		return false;
	});

	// ###############################################################
	// ######################### PESQUISA ############################
	// ###############################################################
	$('.j_search_button').click(function () {
		$(this).toggleClass('active');

		//        $('.j_search').fadeToggle("fast", function () {
		//            $('.j_search input').focus();
		//        });

		var Search = $('.j_search_form');
		var SearchBg = $('.dashboard_search_bg');
		Search.fadeIn(200, function () {
			$(Search).find('input').focus();
		});
		$(SearchBg).click(function () {
			$(Search).fadeOut(200, function () {
				$(Search).css('display', 'none');
				$('.j_search_button').removeClass('active');
			});
		});
	});

	/*Fecha modal ao pressionar tecla ESC*/
	$(document).bind('keydown', function (e) {
		if (e.which === 27) {
			$('.j_search_form').fadeOut(200, function () {
				$('.j_search_form').css('display', 'none');
			});
			$('.modal-container').fadeOut(200);
		}
	});

	// ###############################################################
	// ####################### SELEÇÃO ESTADOS #######################
	// ###############################################################
	// $('.main_header_estados').hide();
	// $('.main_header_topo_estados').click(function () {
	// 	if (!$(this).hasClass('active')) {
	// 		$(this).addClass('active');
	// 	} else {
	// 		$(this).removeClass('active');
	// 	}
	// 	$('.main_header_estados').slideToggle();
	// 	return false;
	// });

	// ###############################################################
	// ################### PUBLICIDADE SLIDE TOPO ####################
	// ###############################################################
	$('.main_header_publicidade').hide();
	$('.main_header_topo_data').click(function () {
		if (!$(this).hasClass('active')) {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
		$('.main_header_publicidade').slideToggle();
		return false;
	});

	// ###############################################################
	// ######################## MOBILE SLIDES ########################
	// ###############################################################
	/*SLIDE DESTAQUE MOBILE*/
	var actionSlideDestaques = setInterval(function () {
		slideGo('destaques');
	}, 7000);
	$('.slide_nav_destaques.go').click(function () {
		clearInterval(actionSlideDestaques);
		slideGo('destaques');
	});
	$('.slide_nav_destaques.back').click(function () {
		clearInterval(actionSlideDestaques);
		slideBack('destaques');
	});

	/*SLIDE VÍDEOS MOBILE*/
	var actionSlideVideos = setInterval(function () {
		slideGo('videos');
	}, 7000);
	$('.slide_nav_videos.go').click(function () {
		clearInterval(actionSlideVideos);
		slideGo('videos');
	});
	$('.slide_nav_videos.back').click(function () {
		clearInterval(actionSlideVideos);
		slideBack('videos');
	});

	/*SLIDE GALERIAS MOBILE*/
	var actionSlideGalerias = setInterval(function () {
		slideGo('galerias');
	}, 7000);
	$('.slide_nav_galerias.go').click(function () {
		clearInterval(actionSlideGalerias);
		slideGo('galerias');
	});
	$('.slide_nav_galerias.back').click(function () {
		clearInterval(actionSlideGalerias);
		slideBack('galerias');
	});

	function slideGo(slide) {
		if ($('.slide_mobile_item_' + slide + '.first').next('.slide_mobile_item_' + slide).size()) {
			$('.slide_mobile_item_' + slide + '.first').fadeOut(200, function () {
				$('.slide_mobile_item_' + slide + '').fadeOut().removeClass('first');
				$(this).next().fadeIn().addClass('first');
			});
		} else {
			$('.slide_mobile_item_' + slide + '.first').fadeOut(200, function () {
				$('.slide_mobile_item_' + slide).removeClass('first');
				$('.slide_mobile_item_' + slide + ':eq(0)').fadeIn().addClass('first');
			});
		}
	}

	function slideBack(slide) {
		if ($('.slide_mobile_item_' + slide + '.first').index() > 1) {
			$('.slide_mobile_item_' + slide + '.first').fadeOut(200, function () {
				$('.slide_mobile_item_' + slide + '').fadeOut().removeClass('first');
				$(this).prev().fadeIn().addClass('first');
			});
		} else {
			$('.slide_mobile_item_' + slide + '.first').fadeOut(200, function () {
				$('.slide_mobile_item_' + slide).removeClass('first');
				$('.slide_mobile_item_' + slide + ':last-of-type').fadeIn().addClass('first');
			});
		}
	}

	// ###############################################################
	// ####################### DESKTOP SLIDES ########################
	// ###############################################################
	/*SLIDE DESTAQUE DESKTOP*/
	var actionSlideDeskDestaques = setInterval(function () {
		slideDesktopGo('destaques');
	}, 7000);
	$('.slide_nav_destaques.go').click(function () {
		clearInterval(actionSlideDeskDestaques);
		slideDesktopGo('destaques');
	});
	$('.slide_nav_destaques.back').click(function () {
		clearInterval(actionSlideDestaques);
		slideDesktopBack('destaques');
	});

	$('.slide_mark_destaques').click(function () {
		if (!$(this).hasClass('active')) {
			var goto = $(this).attr('id');
			$('.slide_item_destaques.first').fadeOut(200, function () {
				$('.slide_item_destaques').fadeOut().removeClass('first');
				$('.slide_item_destaques:eq(' + goto + ')').fadeIn().addClass('first');
				slideDesktopMark('destaques');
				clearInterval(actionSlideDeskDestaques);
			});
		}
	});

	/*SLIDE VÍDEOS DESKTOP*/
	var actionSlideDeskVideos = setInterval(function () {
		slideDesktopGo('videos');
	}, 7000);
	$('.slide_nav_videos.go').click(function () {
		clearInterval(actionSlideDeskVideos);
		slideDesktopGo('videos');
	});
	$('.slide_nav_videos.back').click(function () {
		clearInterval(actionSlideVideos);
		slideDesktopBack('videos');
	});

	$('.slide_mark_videos').click(function () {
		if (!$(this).hasClass('active')) {
			var goto = $(this).attr('id');
			$('.slide_item_videos.first').fadeOut(200, function () {
				$('.slide_item_videos').fadeOut().removeClass('first');
				$('.slide_item_videos:eq(' + goto + ')').fadeIn().addClass('first');
				slideDesktopMark('videos');
				clearInterval(actionSlideDeskVideos);
			});
		}
	});

	/*SLIDE VÍDEOS DESKTOP*/
	var actionSlideDeskGalerias = setInterval(function () {
		slideDesktopGo('galerias');
	}, 7000);
	$('.slide_nav_galerias.go').click(function () {
		clearInterval(actionSlideDeskGalerias);
		slideDesktopGo('galerias');
	});
	$('.slide_nav_galerias.back').click(function () {
		clearInterval(actionSlideGalerias);
		slideDesktopBack('galerias');
	});

	$('.slide_mark_galerias').click(function () {
		if (!$(this).hasClass('active')) {
			var goto = $(this).attr('id');
			$('.slide_item_galerias.first').fadeOut(200, function () {
				$('.slide_item_galerias').fadeOut().removeClass('first');
				$('.slide_item_galerias:eq(' + goto + ')').fadeIn().addClass('first');
				slideDesktopMark('galerias');
				clearInterval(actionSlideDeskGalerias);
			});
		}
	});

	function slideDesktopGo(slide) {
		if ($('.slide_item_' + slide + '.first').next('.slide_item_' + slide).size()) {
			$('.slide_item_' + slide + '.first').fadeOut(200, function () {
				$('.slide_item_' + slide).fadeOut().removeClass('first');
				$(this).next().fadeIn().addClass('first');
				slideDesktopMark(slide);
			});
		} else {
			$('.slide_item_' + slide + '.first').fadeOut(200, function () {
				$('.slide_item_' + slide).removeClass('first');
				$('.slide_item_' + slide + ':eq(0)').fadeIn().addClass('first');
				slideDesktopMark(slide);
			});
		}
	}

	function slideDesktopBack(slide) {
		if ($('.slide_item_' + slide + '.first').index() > 1) {
			$('.slide_item_' + slide + '.first').fadeOut(200, function () {
				$('.slide_item_' + slide + '').fadeOut().removeClass('first');
				$(this).prev().fadeIn().addClass('first');
				slideDesktopMark(slide);
			});
		} else {
			$('.slide_item_' + slide + '.first').fadeOut(200, function () {
				$('.slide_item_' + slide).removeClass('first');
				$('.slide_item_' + slide + ':last-of-type').fadeIn().addClass('first');
				slideDesktopMark(slide);
			});
		}
	}

	function slideDesktopMark(slide) {
		var slideThis = $('.slide_item_' + slide + '.first').index();
		$('.slide_mark_' + slide).removeClass('active');
		$('.slide_mark_' + slide + ':eq(' + slideThis + ')').addClass('active');
	}

	// ###############################################################
	// ######################## ÚLTIMAS HEADER #######################
	// ###############################################################
	setInterval(function () {
		ultimasHeaderGo();
	}, 8000);
	function ultimasHeaderGo() {
		if ($('.main_header_ultimas_item_slide.first').next('.main_header_ultimas_item_slide').size()) {
			$('.main_header_ultimas_item_slide.first').fadeOut(200, function () {
				$('.main_header_ultimas_item_slide').fadeOut().removeClass('first');
				$(this).next().fadeIn().addClass('first');
			});
		} else {
			$('.main_header_ultimas_item_slide.first').fadeOut(200, function () {
				$('.main_header_ultimas_item_slide').removeClass('first');
				$('.main_header_ultimas_item_slide:eq(0)').fadeIn().addClass('first');
			});
		}
	}

	// ###############################################################
	// ####################### ÚLTIMAS ESTADOS #######################
	// ###############################################################
	$('.main_ultimas_nav_ba').click(function () {
		$('.main_ultimas_nav').removeClass('active');
		$(this).addClass('active');
		$('.main_ultimas_content').fadeOut(200);
		$('.main_ultimas_ba').fadeIn(200);
	});

	$('.main_ultimas_nav_es').click(function () {
		$('.main_ultimas_nav').removeClass('active');
		$(this).addClass('active');
		$('.main_ultimas_content').fadeOut(200);
		$('.main_ultimas_es').fadeIn(200);
	});

	// ###############################################################
	// ####################### PODCAST TOGGLE ########################
	// ###############################################################
	$('.j_podcast_action').click(function () {
		$('.j_podcast').slideToggle(250);
	});

	// ###############################################################
	// ######################### SCROLL TOP ##########################
	// ###############################################################
	$('.main_btn_topo').click(function () {
		$('html, body').animate({ scrollTop: 0 }, 1000);
	});

	// ###############################################################
	// ########################### SINGLE ############################
	// ###############################################################

	// ######################## VIDEO CONTROL ########################
	$('.htmlchars video').attr('controls', 'controls');

	// ######################### ALT IMAGEM ##########################
	/*FIGURE*/
	var figureImgCount = $('.htmlchars figure img').length;
	$(window).load(function () {
		for ($c = 0; $c < figureImgCount; $c++) {
			var figureImg = $('.htmlchars figure img:eq(' + $c + ')');
			figureImg.each(function () {
				var imageCaption = $(this).attr('alt');
				if (imageCaption !== undefined) {
					var imgWidth = $(this).width();
					var imgHeight = $(this).height();
					var position = $(this).position();
					var positionTop = position.top + imgHeight - 26;
					$('.img_caption:eq(' + $c + ')').remove();
					$("<span class='img_caption'><em>" + imageCaption + '</em></span>')
						.css({
							position: 'absolute',
							top: positionTop + 'px',
							right: '0'
						})
						.appendTo('.htmlchars figure:eq(' + $c + ')');
					$('figure:eq(' + $c + ')').css({
						'max-width': imgWidth + 'px'
					});
				}
			});
		}
	});
	// $(window).resize(function () {
	//     for ($c = 0; $c < figureImgCount; $c++) {
	//         var figureImg = $(".htmlchars figure img:eq(" + $c + ")");
	//         figureImg.each(function () {
	//             var imageCaption = $(this).attr("alt");
	//             if (imageCaption !== undefined) {
	//                 var imgWidth = $(this).width();
	//                 var imgHeight = $(this).height();
	//                 var position = $(this).position();
	//                 var positionTop = (position.top + imgHeight - 26);
	//                 $(".img_caption:eq(" + $c + ")").remove();
	//                 $("<span class='img_caption'><em>" + imageCaption +
	//                         "</em></span>").css({
	//                     "position": "absolute",
	//                     "top": positionTop + "px",
	//                     "right": "0"
	//                 }).appendTo('.htmlchars figure:eq(' + $c + ')');
	//             }
	//         });
	//     }
	// });

	// ###############################################################
	// ########################## LOAD MORE ##########################
	// ###############################################################
	$('.j_load_more').click(function () {
		var offset = $('.main_content .main_box_news').length;
		var link = $(this).attr('id');
		var category = $(this).attr('data-category');
		var local = $('.j_more').attr('id');

		$.ajax({
			url: SITE + '/artigo/loadmore',
			data: { offset: offset, link: link, local: local, category: category },
			type: 'POST',
			dataType: 'json',
			beforeSend: function () {
				$('.j_load_more').fadeOut(300, function () {
					$('.j_more_load').fadeIn(300);
				});
			},
			success: function (resposta) {
				console.clear();
				console.log(resposta.offset);
				if (resposta.accept) {
					$('.j_more_load').fadeOut(300, function () {
						$('.j_load_more').fadeIn(300);
						$(resposta.accept).appendTo('.j_more');
					});
				}
			}
		});

		return false;
	});

	// ###############################################################
	// ######################### COUNTDOWN ##########################
	// ###############################################################
	var DateClock = $('.j_clock').attr('datetime');
	$('.j_clock').countdown(DateClock, function (event) {
		$(this).html(event.strftime('Restam %D dias %H:%M:%S'));
	});

	var DateClockEnquete = $('.j_clock_enquete').attr('datetime');
	$('.j_clock_enquete').countdown(DateClockEnquete, function (event) {
		$(this).html(event.strftime('Finaliza em %D dias %H:%M:%S'));
	});

	// ###############################################################
	// ######################### INPUT MASK ##########################
	// ###############################################################
	$('input[type="tel"]').mask('(99)99999-9999');
	$('.mask-real').mask('000.000.000.000.000,00', { reverse: true });

	// ###############################################################
	// ########################## FANCYBOX ###########################
	// ###############################################################
	$('[data-fancybox]').fancybox({
		arrows: true,
		animationEffect: 'fade',
		transitionEffect: 'fade',
		loop: false
	});

	/* Carrega POP-UP */
	function fancyLoad(imagem) {
		$.fancybox.open(
			'<div class="message" style="background:none; padding:15px;"><img src="' + imagem + '"/></div>'
		);
	}

	// ###############################################################
	// ####################### FANCYBOX ZOOM #########################
	// ###############################################################
	$('.htmlchars img').addClass('atributo');
	var src = $('.atributo').eq(0).attr('src');
	var src1 = $('.atributo').eq(1).attr('src');
	var src2 = $('.atributo').eq(2).attr('src');
	var src3 = $('.atributo').eq(3).attr('src');
	var src4 = $('.atributo').eq(4).attr('src');
	var src5 = $('.atributo').eq(5).attr('src');
	var src6 = $('.atributo').eq(6).attr('src');
	var src7 = $('.atributo').eq(7).attr('src');
	var src8 = $('.atributo').eq(8).attr('src');
	var src9 = $('.atributo').eq(9).attr('src');
	var src10 = $('.atributo').eq(10).attr('src');

	$('.atributo').wrap('<a class="link"></a>');
	$('.htmlchars a.link').addClass('fancybox');
	$('.htmlchars a.link').attr('data-fancybox', 'galeria');

	$('.htmlchars a.fancybox').eq(0).attr('href', src);
	$('.htmlchars a.fancybox').eq(1).attr('href', src1);
	$('.htmlchars a.fancybox').eq(2).attr('href', src2);
	$('.htmlchars a.fancybox').eq(3).attr('href', src3);
	$('.htmlchars a.fancybox').eq(6).attr('href', src4);
	$('.htmlchars a.fancybox').eq(5).attr('href', src5);
	$('.htmlchars a.fancybox').eq(6).attr('href', src6);
	$('.htmlchars a.fancybox').eq(7).attr('href', src7);
	$('.htmlchars a.fancybox').eq(8).attr('href', src8);
	$('.htmlchars a.fancybox').eq(9).attr('href', src9);
	$('.htmlchars a.fancybox').eq(10).attr('href', src10);

	// $('.htmlchars a.fancybox').wrap('<figure></figure>');
	// $('.htmlchars figure').attr('class', 'fot');

	var texto = $('.htmlchars a.fancybox img').eq(0).attr('alt');
	var texto1 = $('.htmlchars a.fancybox img').eq(1).attr('alt');
	var texto2 = $('.htmlchars a.fancybox img').eq(2).attr('alt');
	var texto3 = $('.htmlchars a.fancybox img').eq(3).attr('alt');
	var texto4 = $('.htmlchars a.fancybox img').eq(4).attr('alt');
	var texto5 = $('.htmlchars a.fancybox img').eq(5).attr('alt');
	var texto6 = $('.htmlchars a.fancybox img').eq(6).attr('alt');
	var texto7 = $('.htmlchars a.fancybox img').eq(7).attr('alt');
	var texto8 = $('.htmlchars a.fancybox img').eq(8).attr('alt');
	var texto9 = $('.htmlchars a.fancybox img').eq(9).attr('alt');
	var texto10 = $('.htmlchars a.fancybox img').eq(10).attr('alt');

	$('.htmlchars a.fancybox').eq(0).attr('title', texto);
	$('.htmlchars a.fancybox').eq(1).attr('title', texto1);
	$('.htmlchars a.fancybox').eq(2).attr('title', texto2);
	$('.htmlchars a.fancybox').eq(3).attr('title', texto3);
	$('.htmlchars a.fancybox').eq(4).attr('title', texto4);
	$('.htmlchars a.fancybox').eq(5).attr('title', texto5);
	$('.htmlchars a.fancybox').eq(6).attr('title', texto6);
	$('.htmlchars a.fancybox').eq(7).attr('title', texto7);
	$('.htmlchars a.fancybox').eq(8).attr('title', texto8);
	$('.htmlchars a.fancybox').eq(9).attr('title', texto9);
	$('.htmlchars a.fancybox').eq(10).attr('title', texto10);

	// ###############################################################
	// ########################## MÁSCARAS ###########################
	// ###############################################################
	$('.form_date').mask('99/99/9999 99:99:99', { placeholder: ' ' });
	$('.form_tel').mask('(99)99999-9999', { placeholder: ' ' });

	// ###############################################################
	// ######################### DATEPICKER ##########################
	// ###############################################################
	$.datetimepicker.setLocale('pt');
	$('.datetimepicker').datetimepicker({
		mask: '39/19/9999 29:59',
		format: 'd/m/Y H:i'
	});

	$('.datepicker').datetimepicker({
		timepicker: false,
		format: 'd/m/Y'
	});

	// ###############################################################
	// ##################### PC TRIGGER MODAL ########################
	// ###############################################################
	/*RETORNA TRIGGER MODAL*/
	function jErro(ErrMsg, ErrNo, ErrCount) {
		var mensagem = ErrMsg;
		var err = ErrNo;
		var id = ErrCount;
		var modalbox = $('.trigger-modal-box');
		var modal = $('.trigger-modal');

		var accept = '<div class="trigger-modal trigger-modal-success" id="j' + id + '"><p>' + mensagem + '</p></div>';
		var info = '<div class="trigger-modal trigger-modal-info" id="j' + id + '"><p>' + mensagem + '</p></div>';
		var alert = '<div class="trigger-modal trigger-modal-alert" id="j' + id + '"><p>' + mensagem + '</p></div>';
		var error = '<div class="trigger-modal trigger-modal-error" id="j' + id + '"><p>' + mensagem + '</p></div>';

		/*Trigger accept .trigger-modal-accept*/
		if (err === 'PC_ACCEPT') {
			//modal.remove();
			modalbox.append(accept);
			modalbox.find('#j' + id).fadeIn(300);
			setInterval(function () {
				$(modalbox).find('#j' + id).animate({ right: '-120%' }, 200, function () {
					$(this).remove();
				});
			}, 5000);
			/*Trigger info .trigger-modal-info*/
		} else if (err === 'PC_INFO') {
			//modal.remove();
			modalbox.append(info);
			modalbox.find('#j' + id).fadeIn(300);
			setInterval(function () {
				$(modalbox).find('#j' + id).animate({ right: '-120%' }, 200, function () {
					$(this).remove();
				});
			}, 5000);
			/*Trigger alert .trigger-modal-alert*/
		} else if (err === 'PC_ALERT') {
			//modal.remove();
			modalbox.append(alert);
			modalbox.find('#j' + id).fadeIn(300);
			setInterval(function () {
				$(modalbox).find('#j' + id).animate({ right: '-120%' }, 200, function () {
					$(this).remove();
				});
			}, 5000);
			/*Trigger error .trigger-modal-error*/
		} else if (err === 'PC_ERROR') {
			//modal.remove();
			modalbox.append(error);
			modalbox.find('#j' + id).fadeIn(300);
			setInterval(function () {
				$(modalbox).find('#j' + id).animate({ right: '-120%' }, 200, function () {
					$(this).remove();
				});
			}, 5000);
		}
	}
});

// ###############################################################
// ######################### PC MODAL ############################
// ###############################################################
function getModal(classe) {
	var ModalContainer = $('.modal-bg');
	var ModalClose = $('.modal-box-action');

	var Modal = classe;
	var ModalContent = '.' + Modal;

	$(ModalContent).fadeIn(200, function () {
		$(ModalContent).css('display', 'block');
	});

	$(ModalClose).click(function () {
		$(ModalContent).fadeOut(200, function () {
			$(ModalContent).css('display', 'none');
		});
	});

	$(ModalContainer).click(function () {
		$(ModalContent).fadeOut(200, function () {
			$(ModalContent).css('display', 'none');
		});
	});
}
