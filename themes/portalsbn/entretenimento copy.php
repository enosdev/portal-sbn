<?php $v->layout('_theme');?>

<main class="main_content container">
    <div class="content">
        <section class="main_content_left container">
        
            <!-- funciona no mobile -->
            <aside class="main_banner_728 banner_top_content">
                <?= bannerAds(1,728,90); ?>
            </aside>

            <!-- vídeos tv sbn -->
            <section class="main_slide main_videos container">
                <h1 class="title"><i class="fa fa-youtube-play"></i> TV SBN</h1>

                <div class="main_slide_mobile">
                    <div class="slide_controll">
                        <div class="slide_nav slide_nav_videos back round"><i class="fa fa-chevron-left"></i></div>
                        <div class="slide_nav slide_nav_videos go round"><i class="fa fa-chevron-right"></i></div>
                    </div>

                    <?php
                    if ($tvSbn):
                        $i = 0;
                        foreach ($tvSbn as $Video):
                            $i++;
                            $first = ( $i == 1 ? ' first' : '' );
                            $v->insert("slide_tv", ["video" => $Video,"first" => $first, "mod" => "mobile"]);
                        endforeach;
                    endif;
                    ?>
                </div><!-- SLIDE MOBILE -->

                <div class="main_slide_videos">
                    <?php
                    if ($tvSbn):
                        $i = 0;
                        foreach ($tvSbn as $Video):
                            $i++;
                            $first = ( $i == 1 ? ' first' : '' );
                            $v->insert("slide_tv", ["video" => $Video,"first" => $first, "mod" => "normal"]);
                        endforeach;
                    endif;
                    ?>

                    <div class="main_slide_destaques_click">
                        <?php
                        if ($tvSbn):
                            $i = 0;
                            foreach ($tvSbn as $Video):
                                $i++;
                                $active = ( $i == 1 ? ' active' : '' );
                                $v->insert("slide_tv", ["video" => $Video,"active" => $active, "mod" => "link", "i" => $i]);
                            endforeach;
                        endif;
                        ?>
                    </div>
                </div><!-- SLIDE DESKTOP -->

                <div class="clear"></div>
            </section><!-- SLIDE VÍDEOS -->

            <section class="main_mais_noticias container" style="display:flex; flex-wrap: wrap;">
                <h1 class="font-zero">Mais Notícias</h1>
                <?php
                    if($maisNoticias):
                        foreach($maisNoticias as $news):
                            $v->insert("article_news", ["news" => $news]);
                        endforeach;
                    endif;
                ?>
            </section><!-- Mais Notícias -->


            <section class="main_slide main_galeria container">
                <h1 class="title"><i class="fa fa-picture-o"></i> Últimas Coberturas</h1>
                <div class="main_slide_mobile">
                    <div class="slide_controll">
                        <div class="slide_nav slide_nav_galerias back round"><i class="fa fa-chevron-left"></i></div>
                        <div class="slide_nav slide_nav_galerias go round"><i class="fa fa-chevron-right"></i></div>
                    </div>

                    <?php
                    if ($gallery):
                        $i = 0;
                        foreach ($gallery as $gal):
                            $i++;
                            $first = ( $i == 1 ? ' first' : '' );
                            $v->insert("slide_gallery", ["gal" => $gal,"first" => $first, "mod" => "mobile"]);
                        endforeach;
                    endif;
                    ?>
                </div><!-- SLIDE MOBILE -->


                <div class="main_slide_galeria">
                    <?php
                    if ($gallery):
                        $i = 0;
                        foreach ($gallery as $gal):
                            $i++;
                            $first = ( $i == 1 ? ' first' : '' );
                            $v->insert("slide_gallery", ["gal" => $gal,"first" => $first, "mod" => "normal"]);
                        endforeach;
                    endif;
                    ?>

                    <div class="main_slide_galeria_click">
                        <?php
                        if ($gallery):
                            $i = 0;
                            foreach ($gallery as $gal):
                                $i++;
                                $active = ( $i == 1 ? ' active' : '' );
                                $v->insert("slide_gallery", ["gal" => $gal,"active" => $active, "mod" => "link", "i" => $i]);
                            endforeach;
                        endif;
                        ?>
                    </div>
                </div><!-- SLIDE DESKTOP -->

                <div class="clear"></div>
            </section><!-- GALERIA DE FOTOS -->

            <section class="main_mais_noticias container" style="display:flex; flex-wrap: wrap;">
                <h1 class="font-zero">Coberturas</h1>
                <?php
                    if($maisGallery):
                        foreach($maisGallery as $gal):
                            $v->insert("article_gallery", ["gal" => $gal]);
                        endforeach;
                    endif;
                ?>
            </section><!-- Coberturas -->

            <aside class="main_banner margin-bottom-20 margin-top-10">
                <h1 class="font-zero">Publicidade</h1>
                <div class="banner_728" style="width: 100%; max-width:728px;"><?= bannerAds(2,728,90); ?></div>
                <div class="banner_220"><?= bannerAds(1,220,90); ?></div>
            </aside><!-- banner -->
        
        </section> <!-- CONTENT LEFT -->

        
        <div class="main_content_right container">
            <div class="main_sidebar container">
                <?php require (__DIR__ . '/inc/sidebar_top_entretenimento.php'); ?>
                <!-- < ?php require (__DIR__ . '/inc/sidebar_bottom_entretenimento.php'); ?> -->
            </div>
        </div><!-- CONTENT RIGHT -->


        <section class="main_outras_noticias container j_more" style="display:flex; flex-wrap: wrap;">
            <h1 class="main_entretenimento_title" style="border-bottom-width: 2px; margin-bottom: 10px;"><i class="fa fa-camera"></i> Mais Coberturas</h1>
                <?php
                    if($maisCoberturas):
                        foreach($maisCoberturas as $gal):
                            $v->insert("article_gallery", ["gal" => $gal]);
                        endforeach;
                    endif;
                ?>
            <div class="clear"></div>
        </section><!-- Outras Notícias -->


        <div class="container align-center">
            <a class="btn btn-blue text-uppercase margin-top-10 margin-bottom-15 j_load_more" id="index" rel="BA">Carregar Mais</a>
            <img class="form-load margin-right-15 j_more_load" style="display:none; padding: 15px;" alt="Enviando Requisição!" title="Enviando Requisição!" src="<?=theme("/img/load.gif");?>"/>
        </div>

        <div class="clear"></div>


    </div> <!-- CONTENT -->
</main>
