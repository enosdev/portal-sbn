<?php $v->layout('_theme');?>

<main class="main_content container">
    <div class="content">

        <div class="main_content_left container" style="width:100%; margin-bottom: 20px;">
            <article class="main_single_content" style="border-top-color:#0888CB;">
                <header style="border-bottom:none; text-align: center; margin-bottom: 0;">
                    <h1 style="font-weight:300;"><?=$post->title;?></h1>
                </header>
                <div class="clear" style="margin-bottom:20px"></div>
                <div class="htmlchars">
                    <?=html_entity_decode($post->content);?>
                </div>
            </article><!-- Content -->
        </div><!-- CONTENT FULL -->
        <div class="clear"></div>
    </div>
</main>