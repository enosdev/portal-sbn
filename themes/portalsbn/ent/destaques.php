<?php $v->layout('ent/_theme-ent');?>

<div class="magazine">
    <div class="primary">
        <div class="header">
            musa sbn
        </div>
        <div class="header2"></div>
        <h1 class="title">A sedução da futura engenheira</h1>
        <div class="column-2">
            <figure>
                <img src="<?=theme("/amostra/1.jpg");?>" alt="imagem 1">
                <span class="first-name">Munilly</span>
                <span class="last-name">Angeli</span>
                <div class="ficha">
                    <strong>FICHA TÉCNICA</strong>
                    <p>Fotografia: <strong>Midiã Almeida</strong></p>
                    <p>Beleza: <strong>Intima Lingerie</strong></p>
                    <p>Parceiro: <strong>Brilho's Perfumaria</strong></p>
                    <p>Modelo: <strong>Munilly Angeli</strong></p>
                    <p>Maquiagem/Iluminação: <strong>Mariada Sedda</strong></p>
                    <p>Locação: <strong>Pousada Maresias - Guriri</strong></p>
                </div>
            </figure>
            <div class="column-1">
                <span class="title-column">seja bem-vinda!</span>
                <p>Ela completa 25 anos em 6 de julho, é a veneziana e mora em São Mateus, onde cursa
                    Engenharia Mecânica numa instituição particular. Estamos falando da Munilly Angeli, a
                    beldade que estreia o projeto <strong>Musa SBN</strong> nesta primeira edição do
                    <strong>Jornal SBN</strong>.</p>
                <p>A loira de olhos verdes tem 1,68m de altura, 58kg e 98 de quadril. Ao receber o convite
                    para o primeiro ensaio fotográfico, que também será destaque no <strong>Portal
                        SBN</strong>, disse que
                    viveu dias de muita ansiedade até as fotos, na Pousada Mirante, em Guriri: “Amei! Será
                    muito bom para divulgação dos trabalhos de todos os envolvidos”.</p>
                <p>Munilly Angeli Souza é filha de Chirlei Maria Angeli e Joilson Ribeiro Souza. Gosta de ir
                    à praia, praticar esportes e sair com os amigos. Recorda que, quando criança participava
                    efetivamente de apresentações na escola e sempre era chamada para ser daminha em
                    casamentos.</p>
                <p>Com experiências também em passaria, diz que gosta de lidar com o público, mas avalia que
                    o trabalho com fotos e modelo consegue “disfarçar melhor quando está um pouquinho
                    insegura”.</p>
                <hr>
                <div class="agenda">
                    <div class="img-agenda">
                        <img class="img-fluid" src="<?=theme("/amostra/agenda.jpg");?>" alt="">
                    </div>
                    <div class="info">
                        <div class="title-agenda">
                            agenda
                        </div>
                        <div class="faixa">
                            <div class="data">
                                <span class="dia">12</span>
                                <span class="mes">JUN</span>
                            </div>
                            <div class="local">
                                <div class="loca">porto bier</div>
                                <small>São Mateus - 20h</small>
                            </div>
                        </div>
                        <div class="faixa">
                            <div class="data">
                                <span class="dia">30</span>
                                <span class="mes">JUN</span>
                            </div>
                            <div class="local">
                                <div class="loca">petiscos</div>
                                <small>Guriri - 20h</small>
                            </div>
                        </div>
                        <div class="contato">
                            Contato<br>
                            27 99883-1923
                        </div>
                    </div>
                    <div class="logo">
                        <img src="<?=theme("/amostra/logo-agenda.jpg");?>" alt="">
                    </div>
                </div>
            </div>
            <div class="column-1">
                <div class="img">
                    <img class="img-fluid" src="<?=theme("/amostra/2.jpg");?>" alt="">
                </div>
                <hr>
                <div class="banner">
                    <img class="img-max" src="<?=theme("/amostra/banner1.jpg");?>" alt="">
                </div>
            </div>
        </div>
        <div class="column-2 column-2-2">
            <div class="column-1">
                <div class="img">
                    <img class="img-fluid" src="<?=theme("/amostra/3.jpg");?>" alt="">
                </div>
                <div class="img">
                    <img class="img-fluid" src="<?=theme("/amostra/4.jpg");?>" alt="">
                </div>
                <span class="title-column">amor e carreira!</span>
                <p>Quem se interessou pela gata, pode ter esperança. A Musa SBN Mulnilly Angeli revela que
                    nunca teve namorado: “Acredito ainda não ter encontrado a pessoa certa”. Ela diz que
                    pensa em ter marido e filhos, mas antes pretende cuidar do futuro, concluindo os estudos
                    e se dedicando à carreira de engenheira mecânica.</p>
                <p>Se tem algo que incomoda a Munilly é ficar com fome ou perto de gente negativa. Por outro
                    lado, estar com a família a deixa muito feliz. Ela passou a infância frequentando em
                    Guriri e a praia, com as águas quentes e ambiente tranquilo, é um dos fatores que a
                    fizeram se apaixonar pela ilha!</p>
                <p>Ela diz que veio para São Mateus por conta da faculdade, mas acabou se adaptando e
                    conquistou muitas amizades. Tem um carinho especial pelo Espirito Santo, apesar de
                    precisar outros lugares fantásticos dos País: “A gente tem sempre algo a mais tem
                    relação ao lugar que a gente nasceu e mora”.</p>
                <hr>
                <div class="banner">
                    <img class="img-max" src="<?=theme("/amostra/banner2.jpg");?>" alt="">
                </div>
            </div>
            <div class="column-1">
                <div class="palavra">
                    <span class="title-column">palavra por palavra!</span>
                    <div class="block-palavras">
                        <div class="per">Jornal SBN - Deus?</div>
                        <div class="res"><span>-Munilly Angeli</span> - Vida</div>
                    </div>
                    <div class="block-palavras">
                        <div class="per">Jornal SBN - Família?</div>
                        <div class="res"><span>-Munilly Angeli</span> - Amor</div>
                    </div>
                    <div class="block-palavras">
                        <div class="per">Jornal SBN - Esturos?</div>
                        <div class="res"><span>-Munilly Angeli</span> - Futuro</div>
                    </div>
                    <div class="block-palavras">
                        <div class="per">Jornal SBN - Trabalho?</div>
                        <div class="res"><span>-Munilly Angeli</span> - Expectativa</div>
                    </div>
                    <div class="block-palavras">
                        <div class="per">Jornal SBN - Moda?</div>
                        <div class="res"><span>-Munilly Angeli</span> - Estilo</div>
                    </div>
                    <div class="block-palavras">
                        <div class="per">Jornal SBN - Vida?</div>
                        <div class="res"><span>-Munilly Angeli</span> - Alegria</div>
                    </div>
                    <div class="block-palavras">
                        <div class="per">Jornal SBN - Casamento?</div>
                        <div class="res"><span>-Munilly Angeli</span> - Futuro</div>
                    </div>
                    <div class="block-palavras">
                        <div class="per">Jornal SBN - Portal/Jornal SBN?</div>
                        <div class="res"><span>-Munilly Angeli</span> - Exxtouro!</div>
                    </div>
                </div>
                <div class="img projeto-musa">
                    <img class="img-fluid" src="<?=theme("/amostra/5.jpg");?>" alt="">
                    <div class="box-musa">
                        <div class="title-musa">projeto muda sbn</div>
                        <div class="linha">
                            <div class="item1">Idealização:</div>
                            <div class="item2">
                                <div>Luscivanio Lopes</div>
                                <div>André Oliveira</div>
                            </div>
                        </div>
                        <div class="linha">
                            <div class="item1">Coordenação:</div>
                            <div class="item2">
                                <div>Midiã Almeida</div>
                            </div>
                        </div>
                        <div class="linha">
                            <div class="item1">Ensaio Completo:</div>
                            <div class="item2">
                                <div>Portal SBN</div>
                                <div>portalsbn.com.br</div>
                            </div>
                        </div>
                        <div class="linha">
                            <div class="item1">Realização:</div>
                            <div class="item2">
                                <div>Rede SBN de Comunicação</div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="banner">
                    <img class="img-max" src="<?=theme("/amostra/banner3.jpg");?>" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<?php $v->start("scripts");?>
    <script src="<?=theme("/assets/js/magazine.js?v=".CONF_SITE_VERSION);?>"></script>
<?php $v->end();?>