<article class="item">
    <a href="<?= url("/evento/{$agenda->uri}");?>" title="<?=$agenda->title;?>">
        <img src="<?= image($agenda->cover, 480,320); ?>" alt="<?=$agenda->title;?>" title="<?=$agenda->title;?>"/>
    </a>

    <div class="main_box_news_desc">
        <ul class="social">
            <li class="social_item"><a href="https://api.whatsapp.com/send?text=Veja esta matéria:<?=url("/evento/{$agenda->uri}");?>" title="Compartilhe WhatsApp" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="whatsapp" rel="nofollow"><i class="fa fa-whatsapp"></i></a></li>
            <li class="social_item"><a href="http://www.facebook.com/sharer.php?u=<?=url("/evento/{$agenda->uri}");?>" title="Compartilhe no Facebook" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="facebook" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
            <li class="social_item"><a href="https://twitter.com/intent/tweet?url=<?=url("/evento/{$agenda->uri}");?>&text=<?=$agenda->title;?>" title="Conte isto no Twitter" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="twitter" rel="nofollow"><i class="fa fa-twitter"></i></a></li>
        </ul><!-- social share -->
        
        <a href="<?=url("/evento/{$agenda->uri}");?>" title="<?=$agenda->title;?>">
            <time datetime="<?=$agenda->post_at;?>"></time>
            <div class="clear"></div>
            <h1><?=$agenda->title;?></h1>
            <p class="tagline" style="margin-top:5px"><i class="fa fa-calendar"></i> <?=date('d/m/Y', strtotime($agenda->event_date));?></p>
            <p class="tagline" style="margin-top:5px"><i class="fa fa-map-marker"></i> <?=$agenda->local;?>, <?=$agenda->city;?>/<?=strtoupper($agenda->estate);?></p>
        </a>
    </div>
</article>