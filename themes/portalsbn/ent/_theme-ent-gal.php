<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?=$head;?>
    <link rel="shortcut icon" href="<?= theme("/img/icon.png?v=".CONF_SITE_VERSION); ?>"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600&display=swap" rel="stylesheet">
    
    <!-- <link rel="stylesheet" href="http://localhost/sbn/themes/portalsbn/_cdn/jquery.fancybox.css"> -->
    <link rel="stylesheet" href="<?= theme("/css/font-awesome.css?v=".CONF_SITE_VERSION); ?>"/>
    <link rel="stylesheet" href="<?= theme("/_cdn/jquery.fancybox.css?v=".CONF_SITE_VERSION); ?>"/>
    <link rel="stylesheet" href="<?= theme("/assets/css/gallery.min.css?v=".CONF_SITE_VERSION); ?>"/>
</head>

<body>

    <header class="header">
        <div class="topo">
            <div class="container">
                <div class="horas-state">
                    <div class="clock">
                        <span id="dia"></span> <span id="hora"></span>
                    </div>
                    <div class="state">
                        <a title="Selecione um estado" href="<?=CONF_URL_STATE;?>">
                            <img src="<?=theme("/img/main_header_es.png");?>" alt="Espirito Santo" title="Espírito Santo"/>
                            <span>Espírito Santo</span>
                        </a>
                    </div>
                </div>
                <div class="redes-sociais">
                    <p>Siga-nos</p>
                    <ul class="social">
                        <li class="social_item"><a class="facebook" href="https://www.facebook.com/<?= CONF_SOCIAL_FACEBOOK_PAGE; ?>" target="_blank" rel="nofollow" title="Portal SBN no Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li class="social_item"><a class="twitter" href="https://www.twitter.com/<?= CONF_SOCIAL_TWITTER_CREATOR; ?>" target="_blank" rel="nofollow" title="Portal SBN no Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li class="social_item"><a class="youtube" href="https://www.youtube.com/<?= CONF_SOCIAL_YOUTUBE_PAGE; ?>" target="_blank" rel="nofollow" title="Portal SBN no Youtube"><i class="fa fa-youtube"></i></a></li>
                        <li class="social_item"><a class="instagram" href="https://www.instagram.com/<?=CONF_SOCIAL_INSTAGRAM_PAGE;?>" target="_blank" rel="nofollow" title="Portal SBN no Instagram"><i class="fa fa-instagram"></i></a></li>
                        <li class="social_item"><a class="telegram" href="#" target="_blank" rel="nofollow" title="Portal SBN no Telegram"><i class="fa fa-telegram"></i></a></li>
                        <li class="social_item"><a class="whatsapp" href="#" target="_blank" rel="nofollow" title="Portal SBN no Telegram"><i class="fa fa-whatsapp"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container logo-menu">
            <div class="logo">
                <a href="<?=url("/entretenimento");?>">
                    <img class="img-fluid" src="<?=theme("/assets/img/header-logo-sbn.png");?>" alt="Entretenimento">
                </a>
            </div>
            <div class="menu-mobile">
                <i class="fa fa-navicon"></i>
            </div>
            <nav class="menu-mobile-script">
                <ul>
                    <li><a href="<?=url("/entretenimento");?>" title="Entretenimento">Início</a></li>
                    <li><a href="<?=url("/coberturas");?>" title="Todas as coberturas">Coberturas</a></li>
                    <li><a href="<?= url("/agenda"); ?>" title="Agenda">Agenda</a></li>
                    <li><a href="#" title="Divulgue seu evento">Divulgue</a></li>
                    <li><a href="#" title="Solicitar Cobertura">Solicitar</a></li>
                    <li><a href="<?=url();?>" title="Todas as Notícias">Notícias</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <?= $v->section("content"); ?>

    <script src="<?= theme("/_cdn/jquery.js?v=".CONF_SITE_VERSION); ?>"></script>
    <script src="<?= theme("/_cdn/jquery.fancybox.min.js?v=".CONF_SITE_VERSION); ?>"></script>
    <script src="<?=theme("/assets/js/script-clock.js?v=".CONF_SITE_VERSION);?>"></script>
    <script src="<?=theme("/assets/js/script-custom.js?v=".CONF_SITE_VERSION);?>"></script>

</body>

</html>