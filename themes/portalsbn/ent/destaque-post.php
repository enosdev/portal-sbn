<?php $v->layout('ent/_theme-ent');?>

<div class="magazine">
    <div class="primary">
        <h1 class="header">
            <?=$post->title_highlight;?>
        </h1>
        <div class="header2"></div>
        <h1 class="title"><?=$post->title;?></h1>
        <div class="column">
            <div class="principal">
                <figure>
                    <img src="<?= image($post->cover, 687, 454); ?>" alt="<?=$post->name;?>">
                    <?php
                        $nome_completo = explode('~',$post->name);
                        $nome_separado = explode(' ', $nome_completo[0]);
                    ?>
                    <h2>
                        <span style="color:<?=$nome_completo[1] ?? '';?>" class="first-name"><?=current($nome_separado);?></span>
                        <span style="color:<?=$nome_completo[1] ?? '';?>" class="last-name"><?=end($nome_separado);?></span>
                    </h2>
                    <?php
                        $datasheet = explode('~', $post->datasheet);
                    ?>
                    <div class="ficha" style="color:<?=$datasheet[1];?>">
                        <strong>FICHA TÉCNICA</strong>
                        <?php
                            foreach(explode("!!", $datasheet[0]) as $dt):
                                $res = explode(":", $dt);
                                echo "<p>{$res[0]}: <strong>{$res[1]}</strong></p>";
                            endforeach;
                        ?>
                    </div>
                </figure>
            </div>
            <div class="col2">
                <div class="column-1">
                    <span class="title-column"><?=$post->title2;?></span>
                    <?= html_entity_decode($post->content); ?>
                    <hr>
                    <div class="agenda">
                        <div class="img-agenda">
                            <img class="img-fluid" src="<?=theme("/amostra/agenda.jpg");?>" alt="">
                        </div>
                        <div class="info">
                            <div class="title-agenda">
                                agenda
                            </div>
                            <div class="faixa">
                                <div class="data">
                                    <span class="dia">12</span>
                                    <span class="mes">JUN</span>
                                </div>
                                <div class="local">
                                    <div class="loca">porto bier</div>
                                    <small>São Mateus - 20h</small>
                                </div>
                            </div>
                            <div class="faixa">
                                <div class="data">
                                    <span class="dia">30</span>
                                    <span class="mes">JUN</span>
                                </div>
                                <div class="local">
                                    <div class="loca">petiscos</div>
                                    <small>Guriri - 20h</small>
                                </div>
                            </div>
                            <div class="contato">
                                Contato<br>
                                27 99883-1923
                            </div>
                        </div>
                        <div class="logo">
                            <img src="<?=theme("/amostra/logo-agenda.jpg");?>" alt="">
                        </div>
                    </div>
                </div>
                <div class="column-1">
                    <div class="img">
                        <img class="img-fluid" src="<?= image($post->cover2, 338, 567); ?>" alt="<?=$post->name;?>">
                    </div>
                    <hr>
                    <div class="banner">
                        <?php
                            $publicity = explode('~',$post->publicity);
                            $publicityLink = $publicity[1];
                            
                            if($publicityLink != '#'){
                                $acha = stripos($publicityLink, 'http');
                                if($acha === false){
                                    $publicityLink = "https://{$publicityLink}";
                                }
                            }
                        ?>
                        <a href="<?=$publicityLink;?>" alt="publicidade" target="_blank">
                            <img class="img-max" src="<?= image($publicity[0], 400, 247); ?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="col2">
                <div class="column-1">
                    <div class="img">
                        <img class="img-fluid" src="<?= image($post->cover3, 338, 350); ?>" alt="<?=$post->name;?>">
                    </div>
                    <div class="img">
                        <img class="img-fluid" src="<?= image($post->cover4, 338, 309); ?>" alt="<?=$post->name;?>">
                    </div>
                    <span class="title-column"><?=$post->title3;?></span>
                    <?= html_entity_decode($post->content2); ?>
                    <hr>
                    <div class="banner">
                        <?php
                            $publicity2 = explode('~',$post->publicity2);
                            $publicity2Link = $publicity2[1];
                            
                            if($publicity2Link != '#'){
                                $acha2 = stripos($publicity2Link, 'http');
                                if($acha2 === false){
                                    $publicity2Link = "https://{$publicity2Link}";
                                }
                            }
                        ?>
                        <a href="<?=$publicity2Link;?>" alt="publicidade" target="_blank">
                            <img class="img-max" src="<?= image($publicity2[0], 400, 247); ?>" alt="">
                        </a>
                    </div>
                </div>
                <div class="column-1">
                    <div class="palavra">
                        <span class="title-column">palavra por palavra!</span>
                        <div class="block-palavras">
                            <?php
                                $wordbyword = explode('!!', $post->word_by_word);
                            ?>
                            <div class="per">Portal SBN - Deus?</div>
                            <div class="res"><span>-<?=explode('~',$post->name)[0]?></span> - <?=$wordbyword[0];?></div>
                        </div>
                        <div class="block-palavras">
                            <div class="per">Portal SBN - Família?</div>
                            <div class="res"><span>-<?=explode('~',$post->name)[0]?></span> - <?=$wordbyword[1];?></div>
                        </div>
                        <div class="block-palavras">
                            <div class="per">Portal SBN - Estudos?</div>
                            <div class="res"><span>-<?=explode('~',$post->name)[0]?></span> - <?=$wordbyword[2];?></div>
                        </div>
                        <div class="block-palavras">
                            <div class="per">Portal SBN - Trabalho?</div>
                            <div class="res"><span>-<?=explode('~',$post->name)[0]?></span> - <?=$wordbyword[3];?></div>
                        </div>
                        <div class="block-palavras">
                            <div class="per">Portal SBN - Moda?</div>
                            <div class="res"><span>-<?=explode('~',$post->name)[0]?></span> - <?=$wordbyword[4];?></div>
                        </div>
                        <div class="block-palavras">
                            <div class="per">Portal SBN - Vida?</div>
                            <div class="res"><span>-<?=explode('~',$post->name)[0]?></span> - <?=$wordbyword[5];?></div>
                        </div>
                        <div class="block-palavras">
                            <div class="per">Portal SBN - Casamento?</div>
                            <div class="res"><span>-<?=explode('~',$post->name)[0]?></span> - <?=$wordbyword[6];?></div>
                        </div>
                        <div class="block-palavras">
                            <div class="per">Portal SBN - O Portal SBN?</div>
                            <div class="res"><span>-<?=explode('~',$post->name)[0]?></span> - <?=$wordbyword[7];?></div>
                        </div>
                    </div>
                    <div class="img projeto-musa">
                        <img class="img-fluid" src="<?= image($post->cover5, 338, 561); ?>" alt="<?=explode('~',$post->name)[0]?>">
                        
                        <?php
                            $corArr = explode('~', $post->project);
                            $arr = explode('!!', $corArr[0]);
                        ?>
                        <div class="box-musa" style="color:<?=$corArr[1];?>">
                            <div class="title-musa"><?=$arr[0];?></div>
                            <?php
                                array_shift($arr);
                                foreach( $arr as $proj):
                                    $exp = explode(':', $proj);
                                    $nomes = explode('/',$exp[1]);
                                    $nomes2 = (isset($nomes[1]))? "<div>{$nomes[1]}</div>": '';
                                    echo "<div class=\"linha\">
                                        <div class=\"item1\">{$exp[0]}:</div>
                                        <div class=\"item2\">
                                            <div>{$nomes[0]}</div>
                                            {$nomes2}
                                        </div>
                                    </div>";
                                endforeach;
                            ?>
                        </div>
                    </div>
                    <hr>
                    <div class="banner">
                        <?php
                            $publicity3 = explode('~',$post->publicity3);
                            $publicity3Link = $publicity3[1];
                            
                            if($publicity3Link != '#'){
                                $acha3 = stripos($publicity3Link, 'http');
                                if($acha3 === false){
                                    $publicity3Link = "https://{$publicity3Link}";
                                }
                            }
                        ?>
                        <a href="<?=$publicity3Link;?>" alt="publicidade" target="_blank">
                            <img class="img-max" src="<?= image($publicity3[0], 400, 247); ?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>
    
    <div class="gallery--video">
        <div class="gallery">
            <div class="title">Galeria de imagens</div>
            <div class="images">
                <a href="<?=url("fotos/{$gallery->uri}");?>" target="_blank" title="<?=$gallery->title;?>"><em>Veja album de fotos</em></a>
                <?php
                    if($images[0] != 'no_image'):
                        foreach ($images[0] as $img):
                            $photo = explode(CONF_UPLOAD_DIR,$img);
                            $img = image(end($photo), 346, 198);
                            echo "<div><img src='{$img}' alt='' /></div>";
                        endforeach;
                    endif;
                ?>
            </div>
        </div>
        <div class="video">
            <div class="title">Veja o Vídeo</div>
            <a id="linkvideo" href="<?=youtube_link($post->video);?>">
                <img class="img-max" src="<?=youtube_photo($post->video, true);?>" alt="">
                <i class="fa fa-play fa-3x"></i>
            </a>
        </div>       
    </div>
    
    <div class="clear" style="margin: 1em 0;background-color:#ccc;padding:10px"></div>


    <div class="secondary">
        <div class="column-2">
            <h1 class="header"><?=$post->businessperson_title;?></h1>
            <h1><?=$post->businessperson_title1;?></h1>
            <div class="column-2-1">
                <?php
                    $aut = explode("!!", $post->businessperson_author);
                ?>
                <div class="author">
                    <span><?=$aut[0] ?? '';?></span><br>
                    <?=$aut[1] ?? '';?>
                </div>
                <?php
                    $arrWords = explode(" ", html_entity_decode($post->businessperson_content));
                    $words = implode(" ", array_slice($arrWords, 0, 180));
	                $words2 = implode(" ", array_slice($arrWords, 180));
                ?>
                <div class="texto-1c">
                    <?=$words;?>
                </div>
            </div>
            <div class="column-2-2">
                <figure>
                    <img src="<?= image($post->businessperson_photo, 447, 274); ?>" alt="">
                    <div class="site">PORTAL SBN</div>
                    <div class="sub-photo" style="color:<?=explode('~',$post->businessperson_photo_subtitle)[1];?>">
                        <?=explode('~',$post->businessperson_photo_subtitle)[0];?>
                    </div>
                </figure>
                <div class="text-2c">
                    <?=$words2;?>
                </div>
            </div>
        </div>
        <div class="column-2">           
            <div class="column-2-2">
                <figure>
                    <img src="<?= image($post->businessperson_photo2, 447, 559); ?>" alt="">
                    <div class="subtitle-photo">
                        <?=$post->businessperson_photo2_subtitle;?>
                    </div>
                </figure>
                <div class="banner" style="margin-top:10px">
                    <?php
                        $publicity4 = explode('~',$post->publicity4);
                        $publicity4Link = $publicity4[1];
                        
                        if($publicity4Link != '#'){
                            $acha4 = stripos($publicity4Link, 'http');
                            if($acha4 === false){
                                $publicity4Link = "https://{$publicity4Link}";
                            }
                        }
                    ?>
                    <a href="<?=$publicity4Link;?>" alt="publicidade" target="_blank">
                        <img class="img-max" src="<?= image($publicity4[0], 450, 150); ?>" alt="publicidade">
                    </a>
                </div>
            </div>
            <div class="column-2-1">
                <div class="texto-1c border">
                    <h2 class="title-column"><?=$post->businessperson_subtitle;?></h2>
                    <?= html_entity_decode($post->businessperson_content2);?>
                </div>
            </div>
        </div>
        
    </div>
</div>

<?php $v->start("scripts");?>
    <script src="<?=theme("/assets/js/magazine.js?v=".CONF_SITE_VERSION);?>"></script>
<?php $v->end();?>