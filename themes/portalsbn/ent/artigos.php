<?php $v->layout('ent/_theme-ent');?>

<div class="box-left">
    <div class="banner">
        <?= bannerAds(1,728,90); ?>
    </div>

    <div class="category-title">
        <!-- notícias relacionadas a entretenimento -->
        <h1><?=(strpos($_SERVER['REQUEST_URI'], 'video-entretenimento') === false)? 'Notícias' : 'Vídeos Entretenimento';?></h1>
    </div>
    <?php if($listNews):
        foreach($listNews as $news):?>
    <article>
        <div class="conteudo">
            <a href="<?=url("/artigos-entretenimento/{$news->uri}");?>" title="<?=$news->title;?>">
                <div class="img">
                    <img class="img-fluid" src="<?= image($news->cover, 344, 197); ?>" alt="">
                </div>
                <div class="titulo">
                    <h1><?=$news->title;?></h1>
                    <p><span style="background-color:<?=$cor;?>"><?=$news->category()->title;?></span> <span><?=$news->tag;?></span></p>
                </div>
            </a>
        </div>
        <hr>
    </article>
    <?php endforeach;
        endif;?>
    <?=$paginator;?>
</div>

<div class="box-right">
    <?php require(__DIR__."/aside.php"); ?>
</div>