<div class="banner linkvideo">
    <?php
    if($videosEntrLink):
        $strVideo = strpos($videosEntrLink->content, "youtube.com/embed/");
        $idVideo = substr($videosEntrLink->content, $strVideo + 18, 11);
        $linkVideo = "https://www.youtube.com/embed/{$idVideo}";?>

        <a id="linkvideo" href="<?=$linkVideo;?>">
            <div class="img">
                <img class="img-max" src="<?= image($videosEntrLink->cover, 300, 200); ?>" alt="">
            </div>
            <i class="fa fa-play fa-3x"></i>
        </a>

    <?php else:
        echo 'Não existe vídeo';
    endif;
    ?>
</div>

<div class="box-news aniversario">
    <h1 class="title">Destaques</h1>
    <?php
        if($highliths):
            foreach($highliths as $hl):?>
                <article>
                    <a href="<?=url("/destaques/{$hl->uri}");?>">
                        <div class="img">
                            <img class="img-fluid" src="<?=image($hl->cover,75,75);?>" alt="<?=$hl->title_highlight;?>">
                        </div>
                        <div class="titulo">
                            <h1><?=$hl->title;?></h1>
                        </div>
                    </a>
                </article>
    <?php
            endforeach;
        endif;
    ?>
    
</div>

<div class="box-news">
    <h1 class="title cor-portal"><a href="<?=url();?>"><img src="<?=theme("/img/main_header_home_logo.png");?>">Últimas do Portal SBN</a></h1>
    <?php
        if($postsRecentes):
            foreach($postsRecentes as $pr):
                $links = ($pr->category()->id == 10 || $pr->category()->id == 36 || $pr->category()->parent == 10)? 'artigos_entretenimento' : 'artigo';
    ?>
                <article>
                    <a href="<?=url("/{$links}/{$pr->uri}");?>" title="<?=$pr->title;?>" target="_blank">
                        <div class="img">
                            <img class="img-fluid" src="<?=image($pr->cover,110,63);?>" alt="<?=$pr->title;?>">
                        </div>
                        <div class="titulo">
                            <h1><?=str_limit_words($pr->title,10);?></h1>
                        </div>
                    </a>
                </article>
    <?php
            endforeach;
        endif;
    ?>
    <!-- <div class="seemore">
        <a href="<?=url("/artigos-entretenimento");?>">Mais posts recentes</a>
    </div> -->
</div>

<div class="banner">
    <?= bannerAds(1,300,250); ?>
</div>

<div class="box-news">
    <h1 class="title">Posts mais visitados no mês <?=date("m/Y");?></h1>
    
    <?php 
        if($maisVisitados):
            foreach($maisVisitados as $mv):
    ?>
                <article>
                    <a href="<?=url("/artigos-entretenimento/{$mv->uri}");?>" title="<?=$mv->title;?>">
                        <div class="img">
                            <img class="img-fluid" src="<?=image($mv->cover,110,63);?>" alt="<?=$mv->title;?>">
                        </div>
                        <div class="titulo">
                            <h1><?=str_limit_words($mv->title,10);?></h1>
                        </div>
                    </a>
                </article>
    <?php
            endforeach;
        else:
            echo "Nenhum post para este mês \"".date('m/Y')."\"";
        endif;
    ?>
</div>


<div class="box-news">
    <h1 class="title">Siga-nos no Fecebook</h1>
    <div class="fb-page" data-href="https://www.facebook.com/PortalSBN/" data-tabs="timeline" data-width="357" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/PortalSBN/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/PortalSBN/">Portal Sbn</a></blockquote></div>
</div>