<?php $v->layout('ent/_theme-ent');?>

<div class="content-list">
    <header>
        <h1>Eventos</h1>
        <hr>
    </header>
    
    <?php
    if ($list_agenda):
        ?>
        <section>
            <?php
            foreach ($list_agenda as $agenda):
                $v->insert("ent/list_agenda", ["agenda" => $agenda]);
            endforeach;
            ?>
            <div class="clear"></div>
        </section><!-- Outras Notícias -->
        <?php
    else:
        ?>
        <div style="padding: 50px 0 150px 0 !important; color: #777;width:100%">
            <header style="border-bottom:none; text-align: center; margin-bottom: 0;">
                <h1 style="font-weight:300; font-size: 1.8em; color: #666;"><i class="fa fa-frown-o"></i> Nenhuma Agenda encontrada!</h1>
            </header>
            <div class="clear"></div>
            <div style="text-align: center;">
                <p>No momento não existem agendas de eventos. Por favor, volte mais tarde.</p>
            </div>
        </div>
        <?php
    endif;

    /* Cria a paginação de resultados */
    //$Pager->FullPaginator("SELECT * FROM pc_posts WHERE post_type = :type AND post_status = 1 AND post_entertaiment = 1 AND post_trash = 0 AND post_estate LIKE '%$estate%' ORDER BY post_date DESC", "type=gallery");
    /* Printa a paginação */
    echo $paginator;
    ?>
    <div class="clear"></div>
</div>
