<?php $v->layout('ent/_theme-ent');?>

<div class="box-left">
    <div class="banner">
        <?= bannerAds(1,728,90); ?>
    </div>

    <div class="slide">
        <div class="slider">
            <?php if($slideNoticias):
            foreach($slideNoticias as $sNews):?>
            <div class="conteudo">
                <a href="<?=url("/artigos-entretenimento/{$sNews->uri}");?>">
                    <div class="img">
                        <img class="img-fluid" src="<?= image($sNews->cover, 344, 197); ?>" alt="<?=$sNews->title;?>">
                    </div>
                    <div class="titulo">
                        <h1><?=$sNews->title;?></h1>
                        <p><span style="background-color:<?=$cor;?>"><?=$sNews->category()->title;?></span> <span><?=$sNews->tag;?></span></p>
                    </div>
                </a>
            </div>
            <?php
            endforeach;
             endif;?>
        </div>
        <div class="progressbar">
            <div class="progress"></div>
        </div>
        <div class="indicador"></div>
    </div>

    <div class="category-title">
        <!-- galeria de imagens -->
        <h1>Galeria em destaque</h1>
        <small><a href="<?=url("/coberturas");?>">Veja todas as coberturas</a></small>
    </div>
    <?php if($galleryDestaque):
        foreach($galleryDestaque as $galDestaque):?>
    <article>
        <div class="conteudo">
            <a href="<?=url("fotos/{$galDestaque->uri}");?>" title="<?=$galDestaque->title;?>">
                <div class="img">
                    <img class="img-fluid" src="<?= image($galDestaque->cover, 344, 197); ?>" alt="<?=$galDestaque->title;?>">
                </div>
                <div class="titulo">
                    <h1><?=$galDestaque->title;?></h1>
                    <p><span style="background-color:<?=$cor;?>"><?=$galDestaque->local;?></span> <span><?=$galDestaque->tag;?></span></p>
                </div>
            </a>
        </div>
        <hr>
    </article>
    <?php endforeach;
    endif;?>

    <div class="category-title">
        <!-- notícias relacionadas a entretenimento -->
        <h1>Notícias em destaque</h1>
        <small><a href="<?=url("/artigos-entretenimento");?>">Veja todos os posts</a></small>
    </div>
    <?php if($outrasNoticias):
        foreach($outrasNoticias as $news):?>
    <article>
        <div class="conteudo">
            <a href="<?=url("/artigos-entretenimento/{$news->uri}");?>" title="<?=$news->title;?>">
                <div class="img">
                    <img class="img-fluid" src="<?= image($news->cover, 344, 197); ?>" alt="<?=$news->title;?>">
                </div>
                <div class="titulo">
                    <h1><?=$news->title;?></h1>
                    <p><span style="background-color:<?=$cor;?>"><?=$news->category()->title;?></span> <span><?=$news->tag;?></span></p>
                </div>
            </a>
        </div>
        <hr>
    </article>
    <?php endforeach;
        endif;?>

    <div class="category-title">
        <h1>Vídeos em destaque</h1>
        <small><a href="<?=url("/video-entretenimento");?>">Veja todos os posts</a></small>
    </div>
    <?php if($videosEntr):
    foreach($videosEntr as $video):?>
    <article>
        <div class="conteudo">
            <a href="<?=url("/artigos-entretenimento/{$video->uri}");?>" title="<?=$video->title;?>">
                <div class="img">
                    <img class="img-fluid" src="<?= image($video->cover, 344, 197); ?>" alt="<?=$video->title;?>">
                </div>
                <div class="titulo">
                    <h1><?=$video->title;?></h1>
                    <p><span style="background-color:<?=$cor;?>"><?=$video->category()->title;?></span> <span><?=$video->tag;?></span></p>
                </div>
            </a>
        </div>
        <hr>
    </article>
    <?php endforeach;
    endif;
    ?>

</div>

<div class="box-right">
    <?php require(__DIR__."/aside.php"); ?>
</div>