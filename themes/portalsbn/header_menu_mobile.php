
<?php
    //função para dexar o menu selecionado
    if(isset($idd)){
        $mm = (new Source\Models\Category())
    ->find('id = :p', "p={$idd}", "parent")
    ->fetch()->parent;
    }
    $categoryId = ($mm ?? '')? $mm : $idd ?? '';
    $activeHome = function ($value) use ($categoryId) {
        return ($categoryId == $value ? "active" : "");
    };

    $url = ($menu->uri == 'entretenimento')? url("/{$menu->uri}") : url("/artigo/lista/{$menu->uri}") ;
?>

<li class="main_header_nav_item <?=$activeHome($menu->id);?>"><a href="<?=$url;?>"><?=$menu->title;?></a>
    <ul class="main_header_nav_sub">
        <?php
        $menuCat = (new Source\Models\Category())->find("type = :t && parent = :id","t=post&id={$menu->id}")->fetch(true);
        /* Exibe menu de sub-categorias se existir */
        if ($menuCat): ?>
            <?php foreach ($menuCat as $Cat): ?>
                <li class="main_header_nav_sub_item"><a href="<?=url("/artigo/em/{$Cat->uri}");?>" title=""><?= $Cat->title; ?></a></li>
            <?php endforeach;?>
        <?php endif;?>
    </ul>
</li>