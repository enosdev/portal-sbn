<?php

$cor_slide = (new Source\Models\Category())->findByUri("entretenimento")->color;

if($mod == 'mobile'):?>

<article class="main_box_news main_box_gray slide_mobile_item margin-top-0 slide_mobile_item_galerias<?= $first; ?>">
    <a href="<?=url("fotos/{$gal->uri}");?>" title="<?= $gal->title; ?>">
        <img class="img-fluid" src="<?= image($gal->cover, 520, 310); ?>" alt="<?=$gal->title;?>" title="<?=$gal->title;?>"/>
    </a>
    <div class="main_box_news_desc">
        <a href="<?=url("fotos/{$gal->uri}");?>" title="<?= $gal->title; ?>">
            <mark style="background-color:<?=$cor_slide;?>; " class="categoria"><?=$gal->local;?></mark>
            <p class="tagline"><?=$gal->tag;?></p>
            <time datetime="<?= date('Y-m-d H:i:s', strtotime($gal->post_at));?>"><?= date('d/m/Y', strtotime($gal->post_at));?></time>
            <div class="clear"></div>
            <h1><?= str_limit_chars($gal->title, 80); ?></h1>
        </a>
    </div>
</article><!-- Item mobile -->

<?php 
elseif($mod == 'normal'): ?>

<div class="main_slide_galeria_item slide_item_galerias<?= $first; ?>">
    <a href="<?=url("fotos/{$gal->uri}");?>" title="<?= $gal->title; ?>">
        <img class="img-fluid" src="<?= image($gal->cover, 840, 605); ?>" alt="<?=$gal->title;?>" title="<?=$gal->title;?>"/>
    </a>
    <div class="main_slide_galeria_item_desc">
        <a href="<?=url("fotos/{$gal->uri}");?>" title="<?= $gal->title; ?>">
            <mark style="background-color:<?=$cor_slide;?>; " class="categoria"><?=$gal->local;?></mark>
            <p class="tagline"><?=$gal->tag;?></p>
            <time datetime="<?= date('Y-m-d H:i:s', strtotime($gal->date_at));?>"><?= date('d/m/Y', strtotime($gal->date_at));?></time>
            <div class="clear"></div>
            <p class="title"><?=$gal->title;?></p>
        </a>
    </div>
</div>

<?php
elseif($mod == 'link'): ?>

<div class="main_slide_galeria_item_click slide_mark_galerias<?= $active; ?>" id="<?= ($i - 1); ?>">
    <mark style="background-color:<?= $cor_slide; ?>; " class="categoria"><?= $gal->local; ?></mark>
    <p class="tagline"><?= $gal->tag; ?></p>
    <div class="clear"></div>
    <p class="title"><?=str_limit_chars($gal->title, 80);?></p>
    <div style="bottom:0; background: #0071BC;" class="main_midia_icon">Galeria <i class="fa fa-picture-o"></i></div>
</div><!-- Item click -->

<?php
endif;