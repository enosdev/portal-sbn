<?php $v->layout("_theme"); ?>
<!-- modificação feita por Enos -->
<!-- CARREGA TODO CSS DA PARTE DE COMPARTILHAMENTO DE MATÉRIAS -->
<style>
    .right{float:right}
    .display{display:none}
    .article-header-info {display: block;border-top: 1px dotted #bfbfbf;padding-top: 20px;}
    .article-header-info .article-header-meta:after {display: block;content: '';clear: both;}
    .article-header-info .article-header-meta > span {display: block;float: left;color: #2a2b2c;margin-right: 10px;}
    .article-header-info .article-header-meta > span:last-child {margin-right: 0px;}
    .article-header-info .article-header-meta .article-header-meta-date {font-size: 30px;font-weight: bold;line-height: 100%;}
    .article-header-info .article-header-meta .article-header-meta-time .head-time {font-size: 12px;line-height: 100%;display: block;padding-top: 2px;}
    .article-header-info .article-header-meta .article-header-meta-time .head-year {font-size: 13px;line-height: 100%;font-weight: bold;display: block;padding-top: 1px;}
    .article-header-info .article-header-meta .article-header-meta-links {border-left: 1px dotted #bfbfbf;margin-left: 5px;padding-left: 14px;margin-top: -3px;}
    
    .article-header-meta-links.one-is-missing {padding-top: 8px!important;padding-bottom: 8px!important;}
    .article-header-meta .article-header-meta-links a {display: block;font-size: 12px;color: #3f484f;line-height: 150%;}
    .article-header-meta .article-header-meta-links a:hover {color: #e32e15;}
    .article-header-meta .article-header-meta-links a strong,
    .article-header-meta .article-header-meta-links a span {border-bottom: 1px dotted transparent;transition: border-bottom 0.2s;-webkit-transition: border-bottom 0.2s;-moz-transition: border-bottom 0.2s;}
    .article-header-meta .article-header-meta-links a:hover strong,
    .article-header-meta .article-header-meta-links a:hover span {border-bottom: 1px dotted #e32e15;}
    .article-header-meta .article-header-meta-links a i.fa {padding-right: 7px;}
    .social-headers {display: block;cursor: default;margin-top: -3px;margin-bottom: -3px;}
    .social-headers a {display: inline-block;height: 35px;width: 38px;text-align: center;line-height: 38px;background: transparent;color: #232323;border-bottom: 2px solid #232323;margin-left: 2px;font-size: 21px;}
    .social-headers a:hover {color: #fff!important;background: #232323;}
    .social-headers a.soc-whatsapp {color: #128c7e;border-bottom: 2px solid #128c7e;}
    .social-headers a.soc-whatsapp:hover {background: #128c7e;}
    .social-headers a.soc-facebook {color: #3b5998;border-bottom: 2px solid #3b5998;}
    .social-headers a.soc-facebook:hover {background: #3b5998;}
    .social-headers a.soc-twitter {color: #00aced;border-bottom: 2px solid #00aced;}
    .social-headers a.soc-twitter:hover {background: #00aced;}
    .social-headers a.soc-linkedin {color: #0e76a8;border-bottom: 2px solid #0e76a8;}
    .social-headers a.soc-linkedin:hover {background: #0e76a8;}

    /*480PX BREAKPOINT*/
    @media(max-width: 30em){
        .right{float:left}
        .display{display:block}
        .article-header-info {padding-top: 10px;}
        .social-headers{margin-bottom:5px}
        .article-header-meta-links.one-is-missing{display:none!important}
    }
</style>
<main class="main_content container">
    <div class="content">

        <div class="main_content_left container">

            <!-- funciona no mobile -->
            <aside class="main_banner_728 banner_top_content">
                <!-- <img src="< ?= theme('/img/banner_728_90.png');?>" alt=""> -->
                <?= bannerAds(1,728,90); ?>
            </aside>

            <article class="main_single_content" style="border-top-color:<?= $cor; ?>;">
                <?php
                    if($post->type == 'column' && $post->author()->banner != NULL):?>
                        <img class="img-fluid" style="margin-bottom:15px" src="<?=image($post->author()->banner, 980);?>" alt="<?= $post->author()->fullName(); ?>" title="<?= $post->title; ?>"/>
                <?php
                    endif;
                ?>
                <header>
                    <h1><?= $post->title; ?></h1>
                    <?php
                    //SE FOR VÍDEO NÃO EXIBE A IMAGEM DE CAPA
                    if ($post->category()->parent == NULL && $post->category()->id != 9 || $post->category()->parent != NULL && $post->category()->parent != 9 && $post->category()->id != 9):
                        ?>
                                                
                        <?php if ($post->video):
                            $url = $post->video;
                            
                            if(strpos($url, 'youtu.be/') !== false){
                                $exp = explode('/', $url);
                                $code_video = end($exp);
                            }elseif(strpos($url, 'youtube.com/embed/') !== false){
                                $exp = explode('/', $url);
                                $code_video = end($exp);
                            } else {
                                $itens = parse_url ($url);
                                parse_str($itens['query'], $params);
                                $code_video = $params['v'];
                            }

                            
                        ?>
                        <style>
                            .box-video {width: 100%;padding-top: 56.25%;position: relative;}
                            .box-video iframe {width: 100%;height: 100%;position: absolute;top: 0;left: 0;}
                        </style>

                        <div class="box-video">
                        <iframe src="https://www.youtube.com/embed/<?=$code_video;?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <?php else: ?>
                            <img src="<?=image($post->cover, 960);?>" alt="<?= $post->title; ?>" title="<?= $post->title; ?>"/>
                        <?php endif;?>
                        
                        <div class="main_single_content_header_share">                            
                            <div class="article-header-info">
                                <div class="right social-headers">
                                    <a target="_blank" href="https://api.whatsapp.com/send?text=Veja esta matéria: <?= url("/artigo/{$post->uri}"); ?> - É notícia? Tá no <?=CONF_SITE_NAME;?> | <?=CONF_SITE_TITLE;?>!" class="soc-whatsapp ot-share"><i class="fa fa-whatsapp"></i></a>
                                    <a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=<?= url("/artigo/{$post->uri}"); ?>" data-url="<?= url("/artigo/{$post->uri}"); ?>" class="soc-facebook ot-share"><i class="fa fa-facebook"></i></a>
                                    <a target="_blank" href="https://twitter.com/intent/tweet?original_referer=<?= url("/artigo/{$post->uri}"); ?>&amp;source=tweetbutton&amp;text=<?= $post->title;?>&amp;url=<?= url("/artigo/{$post->uri}"); ?>&amp;via=" data-hashtags="" data-url="<?= url("/artigo/{$post->uri}"); ?>" data-via="" data-text="<?= $post->title;?>" class="soc-twitter ot-tweet" target="_blank"><i class="fa fa-twitter"></i></a>
                                    <a target="_blank" title="Linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= url("/{$linkEntHome}/{$post->uri}") ?>&title=<?= $post->title ?>&summary=<?= $post->subtitle ?>&source=<?= CONF_SITE_NAME ?>" class="soc-linkedin ot-share"><i class="fa fa-linkedin"></i></a>
                                </div>
                                <div class="clear display"></div>
                                <span class="article-header-meta">
                                <?php
                                $mes = date('m', strtotime($post->post_at));
                                switch ($mes){
 
                                    case 1: $mes = "janeiro"; break;
                                    case 2: $mes = "fevereiro"; break;
                                    case 3: $mes = "março"; break;
                                    case 4: $mes = "abril"; break;
                                    case 5: $mes = "maio"; break;
                                    case 6: $mes = "junho"; break;
                                    case 7: $mes = "julho"; break;
                                    case 8: $mes = "agosto"; break;
                                    case 9: $mes = "setembro"; break;
                                    case 10: $mes = "outubro"; break;
                                    case 11: $mes = "novembro"; break;
                                    case 12: $mes = "dezembro"; break;
                                     
                                    }
                                ?>
                                    <span class="article-header-meta-date"><?= date('d', strtotime($post->post_at)); ?> <?= $mes;?></span>
                                    <span class="article-header-meta-time">
                                        <span class="head-time"><?= date('H:i', strtotime($post->post_at)); ?></span>
                                        <span class="head-year"><?= date('Y', strtotime($post->post_at)); ?></span>
                                    </span>
                                    <span class="article-header-meta-links one-is-missing">
                                        <a href="#">
                                            <i class="fa fa-print"></i><span>Imprimir notícia</span>
                                        </a>
                                    </span>
                                </span>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <?php
                    endif;
                    ?>
                </header>
                <div class="main_single_tag">
                    <mark style="background-color:<?= $post->category()->color; ?>;" class="categoria"><?= $post->category()->title; ?></mark>
                    <p class="posttag"><?= $post->tag; ?></p>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="htmlchars" style="word-wrap: break-word;">
                <!-- <div class="htmlchars" style="word-wrap: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; "> -->
                <!-- <div class="htmlchars"> -->
                    <?php
                    $texto = html_entity_decode($post->content);

                    if($galleryId):
                        $pastinha = explode('.',$galleryId->cover);
                        $past = explode('/',$pastinha[0]);
                        $past = array_pop($past);
                        $folder = str_replace($past,'',$pastinha[0]);

                        $arch = glob(getcwd().'/'.CONF_UPLOAD_DIR.'/'.$folder.$galleryId->uri.'/*.*');
                        $countArch = count($arch);


                        $idGallery = "[[$post->gallery]]";
                        $urlImage = url("/".CONF_UPLOAD_DIR."/{$galleryId->cover}");
                        $showGallery = "<div class=\"gallery\">
                                        <a data-fancybox=\"foto\" data-caption=\"{$galleryId->title}\" href=\"{$urlImage}\" title=\"{$galleryId->title}\">
                                            <img src=\"{$urlImage}\" alt=\"{$galleryId->title}\" title=\"{$galleryId->title}\"/>
                                            <div class=\"gallery_controls\">
                                                <i class=\"fa fa-chevron-left back\"></i>
                                                <i class=\"fa fa-chevron-right go\"></i>
                                            </div>
                                        </a>
                                    </div><!-- Gallery -->
                                    <p class=\"float-left gallery_text\"><b>{$galleryId->title}</b></p>
                                    <p class=\"float-right gallery_text\">1/\"{$countArch}\" <i class=\"fa fa-picture-o\"></i></p><div class=\"clear\"></div>
                        ";
                        
                        foreach($arch as $image):
                            $image = url().str_replace(getcwd(),'',$image);
                            $showGallery .= "<a class=\"display-none\" data-fancybox=\"foto\" data-caption=\"{$galleryId->title}\" href=\"{$image}\" title=\"{$galleryId->title}\"></a>";
                        endforeach;
                        $texto = str_replace($idGallery,$showGallery, $texto);
                    endif;
  
                    if($post->seemore != NULL){
                        $vejaMais = str_replace(',','', $post->seemore);
                        $mudar = '<div class="veja-tambem">
                            <h4>Veja<br>Também</h4>
                            <div class="list-titles">
                                '.$vejaMais.'
                            </div>
                        </div>';
                        $texto = str_replace('[[vejamais]]', $mudar, $texto);
                    }

                    //carrega o texto nornalmente
                    echo $texto;
                    
                ?>
                </div>

                <?php
                //ENQUETE VINCULADA
                if (!is_null($post->poll)):
                    $v->insert("enquete", ["enquete" => $poll]);
                endif;?>

                <!-- Composite Start -->
                <div id="M701973ScriptRootC1116774"></div>
                <script src="https://jsc.mgid.com/p/o/portalsbn.com.1116774.js" async></script>
                <!-- Composite End -->

            </article><!-- Content -->

            <div class="main_single_content_share_bottom">
                <!-- <ul class="sharebox">
                    <li class="like facebook radius"><a href="< ?= HOME . '/' . SINGLEPOST . '/' . $post_name; ?>&app_id=< ?= FACEBOOK_APP; ?>" title="Compartilhe no Facebook"><i class="fa fa-facebook"></i> <span class="count">0</span></a></li>
                    <li class="like google radius"><a href="< ?= HOME . '/' . SINGLEPOST . '/' . $post_name; ?>" title="Recomende no Google+"><i class="fa fa-google-plus"></i> <span class="count">0</span></a></li>
                    <li class="like twitter radius"><a href="< ?= HOME . '/' . SINGLEPOST . '/' . $post_name; ?>" rel="&text=< ?= $post_title; ?>" title="Conte isto no Twitter"><i class="fa fa-twitter"></i></a></li>
                </ul>
                <p>Compartilhe esse post</p>  -->

                <div style="background:#fff;padding:5px 10px 15px 10px">
                    <div class="right social-headers">
                        <a target="_blank" href="https://api.whatsapp.com/send?text=Veja esta matéria: <?= url("/artigo/{$post->uri}"); ?> - É notícia? Tá no <?=CONF_SITE_NAME;?> | <?=CONF_SITE_TITLE;?>!" class="soc-whatsapp ot-share"><i class="fa fa-whatsapp"></i></a>
                        <a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=<?= url("/artigo/{$post->uri}"); ?>" data-url="<?= url("/artigo/{$post->uri}"); ?>" class="soc-facebook ot-share"><i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="https://twitter.com/intent/tweet?original_referer=<?= url("/artigo/{$post->uri}"); ?>&amp;source=tweetbutton&amp;text=<?= $post->title;?>&amp;url=<?= url("/artigo/{$post->uri}"); ?>&amp;via=" data-hashtags="" data-url="<?= url("/artigo/{$post->uri}"); ?>" data-via="" data-text="<?= $post->title;?>" class="soc-twitter ot-tweet" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" title="Linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= url("/{$linkEntHome}/{$post->uri}") ?>&title=<?= $post->title ?>&summary=<?= $post->subtitle ?>&source=<?= CONF_SITE_NAME ?>" class="soc-linkedin ot-share"><i class="fa fa-linkedin"></i></a>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div><!-- Share Bottom -->

            <aside class="main_banner margin-bottom-20 margin-top-10">
                <h1 class="font-zero">Publicidade</h1>
                <div class="banner_728 banner" style="width: 100%; max-width:728px;"><?= bannerAds(2,728,90); ?></div>
                <div class="banner_220 banner"><?= bannerAds(1,220,90); ?></div>
            </aside><!-- banner -->

            <article class="main_single_comments">
                <header>
                    <h1>Deixe seu comentário</h1>
                </header>
                <div class="fb-comments" width="100%" data-href="<?=url("/artigo/{$post->uri}");?>" data-numposts="5"></div>
            </article><!-- Comments -->

            <div class="main_sidebar_top container">
                <?php require (__DIR__ . '/inc/sidebar_top_single.php'); ?>
            </div><!-- SIDEBAR TOP -->

            <?php
            // $readRelacionados = new Read("relacionadas-noticia-" . $post_id . "-" . $estate, "111 minutes", "cache");
            // $readRelacionados->FullRead("SELECT pc_posts.*,pc_categories.* FROM pc_posts_categories "
            //         . "INNER JOIN pc_posts ON pc_posts.post_id = pc_posts_categories.post_id "
            //         . "INNER JOIN pc_categories ON pc_categories.category_id = pc_posts_categories.category_id "
            //         . "WHERE pc_categories.category_id = :category AND pc_posts.post_id != :postid AND pc_posts.post_type = :type AND pc_posts.post_status = 1 AND pc_posts.post_trash = 0 AND pc_posts.post_estate LIKE '%$estate%' ORDER BY pc_posts.post_date DESC LIMIT :limit OFFSET :offset", "postid={$post_id}&category={$category_id}&type=post&limit=9&offset=0");
            // if ($related):
                ?>
                <section class="main_single_related j_more">
                    <h1>Leia também</h1>
                    <?php
                    if($related):
                        foreach($related as $outer):
                            $v->insert("article_news", ["news" => $outer]);
                        endforeach;
                    endif;
                    ?>
                </section>
                <?php
            // endif;
            ?>

            <div class="container align-center">
                <a class="btn btn-blue text-uppercase margin-top-10 margin-bottom-15 j_load_more" id="artigo-post" data-category="<?=$post->category;?>">Carregar Mais</a>
                <img class="form-load margin-right-15 j_more_load" style="display:none; padding: 15px;" alt="Enviando Requisição!" title="Enviando Requisição!" src="<?=theme("/img/load.gif");?>"/>
            </div>

            <div class="clear"></div>

            <div class="main_sidebar_bottom container">
                <?php require (__DIR__ . '/inc/sidebar_bottom_single.php'); ?>    
            </div><!-- SIDEBAR BOTTOM -->

            <div class="main_sidebar_top container">
                <?php //require (__DIR__ . '/inc/sidebar_top_single.php'); ?>
            </div><!-- SIDEBAR TOP -->
        </div><!-- CONTENT LEFT -->

        <div class="main_content_right container">
            <div class="main_sidebar container">
                <?php require (__DIR__ . '/inc/sidebar_top_single.php'); ?>
                <?php require (__DIR__ . '/inc/sidebar_bottom_single.php'); ?> 
            </div>
        </div><!-- CONTENT RIGHT -->

        <div class="clear"></div>
    </div>
</main>