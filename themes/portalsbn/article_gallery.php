<?php $icon = "<div style=\"top:-17px; height:18px; background: #0071BC;\" class=\"main_midia_icon\">Galeria <i class=\"fa fa-picture-o\"></i></div>";?>
<article class="main_box_news main_box_white item">
    <a href="<?= url("/fotos/{$gal->uri}");?>" title="<?=$gal->title;?>">
        <img class="img-fluid" src="<?= image($gal->cover, 480,240); ?>" alt="<?=$gal->title;?>" title="<?=$gal->title;?>"/>
    </a>

    <div class="main_box_news_desc">
        <ul class="social">
            <li class="social_item"><a href="https://api.whatsapp.com/send?text=Veja esta matéria:<?= url("/fotos/{$gal->uri}"); ?>" title="Compartilhe WhatsApp" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="whatsapp" rel="nofollow"><i class="fa fa-whatsapp"></i></a></li>
            <li class="social_item"><a href="http://www.facebook.com/sharer.php?u=<?= url("/fotos/{$gal->uri}"); ?>" title="Compartilhe no Facebook" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="facebook" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
            <li class="social_item"><a href="https://twitter.com/intent/tweet?url=<?= url("/fotos/{$gal->uri}"); ?>&text=<?=$gal->title;?>" title="Conte isto no Twitter" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="twitter" rel="nofollow"><i class="fa fa-twitter"></i></a></li>
        </ul>
        
        <a href="<?=url("/fotos/{$gal->uri}");?>" title="<?=$gal->title;?>">
            <?=$icon;?>
            <mark style="background-color:<?= $cor ?>;" class="categoria"><?=$gal->local;?></mark>
            <p class="tagline"><?=$gal->tag;?></p>
            <time datetime="<?= $gal->event_date;?>"><?= date('d/m/Y', strtotime($gal->event_date));?></time>
            <div class="clear"></div>
            <h1><?=str_limit_chars($gal->title, 70);?></h1>
        </a>
    </div>
</article>