
<?php
    $url = ($menu->uri == 'entretenimento')? url("/{$menu->uri}") : url("/fotos/lista/{$menu->uri}") ;
?>

<li class="main_header_nav_desktop_item"><a href="<?=$url;?>"><?=$menu->title;?></a>
    <ul class="main_header_nav_desktop_sub">
        <div class="content">
            <?php
            $menuCat = (new Source\Models\Category())->find("type = :t && parent = :id","t=post&id={$menu->id}")->fetch(true);
            $contaSub = ($menuCat)? 3 : 4;
            $header = (new Source\Models\Gallery())
                ->findGallery("entertainment = :e", "e=yes")
                ->order("date_at DESC")
                ->limit($contaSub)->fetch(true);
            foreach($header as $nm):
               $icon = ( strpos($nm->content, "youtube.com/embed/") ? "<div style=\"top:-17px; bottom:auto;\" class=\"main_midia_icon\">Vídeo <i class=\"fa fa-youtube-play\"></i></div>" : (strpos($nm->content, "@#[") ? "<div style=\"top:-17px; bottom:auto;\" class=\"main_midia_icon\">Galeria <i class=\"fa fa-picture-o\"></i></div>" : "") );
            ?>
                <article class="main_box_news main_box_white item">
                    <a href="<?= url("/fotos/{$nm->uri}");?>" title="<?=$nm->title;?>">
                        <img src="<?= image($nm->cover, 480,240); ?>" alt="<?=$nm->title;?>" title="<?=$nm->title;?>"/>
                    </a>
                    <div class="main_box_news_desc" style="min-height: auto;">
                        <ul class="social">
                            <li class="social_item"><a href="https://api.whatsapp.com/send?text=Veja esta matéria:<?= url("/fotos/{$nm->uri}");?>" title="Compartilhe WhatsApp" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="whatsapp" rel="nofollow"><i class="fa fa-whatsapp"></i></a></li>
                            <li class="social_item"><a href="http://www.facebook.com/sharer.php?u=<?= url("/fotos/{$nm->uri}");?>" title="Compartilhe no Facebook" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="facebook" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
                            <li class="social_item"><a href="https://twitter.com/intent/tweet?url=<?= url("/fotos/{$nm->uri}");?>&text=<?=$nm->title;?>" title="Conte isto no Twitter" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="twitter" rel="nofollow"><i class="fa fa-twitter"></i></a></li>
                        </ul><!-- social share -->

                        <a href="<?= url("/fotos/{$nm->uri}");?>" title="<?=$nm->title;?>">
                            <div style="top:-17px; bottom:auto; background: #0071BC;" class="main_midia_icon">Galeria <i class="fa fa-picture-o"></i></div>
                            <mark style="background-color:<?= $menu->color;?>" class="categoria"><?= $nm->local;?></mark>
                            <p class="tagline" style="color:#666;"><?=$nm->tag;?></p>
                            <time datetime="<?= date('Y-m-d H:i:s', strtotime($nm->event_date));?>" style="margin-top: 4px;"><?= date('d/m/Y', strtotime($nm->event_date));?></time>
                            <div class="clear"></div>

                            <h1><?=str_limit_chars($nm->title, 70);?></h1>
                        </a>
                    </div>
                </article>
            <?php endforeach;

            /* Exibe menu de sub-categorias se existir */
            if ($menuCat): ?>
                <div class="main_header_nav_desktop_sub_list">
                    <p class="title">Sub-categorias</p>
                    <?php foreach ($menuCat as $Cat):
                        ?>
                        <li class="main_header_nav_desktop_sub_item"><a href="<?=url("/fotos/em/{$Cat->uri}");?>" title=""><?= $Cat->title; ?></a></li>
                        <?php
                    endforeach;?>
               </div>
            <?php endif;?>
        </div>
    </ul>
</li>