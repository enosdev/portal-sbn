jQuery(function ($) {
	var semana = window.innerWidth;
	if (semana > 500) {
		var dayName = [
			'Domingo',
			'Segunda-Feira',
			'Terça-Feira',
			'Quarta-Feira',
			'Quinta-Feira',
			'Sexta-Feira',
			'Sábado'
		];
		var monName = [
			'Janeiro',
			'Fevereiro',
			'Março',
			'Abril',
			'Maio',
			'Junho',
			'Julho',
			'Agosto',
			'Setembro',
			'Outubro',
			'Novembro',
			'Dezembro'
		];
	} else {
		var dayName = [
			'Dom',
			'Seg',
			'Ter',
			'Qua',
			'Qui',
			'Sex',
			'Sáb'
		];
		var monName = [
			'Janeiro',
			'Fevereiro',
			'Março',
			'Abril',
			'Maio',
			'Junho',
			'Julho',
			'Agosto',
			'Setembro',
			'Outubro',
			'Novembro',
			'Dezembro'
		];
	}

	var now = new Date();
	var data =
		'<strong>' +
		dayName[now.getDay()] +
		'</strong>, ' +
		now.getDate() +
		' de ' +
		monName[now.getMonth()] +
		' de ' +
		now.getFullYear() +
		',';

	$('#dia').html(data);

	setInterval(passa, 1000);
	function passa() {
		var today = new Date();
		var h = today.getHours();
		var m = today.getMinutes() < 10 ? '0' + today.getMinutes() : today.getMinutes();
		var s = today.getSeconds() < 10 ? '0' + today.getSeconds() : today.getSeconds();
		var hora = h + ':' + m + ':' + s;
		$('#hora').html(hora);
	}
});
