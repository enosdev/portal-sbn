
let linkvideo = document.querySelector("#linkvideo")

if (linkvideo != null) {
    linkvideo.addEventListener('click', (e) => {
        e.preventDefault()
        let div = document.createElement('div')
        let body = document.querySelector('body')
        div.classList.add('mymodal')
        body.prepend(div)

        let pega = document.querySelector('.mymodal')
        setTimeout(() => {
            pega.classList.add('abrirModal');
        }, 300)

        let video = `<div class="video-play"><div class="close">X</div><iframe src="${linkvideo.href}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>`

        //inserindo o vídeo dentro do modal
        pega.innerHTML = video

        let close = document.querySelector('.close')
        close.addEventListener('click', () => {
            pega.classList.remove('abrirModal')
            setTimeout(() => {
                pega.remove()
            }, 300)
        })

    })
}