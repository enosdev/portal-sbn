<section class="main_podcast">
    <header>
        <i class="fa fa-headphones"></i> 
        <h1>Podcast</h1>
        <div class="clear"></div>
    </header>
    <article class="main_podcast_item">
        <h1>Greve dos profissionais da saúde é uma palhaçada; diz prefeito Temóteo</h1>
        <audio id="audio" preload="auto" tabindex="0" controls="" type="audio/mpeg" style="width: 100%">
            <source type="audio/mp3" src="https://portalsbn.com.br/uploads/podcast/2019/02/4.mp3">
            Desculpe, esse navegador nao da suporte ao HTML5.
        </audio>
    </article>

    <button title="Veja mais" class="btn text-uppercase j_podcast_action" style="width: 100%; background: #666;">Ouça mais</button>

    <div class="main_podcast_audios j_podcast">
        <article style="display: none;" class="main_podcast_item">
            <h1>Greve dos profissionais da saúde é uma palhaçada; diz prefeito Temóteo</h1>
            <audio id="audio" preload="auto" tabindex="0" controls="" type="audio/mpeg" style="width: 100%">
                <source type="audio/mp3" src="https://portalsbn.com.br/uploads/podcast/2019/02/4.mp3">
                Desculpe, esse navegador nao da suporte ao HTML5.
            </audio>
        </article>
        <article class="main_podcast_item">
            <h1>STF decide restringir foro privilegiado de deputados e senadores</h1>
            <audio id="audio" preload="auto" tabindex="0" controls="" type="audio/mpeg" style="width: 100%">
                <source type="audio/mp3" src="https://portalsbn.com.br/uploads/podcast/2018/05/7.mp3">
                Desculpe, esse navegador nao da suporte ao HTML5.
            </audio>
        </article>
        <div class="clear"></div>
    </div><!-- Mais áudios -->      
</section><!-- PODCAST -->

<?php if($timer):?>
<article class="main_contagem_regressiva">
    <h1><span class="font-zero">Contagem Regressiva</span><?=$timer->title?></h1>
    <img src="<?= image($timer->cover,400);?>" title="Contagem Regressiva PREFEITOS E VEREADORES" alt="Contagem Regressiva PREFEITOS E VEREADORES"> <p class="tagline j_clock" datetime="<?=date("Y/m/d H:i:s", strtotime($timer->event_at));?>">Restam 342 dias 21:38:03</p>
    <p class="desc"><?=$timer->description;?></p>
</article><!-- CONTAGEM REGRESSIVA -->
<?php endif;?>

<section class="main_ultimas container">
    <header>
        <h1>Últimas <span class="font-zero">Notícias</span></h1>
        <ul>
            <li class="main_ultimas_nav main_ultimas_nav_ba ">Bahia</li>
            <li class="main_ultimas_nav main_ultimas_nav_es active">Espírito Santo</li>
        </ul>
    </header>
    <section class="main_ultimas_content main_ultimas_ba container ">
        <h1 class="font-zero">Bahia</h1>
        <?php if($ultimasEstado):
            foreach($ultimasEstado as $us):?>
            <article class="main_ultimas_item">
                <a href="<?=url("/artigo/{$us->uri}");?>" title="<?=$us->title;?>">
                    <img src="<?=image($us->cover, 520, 250);?>" title="<?=$us->title;?>" alt="<?=$us->title;?>">
                    <h1><?=$us->title;?></h1>
                </a>
            </article>
        <?php endforeach;
        endif;?>
        <div class="clear"></div>
    </section><!-- bahia -->

    <section class="main_ultimas_content main_ultimas_es container first">
        <h1 class="font-zero">Espírito Santo</h1>
        <?php
            $es = file_get_contents("https://www.portalsbn.com.br/espiritosanto/520/250");
            $api_es = json_decode($es);
            foreach($api_es as $value):
        ?>
            <article class="main_ultimas_item">
                <a href="<?=$value->uri;?>" title="<?=$value->title;?>">
                    <img src="<?=$value->cover;?>" title="<?=$value->title;?>" alt="<?=$value->title;?>">
                    <h1><?=str_limit_words($value->title,12);?></h1>
                </a>
            </article>
        <?php endforeach;?>
        
        <div class="clear"></div>
    </section><!-- espírito santo -->
    <div class="clear"></div>
</section><!-- ÚLTIMAS BA ES -->

<!-- <aside class="main_banner_300 margin-bottom-20"> -->
    <!-- <h1 class="font-zero">Publicidade</h1> -->
    <!-- <img src="< ?= theme('/img/banner_300_250.png');?>" alt=""> -->
    <!-- < ?= bannerAds(2,300,250); ?> -->
<!-- </aside>  -->
<!-- banner 300 -->