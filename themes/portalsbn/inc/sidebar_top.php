<section class="main--magazine">
    <div class="title--magazine">Portal SBN Destaca</div>
    <div class="scroll">
        <?php 
        if($highliths):
            foreach($highliths as $hl):?>
                <div class="list">
                    <a href="<?=url("destaques/{$hl->uri}");?>">
                        <img class="img-fluid" src="<?=image($hl->cover,320,150);?>" alt="<?=$hl->title_highlight;?>">
                        <h2><?=$hl->title;?></h2>
                    </a>
                </div>
        <?php 
            endforeach;
        endif;?>
    </div>
</section>

<aside class="main_banner_300 margin-bottom-20 banner">
    <h1 class="font-zero">Publicidade</h1>
    <!-- <img src="< ?= theme('/img/banner_300_250.png');?>" alt=""> -->
    <?= bannerAds(1,300,250); ?>
</aside><!-- banner 300 -->

<?php
if ($list_agenda):
    ?>
    <section class="main_colunistas">
        <h1 class="margin-bottom-5" style="text-align:center"><a href="<?=url("/agenda");?>" title="Veja todas as agendas" style="color:#ff8000"><strong>Próximos Eventos</strong></a></h1>
        <div class="box_evento">
            <?php
                foreach($list_agenda as $agenda):
                    $v->insert("article_sidebar_agenda", ["agenda" => $agenda]);
                endforeach;
            ?>
        </div>
        <div class="clear"></div>
    </section><!-- Agenda -->
    <?php
endif;
?>

<?php
    $v->insert("enquete", ["enquete" => $enqueteHome]);
?>

<section class="main_colunistas margin-bottom-20">
    <h1><a href="#" title="Veja todos os colunistas">Colunistas</a></h1>
    
    <div class="box-colunista">

        <?php if($colunista):
            foreach($colunista as $column):
                $post_col = (new Source\Models\Post())->findPost("author = :a","a={$column->id}")->order("post_at DESC")->fetch();
            ?>
                <article class="main_colunistas_item">
                    <a href="<?=url("/artigo/{$post_col->uri}");?>"><img class="round" src="<?= image($column->photo, 130, 128); ?>" title="<?=$column->fullName();?>" alt="<?=$column->fullName();?>"></a>
                    <div class="main_colunistas_item_content">
                        <a class="link" href="<?=url("/artigo/{$post_col->uri}");?>"><h1><?=$column->fullName();?></h1>
                            <p class="tagline"><?=$column->description;?></p></a>
                        <a class="btn btn-blue" href="<?=url("/artigo/{$post_col->uri}");?>" title="Clique para Ler"><?=$post_col->title;?></a>
                    </div>
                </article><!-- item -->    
            <?php endforeach; 
        endif;?>

    </div>
    <div class="clear"></div>
</section><!-- Colnistas -->

<article class="main_radio">
    <h1><a href="#" title="Clique para ouvir uma rádio"><i class="fa fa-microphone"></i></a> Rádio <span>Online</span></h1>
</article> Rádio Online

<!-- <div class="main_radio_parceira"> -->
    <!-- < ?php $readPub->GetPub($estate, "300x250-radio-lateral-content", $localPub); ?> -->
<!--</div>--><!-- Rádio Parceira -->

<script>
    //inserir para funcionar no mobile
    let html = document.querySelector('#pegarHtml').innerHTML
    document.querySelector('#insert').innerHTML = html
</script>