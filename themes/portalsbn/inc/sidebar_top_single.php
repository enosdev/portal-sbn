<aside class="main_banner_300 margin-bottom-20 banner">
    <h1 class="font-zero">Publicidade</h1>
    <!-- <img src="< ?= theme('/img/banner_300_600.png');?>" alt=""> -->
    <?= bannerAds(1,300,250); ?>
</aside><!-- banner 300 -->

<div class="clear"></div>
<div class="box_social">
    <p>SIGA-NOS</p>
    <ul class="social_compartilha">        
        <li><a href="https://www.facebook.com/<?= CONF_SOCIAL_FACEBOOK_PAGE; ?>" target="_blank"><i class="fa fa-facebook facebook"></i></li>
        <li><a href="https://www.youtube.com/<?= CONF_SOCIAL_YOUTUBE_PAGE; ?>" target="_blank"><i class="fa fa-youtube youtube"></i></li>
        <li><a href="https://www.instagram.com/<?=CONF_SOCIAL_INSTAGRAM_PAGE;?>" target="_blank"><i class="fa fa-instagram instagram"></i></li>
        <li><a href="https://www.twitter.com/<?= CONF_SOCIAL_TWITTER_CREATOR; ?>" target="_blank"><i class="fa fa-twitter twitter"></i></li>
        <!-- <li><a href="#" target="_blank"><i class="fa fa-telegram telegram"></i></li>
        <li><a href="#" target="_blank"><i class="fa fa-whatsapp whatsapp"></i></li> -->
    </ul>
</div>
<div class="clear"></div>



<?php
if ($list_agenda):
    ?>
    <section class="main_colunistas">
        <h1 class="margin-bottom-5" style="text-align:center"><a href="<?=url("/agenda");?>" title="Veja todas as agendas" style="color:#ff8000"><strong>Próximos Eventos</strong></a></h1>
        <div class="box_evento">
            <?php
                foreach($list_agenda as $agenda):
                    $v->insert("article_sidebar_agenda", ["agenda" => $agenda]);
                endforeach;
            ?>
        </div>
        <div class="clear"></div>
    </section><!-- Agenda -->
    <?php
endif;
?>

<article class="main_radio">
    <h1><a href="#" title="Clique para ouvir uma rádio"><i class="fa fa-microphone"></i></a> Rádio <span>Online</span></h1>
</article> Rádio Online

<!-- <div class="main_radio_parceira"> -->
    <!-- < ?php $readPub->GetPub($estate, "300x250-radio-lateral-content", $localPub); ?> -->
<!--</div>--><!-- Rádio Parceira -->