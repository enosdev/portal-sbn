<?php $v->layout('_theme');?>

<section class="main_header_ultimas container">
    <div class="content" style="padding: 0;">
        <h1 class="font-zero">Sub-categorias</h1>
        <article class="main_header_ultimas_item first"> 
            <ul>
                <?php
                    //dpega subcategoria caso tenha
                    if($menu):
                        
                        $sm = (new Source\Models\Category())
                            ->find('parent = :p', "p={$menu}")
                            ->fetch(true);

                        if($sm):
                            $byid = (new Source\Models\Category())->findById($menu);?>
                                <li><a title="<?=$byid->title;?>" href="<?=url("/artigo/em/{$byid->uri}");?>"><?=$byid->title;?></a></li>
                            <?php
                            foreach ($sm as $sub):?>
                            <li><a title="<?=$sub->title;?>" href="<?=url("/artigo/em/{$sub->uri}");?>"><?=$sub->title;?></a></li>
                        <?php endforeach;
                        else:
                            $t = (new Source\Models\Category())
                            ->find('id = :id', "id={$menu}", "parent")
                            ->fetch()->parent;

                            $categoryId = $menu;
                            $active = function ($value) use ($categoryId) {
                                return ($categoryId == $value ? "active" : "");
                            };

                            $sb = (new Source\Models\Category())
                            ->find('parent = :p || id = :p', "p={$t}")
                            ->fetch(true);

                            if($sb):
                                foreach($sb as $sub):?>
                                    <li><a class="<?=$active($sub->id);?>" title="<?=$sub->title;?>" href="<?=url("/artigo/em/{$sub->uri}");?>"><?=$sub->title;?></a></li>
                                <?php endforeach;
                            endif;

                        endif;
                    endif;
                ?>
            </ul>
        </article>
        <div class="clear"></div>
    </div>
</section><!-- SUBCATEGORIAS  -->



<main class="main_content container">
    <div class="content">
        <section class="main_content_left container">
        
            <!-- funciona no mobile -->
            <aside class="main_banner_728 banner_top_content">
                <?= bannerAds(1,728,90); ?>
            </aside>

            <section class="main_slide container">
                <!-- mobile slide -->
                <div class="main_slide_mobile">
                    <div class="slide_controll">
                        <div class="slide_nav slide_nav_destaques back round"><i class="fa fa-chevron-left"></i></div>
                        <div class="slide_nav slide_nav_destaques go round"><i class="fa fa-chevron-right"></i></div>
                    </div>
                    <?php
                        if($slide):
                            $i = 0;
                            foreach($slide as $sl):
                                $i++;
                                $first = ( $i == 1 ? ' first' : '' );
                                $v->insert("slide", ["sl" => $sl,"first" => $first, "mod" => "mobile"]);
                            endforeach;
                        endif;
                    ?>
                </div>
                

                <div class="main_slide_destaques">
                    <?php
                        if($slide):
                            $i = 0;
                            foreach($slide as $sl):
                                $i++;
                                $first = ( $i == 1 ? ' first' : '' );
                                $v->insert("slide", ["sl" => $sl,"first" => $first, "mod" => "normal"]);
                            endforeach;
                        endif;
                    ?>
                    

                    <div class="main_slide_destaques_click">
                        <?php
                            if($slide):
                                $i = 0;
                                foreach($slide as $sl):
                                    $i++;
                                    $active = ( $i == 1 ? ' active' : '' );
                                    $v->insert("slide", ["sl" => $sl,"active" => $active, "mod" => "link", "i" => $i]);
                                endforeach;
                            endif;
                        ?>
                    </div>
                </div><!-- SLIDE DESKTOP -->
                <div class="clear"></div>
                
            </section><!-- SLIDE DESTAQUES -->

            <section class="main_mais_destaques_list container">
                <h1 class="font-zero">Mais Destaques</h1>
                <?php
                    if($maisDestaque):
                        foreach($maisDestaque as $news):
                            $v->insert("article_news", ["news" => $news]);
                        endforeach;
                    endif;
                ?>
                <div class="clear"></div>
            </section><!-- MAIS DESTAQUES -->

            <div id="insert" class="main_sidebar_top container">
                <!-- < ?php require (__DIR__.'/inc/sidebar_top.php'); ?> -->
            </div> <!--SIDEBAR TOP-->

            <aside class="main_banner margin-bottom-20 margin-top-10">
                <h1 class="font-zero">Publicidade</h1>
                <div class="banner_728 banner" style="width: 100%; max-width:728px;"><?= bannerAds(2,728,90); ?></div>
                <div class="banner_220 banner"><?= bannerAds(1,220,90); ?></div>
            </aside><!-- banner -->

            <section class="main_mais_noticias_list container" style="display:flex; flex-wrap: wrap;">
                <h1 class="font-zero">Mais Notícias</h1>
                <?php
                    if($maisNoticias):
                        foreach($maisNoticias as $news):
                            $v->insert("article_news", ["news" => $news]);
                        endforeach;
                    endif;
                ?>
            </section><!-- Mais Notícias -->


            <div class="main_sidebar_bottom container">
                <?php require (__DIR__.'/inc/sidebar_bottom.php'); ?>    
            </div><!-- SIDEBAR BOTTOM -->

            <aside class="main_banner margin-bottom-20 margin-top-10">
                <h1 class="font-zero">Publicidade</h1>
                <div class="banner_728 banner" style="width: 100%; max-width:728px;"><?= bannerAds(3,728,90); ?></div>
                <div class="banner_220 banner"><?= bannerAds(1,220,90); ?></div>
            </aside><!-- banner -->

        </section> <!-- CONTENT LEFT -->

        
        <div class="main_content_right container">
            <div class="main_sidebar container">
                <div id="pegarHtml">
                    <?php require (__DIR__ . '/inc/sidebar_top.php'); ?>
                </div>
                <?php require (__DIR__ . '/inc/sidebar_bottom.php'); ?>
            </div>
        </div><!-- CONTENT RIGHT -->


        <section class="main_outras_noticias container j_more" style="display:flex; flex-wrap: wrap;">
            <h1 class="font-zero">Outras Notícias</h1>
            <?php
                if($outrasNoticias):
                    foreach($outrasNoticias as $outer):
                        $v->insert("article_news", ["news" => $outer]);
                    endforeach;
                endif;
            ?>
            <div class="clear"></div>
        </section><!-- Outras Notícias -->


        <div class="container align-center">
            <a class="btn btn-blue text-uppercase margin-top-10 margin-bottom-15 j_load_more" id="artigos" data-category="<?=$menu;?>">Carregar Mais</a>
            <img class="form-load margin-right-15 j_more_load" style="display:none; padding: 15px;" alt="Enviando Requisição!" title="Enviando Requisição!" src="<?=theme("/img/load.gif");?>"/>
        </div>

        <div class="clear"></div>


    </div> <!-- CONTENT -->
</main>