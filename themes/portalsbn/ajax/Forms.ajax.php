<?php

require_once '../../../_app/Config.inc.php';
//RECEBE SUBMIT DO FORMULARIO VIA POST
$getData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$setData = array_map("strip_tags", $getData);
$setData = array_map("trim", $setData);

$action = $setData['action'];
$jSon['accept'] = null;
unset($setData['action']);

switch ($action):
    case 'solicitar_cobertura':
        /* Seta os dados */
        $setData['solicite_date'] = date('Y-m-d H:i:s');
        $setData['solicite_status'] = 2;
        $setData['solicite_event_date'] = Check::Data($setData['solicite_event_date']);

        /* Verifica a existência de campos vazios */
        if (in_array("", $setData)):
            $jSon['alert'] = "OPPSS! Preencha todos os campos para continuar.";
        else:
            /* Valida o E-mail */
            if (!filter_var($setData['solicite_user_email'], FILTER_VALIDATE_EMAIL) || !Check::Email($setData['solicite_user_email'])):
                $jSon['alert'] = "<b>OPPSS! </b> E-mail inválido. <b>Informe outro e-mail!</b>";
            else:
                /* Cadastra solicitação */
                $create = new Create;
                $create->ExeCreate("app_solicite_cobertura", $setData);
                if ($create->getResult()):
                    $jSon['accept'] = "<b>Solicitação enviada! </b> Aguarde, <b>Entraremos em contato</b> o mais breve possível.";
                endif;
            endif;
        endif;

        break;

    case 'divulgar_evento':
        /* Seta os dados */
        $setData['agenda_date'] = date('Y-m-d H:i:s');
        $setData['agenda_status'] = 3;
        $setData['agenda_event_date'] = Check::Data($setData['agenda_event_date']);
        $setData['agenda_event_name'] = Check::Name($setData['agenda_event_title']);
        $agendaId = $setData['agenda_id'];
        
        unset($setData['agenda_id'], $setData['event_image']);
        
        /* Verifica a existência de campos vazios */
        if (in_array("", $setData)):
            $jSon['alert'] = "OPPSS! Preencha todos os campos para continuar.";
        else:
            /* Valida o E-mail */
            if (!filter_var($setData['agenda_user_email'], FILTER_VALIDATE_EMAIL) || !Check::Email($setData['agenda_user_email'])):
                $jSon['alert'] = "<b>OPPSS! </b> E-mail inválido. <b>Informe outro e-mail!</b>";
            else:
                /* Atualiza solicitação */
                $update = new Update;
                $update->ExeUpdate("app_agenda", $setData, "WHERE agenda_id = :id", "id={$agendaId}");
                if ($update->getResult()):
                    $jSon['accept'] = "<b>Enviado! </b> Obrigado por divulgar o seu evento no Portal SBN";
                endif;
            endif;
        endif;
        break;
        
    case 'upload_form_cover':
        //CONSULTA AGENDA NO BANCO DE DADOS
        $agendaId = $setData['agenda_id'];
        
        unset($setData['agenda_id']);
        
        $readPost = new Read;
        $readPost->ExeRead("app_agenda", "WHERE agenda_id = :id", "id={$agendaId}");
        if (!$readPost->getResult()):
            $jSon['error'] = "<b>OOPPSS! {$_SESSION['userlogin']['user_name']}, algo deu errado!</b> Tente novamente.";
        else:
            /* Exclui imagem de capa do post (se existir) */
            $capa = '../../uploads/images/' . $readPost->getResult()[0]['agenda_event_cover'];
            if (file_exists($capa) && !is_dir($capa)):
                unlink($capa);
            endif;

            /* Faz o upload da imagem */
            $upload = new Upload;
            $upload->Image($_FILES['event_image'], 'agenda-' . $readPost->getResult()[0]['agenda_id'] . "-" . rand(1, 9), 960, "../../../uploads/images");
            if (!$upload->getResult()):
                $jSon['error'] = "<b>OOPPSS!</b>," . $upload->getError();
            else:
                /* Conta a quantidade de caracteres da string da Imagem */
                $lenght = strlen($upload->getResult());
                /* Gera nova String corrigida, tirando o "../../../uploads/images/" */
                $cover = substr($upload->getResult(), 24, $lenght);

                /* Atualiza o agenda_event_cover no banco de dados */
                $Dados = ['agenda_event_cover' => $cover];
                $updateCover = new Update;
                $updateCover->ExeUpdate("app_agenda", $Dados, "WHERE agenda_id = :id", "id={$readPost->getResult()[0]['agenda_id']}");
                if (!$updateCover->getResult()):
                    $jSon['error'] = "<b>OOPPSS! Algo deu errado!</b> Tente novamente.";
                else:
                    /* Retorna String HTML com a imagem */
                    $jSon['accept'] = "<b>Tudo certo!</b> Imagem enviada com sucesso.";
                endif;
            endif;
        endif;
        
        break;
endswitch;
echo json_encode($jSon);
