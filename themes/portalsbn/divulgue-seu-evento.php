<?php $v->layout('_theme');?>
<main class="main_content container">
    <section class="content main_entretenimento_form" style="margin-bottom: 50px;">
        <div class="main_entretenimento_page_title">
            <h1>Divulgue seu evento</h1>
            <p>Que tal ter seu evento divulgado no Portal SBN? Utilize o formulário abaixo para divulgar seu evento.</p>
        </div>

        <form name="solicitar-cobertura" method="post" class="j_solicite_form_cover" action="">
            <input type="hidden" name="action" value="divulgar_evento"/>
            <input type="hidden" name="agenda_estate" value="<?= $estate; ?>"/>
            <input type="hidden" name="agenda_id" value="<?= $agenda_id; ?>"/>
            <label class="label form_30">
                <span class="legend">Seu Nome <b>*</b></span>
                <input type="text" name="agenda_user_name" required/>
            </label>
            <label class="label form_30">
                <span class="legend">E-mail <b>*</b></span>
                <input type="email" name="agenda_user_email" required/>
            </label>
            <label class="label form_30" style="margin-right: 0;">
                <span class="legend">Telefone <b>*</b></span>
                <input type="tel" name="agenda_user_phone" placeholder="(DDD)0000-0000" required/>
            </label>

            <label class="label form_100">
                <span class="legend">Nome do Evento <b>*</b></span>
                <input type="text" name="agenda_event_title" required/>
            </label>

            <label class="label form_30">
                <span class="legend">Data do evento <b>*</b></span>
                <input type="text" name="agenda_event_date" class="datetimepicker" required/>
            </label>
            <label class="label form_30">
                <span class="legend">Cidade do evento <b>*</b></span>
                <input type="text" name="agenda_event_city" required/>
            </label>
            <label class="label form_30" style="margin-right: 0;">
                <span class="legend">Local do evento <b>*</b></span>
                <input type="text" name="agenda_event_local" placeholder="Clube, área, etc." required/>
            </label>

            <label class="label form_100">
                <span class="legend">Detalhes <b>*</b></span>
                <textarea name="agenda_event_details" rows="10" placeholder="Horários, venda de ingressos, informações adicionais." required></textarea>
            </label>

            <label class="label form_100">
                <span class="legend">Cartaz, material de apoio <b>*</b></span>
                <input style="background: #FFF;" type="file" name="event_image" class="j_form_cover" required/>
            </label>
            
            <div class="progress_bar margin-bottom-15 j_post_cover_bar" style="display:none;">
                    <div class="bar" style="width: 0%">0%</div>
            </div><!-- Progressbar -->


            <button class="btn btn-blue">Enviar <img class="form-load margin-left-10" style="display:none; float: right; max-height: 15px;" alt="Enviando Requisição!" title="Enviando Requisição!" src="<?= INCLUDE_PATH; ?>/img/load.gif"/></button>

        </form>

        <div class="clear"></div>
    </section>
</main>