<?php $v->layout("_theme"); ?>

<section class="main_header_ultimas container">
    <div class="content">

        <?php 
        if($chamadas_ultimas):
            $i = 0;
            foreach($chamadas_ultimas as $ch):
                $i++;
                $first = ( $i == 1 ? 'first' : '' );
        ?>
                <article class="main_header_ultimas_item main_header_ultimas_item_slide <?= $first; ?>"><a href="<?= url("/artigo/{$ch->uri}"); ?>" title="<?=$ch->title;?>"><h1 style="font-size:1.2em;color:red;"><i style="color: #0888CB; " class="fa fa-clock-o"></i> <?=$ch->title;?></h1></a></article>
         <?php endforeach;
        endif;
        ?>

        <div class="clear"></div>
    </div>
</section><!-- ÚLTIMAS -->


<main class="main_content container">
    <div class="content">
        <section class="main_content_left container">
        
            <!-- funciona no mobile -->
            <aside class="main_banner_728 banner_top_content">
                <!-- <img src="< ?= theme('/img/banner_728_90.png');?>" alt=""> -->
                <?= bannerAds(1,728,90); ?>
            </aside>

            <section class="main_slide container">
                <!-- mobile slide -->
                <div class="main_slide_mobile">
                    <div class="slide_controll">
                        <div class="slide_nav slide_nav_destaques back round"><i class="fa fa-chevron-left"></i></div>
                        <div class="slide_nav slide_nav_destaques go round"><i class="fa fa-chevron-right"></i></div>
                    </div>
                    <?php
                        if($slide):
                            $i = 0;
                            foreach($slide as $sl):
                                $i++;
                                $first = ( $i == 1 ? ' first' : '' );
                                $v->insert("slide", ["sl" => $sl,"first" => $first, "mod" => "mobile"]);
                            endforeach;
                        endif;
                    ?>
                </div>
                

                <div class="main_slide_destaques">
                    <?php
                        if($slide):
                            $i = 0;
                            foreach($slide as $sl):
                                $i++;
                                $first = ( $i == 1 ? ' first' : '' );
                                $v->insert("slide", ["sl" => $sl,"first" => $first, "mod" => "normal"]);
                            endforeach;
                        endif;
                    ?>
                    

                    <div class="main_slide_destaques_click">
                        <?php
                            if($slide):
                                $i = 0;
                                foreach($slide as $sl):
                                    $i++;
                                    $active = ( $i == 1 ? ' active' : '' );
                                    $v->insert("slide", ["sl" => $sl,"active" => $active, "mod" => "link", "i" => $i]);
                                endforeach;
                            endif;
                        ?>
                    </div>
                </div><!-- SLIDE DESKTOP -->
                <div class="clear"></div>
                
            </section><!-- SLIDE DESTAQUES -->

            <section class="main_mais_destaques container" style="display:flex; flex-wrap: wrap;">
                <h1 class="font-zero">Mais Destaques</h1>
                <?php
                    if($maisDestaque):
                        foreach($maisDestaque as $news):
                            $v->insert("article_news", ["news" => $news]);
                        endforeach;
                    endif;
                ?>
                <div class="clear"></div>
            </section><!-- MAIS DESTAQUES -->

            <div id="insert" class="main_sidebar_top container">
                <!-- < ?php require (__DIR__.'/inc/sidebar_top.php'); ?> -->
            </div> <!--SIDEBAR TOP-->

            <aside class="main_banner margin-bottom-20 margin-top-10">
                <h1 class="font-zero">Publicidade</h1>
                <div class="banner_728 banner" style="width: 100%; max-width:728px;">
                    <!-- <img src="< ?= theme('/img/banner_728_90.png');?>" alt=""> -->
                    <?= bannerAds(2,728,90); ?>
                </div>
                <div class="banner_220 banner">
                    <!-- <img src="< ?= theme('/img/banner_220_90.png');?>" alt=""> -->
                    <?= bannerAds(1,220,90); ?>
                </div>
            </aside><!-- banner -->


            <!-- vídeos tv sbn -->
            <section class="main_slide main_videos container">
                <h1 class="title"><i class="fa fa-youtube-play"></i> TV SBN</h1>

                <div class="main_slide_mobile">
                    <div class="slide_controll">
                        <div class="slide_nav slide_nav_videos back round"><i class="fa fa-chevron-left"></i></div>
                        <div class="slide_nav slide_nav_videos go round"><i class="fa fa-chevron-right"></i></div>
                    </div>

                    <?php
                    if ($tvSbn):
                        $i = 0;
                        foreach ($tvSbn as $Video):
                            $i++;
                            $first = ( $i == 1 ? ' first' : '' );
                            //PEGA ID DO VÍDEO DO YOUTUBE
                            // $StrYoutube = strpos($Video['post_content'], "youtube.com/embed/");
                            // $IdYoutube = substr($Video['post_content'], $StrYoutube + 18, 11);
                            $v->insert("slide_tv", ["video" => $Video,"first" => $first, "mod" => "mobile"]);
                        endforeach;
                    endif;
                    ?>
                </div><!-- SLIDE MOBILE -->

                <div class="main_slide_videos">
                    <?php
                    if ($tvSbn):
                        $i = 0;
                        foreach ($tvSbn as $Video):
                            $i++;
                            $first = ( $i == 1 ? ' first' : '' );
                            //PEGA ID DO VÍDEO DO YOUTUBE
                            // $StrYoutube = strpos($Video['post_content'], "youtube.com/embed/");
                            // $IdYoutube = substr($Video['post_content'], $StrYoutube + 18, 11);
                            $v->insert("slide_tv", ["video" => $Video,"first" => $first, "mod" => "normal"]);
                        endforeach;
                    endif;
                    ?>

                    <div class="main_slide_destaques_click">
                        <?php
                        if ($tvSbn):
                            $i = 0;
                            foreach ($tvSbn as $Video):
                                /* PEGA CATEGORIA DIFERENTE DE TV SBN SE EXISTIR */
                                // $video_category_color = ( $readCatSlide->getResult() ? $readCatSlide->getResult()[0]['category_color'] : $Video['category_color']);
                                // $video_category_title = ( $readCatSlide->getResult() ? $readCatSlide->getResult()[0]['category_title'] : $Video['category_title']);
                                $i++;
                                $active = ( $i == 1 ? ' active' : '' );
                                $v->insert("slide_tv", ["video" => $Video,"active" => $active, "mod" => "link", "i" => $i]);
                            endforeach;
                        endif;
                        ?>
                    </div>
                </div><!-- SLIDE DESKTOP -->

                <div class="clear"></div>
            </section><!-- SLIDE VÍDEOS -->

            <section class="main_mais_noticias container" style="display:flex; flex-wrap: wrap;">
                <h1 class="font-zero">Mais Notícias</h1>
                <?php
                    if($maisNoticias):
                        foreach($maisNoticias as $news):
                            $v->insert("article_news", ["news" => $news]);
                        endforeach;
                    endif;
                ?>
            </section><!-- Mais Notícias -->


            <div class="main_sidebar_bottom container">
                <?php require (__DIR__.'/inc/sidebar_bottom.php'); ?>    
            </div><!-- SIDEBAR BOTTOM -->

            <aside class="main_banner margin-bottom-20 margin-top-10">
                <h1 class="font-zero">Publicidade</h1>
                <div class="banner_728 banner" style="width: 100%; max-width:728px;"><?= bannerAds(3,728,90); ?></div>
                <div class="banner_220 banner"><?= bannerAds(2,220,90); ?></div>
            </aside><!-- banner -->

            <section class="main_slide main_galeria container">
                <h1 class="title"><i class="fa fa-picture-o"></i> Galerias em Destaque</h1>
                <div class="main_slide_mobile">
                    <div class="slide_controll">
                        <div class="slide_nav slide_nav_galerias back round"><i class="fa fa-chevron-left"></i></div>
                        <div class="slide_nav slide_nav_galerias go round"><i class="fa fa-chevron-right"></i></div>
                    </div>

                    <?php
                    if ($gallery):
                        $i = 0;
                        foreach ($gallery as $gal):
                            $i++;
                            $first = ( $i == 1 ? ' first' : '' );
                            $v->insert("slide_gallery", ["gal" => $gal,"first" => $first, "mod" => "mobile"]);
                        endforeach;
                    endif;
                    ?>
                </div><!-- SLIDE MOBILE -->


                <div class="main_slide_galeria">
                    <?php
                    if ($gallery):
                        $i = 0;
                        foreach ($gallery as $gal):
                            $i++;
                            $first = ( $i == 1 ? ' first' : '' );
                            $v->insert("slide_gallery", ["gal" => $gal,"first" => $first, "mod" => "normal"]);
                        endforeach;
                    endif;
                    ?>

                    <div class="main_slide_galeria_click">
                        <?php
                        if ($gallery):
                            $i = 0;
                            foreach ($gallery as $gal):
                                $i++;
                                $active = ( $i == 1 ? ' active' : '' );
                                $v->insert("slide_gallery", ["gal" => $gal,"active" => $active, "mod" => "link", "i" => $i]);
                            endforeach;
                        endif;
                        ?>
                    </div>
                </div><!-- SLIDE DESKTOP -->

                <div class="clear"></div>
            </section><!-- GALERIA DE FOTOS -->
        
        </section> <!-- CONTENT LEFT -->

        
        <div class="main_content_right container">
            <div class="main_sidebar container">
                <div id="pegarHtml">
                    <?php require (__DIR__ . '/inc/sidebar_top.php'); ?>
                </div>
                <?php require (__DIR__ . '/inc/sidebar_bottom.php'); ?>
            </div>
        </div><!-- CONTENT RIGHT -->


        <section class="main_outras_noticias container j_more" style="display:flex; flex-wrap: wrap;">
            <h1 class="font-zero">Outras Notícias</h1>
            <?php
                if($outrasNoticias):
                    foreach($outrasNoticias as $outer):
                        $v->insert("article_news", ["news" => $outer]);
                    endforeach;
                endif;
            ?>
            <div class="clear"></div>
        </section><!-- Outras Notícias -->


        <div class="container align-center">
            <a class="btn btn-blue text-uppercase margin-top-10 margin-bottom-15 j_load_more" id="index" data-category="">Carregar Mais</a>
            <img class="form-load margin-right-15 j_more_load" style="display:none; padding: 15px;" alt="Enviando Requisição!" title="Enviando Requisição!" src="<?=theme("/img/load.gif");?>"/>
        </div>

        <div class="clear"></div>


    </div> <!-- CONTENT -->
</main>
