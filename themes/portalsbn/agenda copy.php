<?php $v->layout('_theme');?>

<main class="main_content container">
    <div class="content">

        <div class="main_content_left container" style="width:100%;">
            <article class="main_single_content" style="border-top-color:<?= $cor; ?>; background:#f2f2f2; padding-left:0; padding-right:0; padding-top:15px;">
                <header>
                    <h1>Eventos</h1>
                </header>

                <?php
                if ($list_agenda):
                    ?>
                    <section class="main_outras_noticias container container">
                        <?php
                        foreach ($list_agenda as $agenda):
                            $v->insert("article_sidebar_agenda", ["agenda" => $agenda]);
                        endforeach;
                        ?>
                        <div class="clear"></div>
                    </section><!-- Outras Notícias -->
                    <?php
                else:
                    ?>
                    <div style="padding: 50px 0 150px 0 !important; color: #666; float: left; width: 100%;">
                        <header style="border-bottom:none; text-align: center; margin-bottom: 0;">
                            <h1 style="font-weight:300; font-size: 1.8em; color: #666;"><i class="fa fa-frown-o"></i> Nenhum evento encontrado!</h1>
                        </header>
                        <div class="clear"></div>
                        <div class="htmlchars align-center">
                            <p>No momento não existem eventos na nossa agenda. Por favor, volte mais tarde.</p>
                        </div>
                    </div>
                <?php
                endif;
                
                echo $paginator;
                ?>
            </article><!-- Content -->
        </div><!-- CONTENT LEFT -->
        <div class="clear"></div>
    </div>
</main>